#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <ncurses.h>

#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <cstdlib>

using namespace std;

namespace cs {

  class View {

    char ** screen;
    //    int xsize, ysize;
    int maxx, maxy;

  public :
    View() {
      startup();
    }
    ~View() {
      shutdown();
    }

    void clear() {
      for (int i=0; i<maxy; ++i) {
	for (int j=0; j<maxx; ++j) {
	  screen[i][j] = ' ';
	}
      }
    }

    void startup() {


      initscr();
      getmaxyx(stdscr, maxy, maxx);
      endwin();

      //      screen = new char*[maxx];
      screen = new char*[maxy];
      for (int i = 0; i<maxy; ++i) {	
	screen[i] = new char[maxx];
	//	  for (int j = 0; j<maxx; ++j) {	
	//	    screen[i][j] = j;
	//	  }
      }
      clear();
      cout << " maxx = " << maxx << endl;
      cout << " maxy = " << maxy << endl;
    }
    
    void shutdown() {
	// for (int i=0; i<maxx; ++i) {
	//   delete [] screen[i];
	// }
	for (int i=0; i<maxy; ++i) {
	  delete [] screen[i];
	}
	delete [] screen;
    }

    void draw(int lines = 18);

    void draw_eq(const std::string s);

    //    void draw(); {
    //   for (int i=0; i<maxy; ++i) {
    // 	for (int j=0; j<maxx-100; ++j) {
    // 	//	  printf("%s\n", screen[i]);
    // 	  printf("%c", screen[i][j]);
    // 	}
    // 	printf("\n");
    //   }
    // }
	
  public:
	
    static void ansi(int code) {
      printf("%c[%dm",27,code);
    }
    static void ansi2(int code) {
      printf("%c[%d;%dm",1,27,code);
    }
    static void cls(int x=0, int y=0) {
      printf("%c[2J%c[%d;%dH",27,27,y,x);
    }
    void move(int x=0,int y=0) {
      printf("%c[%d;%dH",27,y,x);  
    }
    // colors :
    /*
      # 31 -red

      # 33 - yellow
      # 34 - blue
      # 35 - purple
      # 36 - aquamarine
      # 37 - white
    */

    void cshline(int x, int y,int l) {
      for (int i=1; i<=l; i++) {
	// move(x+i,y);
	// printf("%c",'-');
	//	screen[x+i][y] = '-';
	screen[y][x+i] = '-';
	
      }
    }

    void csvline(int x, int y,int l);
      /*
      for (int i=0; i<l; i++) {
	// move(x,y+i);
	// printf("%c",'|');
	//		screen[x][y+i] = '|';

	// if (y+i > maxy || x > maxx)
	//   cout << " przekroczenie zakresu dla (x,y) : " << x << " " << y << endl;
	// else 
	screen[y+i][x] = '|';
	cout << " y = " << y << endl;
      }
    }
      */
    void sline(int x, int y,int l) { // slash line
      for (int i=1; i<=l; i++) {
	// move(x+l-i,y+i);
	// printf("%c",'/')
	//screen[x+l-i][y+i]= '/';
	// if (y+i > maxy || x+l-i > maxx)
	//   cout << " przekroczenie zakresu dla (x,y) : " << x << " " << y << endl;
	screen[y+i][x+l-i]= '/';
      }
    }
    void bsline(int x, int y,int l) { // back-slash line
      for (int i=1; i<=l; i++) {
	// move(x+i,y+i);
	// printf("%c",'\\');
	//	screen[x+i][y+i] = '\\';
	screen[y+i][x+i] = '\\';
      }
    }
    void line(int nr,int x, int y, int l) { //nr : 1-7
      switch(nr) {
      case 1 : cshline(x+1,y,l); break;
      case 2 : csvline(x,y+1,l); break;
      case 3 : csvline(x+l+2,y+1,l); break;
      case 4 : cshline(x+1,y+l+1,l); break;
      case 5 : csvline(x,y+l+2,l); break;
      case 6 : csvline(x+l+2,y+l+2,l); break;
      case 7 : cshline(x+1,y+2*l+2,l); break;
      case 8 : cshline(x+1,y+l+1,l); break;// -
      case 9 : csvline(x+1+l/2,y+2+l/2,l-1); break;// | od +
      case 10: sline(x+1,y+2+l/2,l); break;// /
      case 11 : bsline(x+1, y+2+l/2,l); break;// \ od *
      case 12 : {
	cshline(x+1,y+l+1-1,l);
	cshline(x+1,y+l+1+1,l);
      } break; // =
      }
    }

    void print(int x, int y,int val) {
      const int l=6;
      switch(val) 
	{
	case 0 : 
	  {
	    line(1,x,y,l);
	    line(2,x,y,l);
	    line(3,x,y,l);
	    line(5,x,y,l);
	    line(6,x,y,l);
	    line(7,x,y,l);
	  } break;
	case 1 : 
	  {
	    line(3,x,y,l);
	    line(6,x,y,l);
	  } break;
	case 2 : 
	  {
	    line(1,x,y,l);
	    line(3,x,y,l);
	    line(4,x,y,l);		
	    line(5,x,y,l);
	    line(7,x,y,l);
	  } break;
	case 3 : 
	  {
	    line(1,x,y,l);
	    line(3,x,y,l);
	    line(4,x,y,l);		
	    line(6,x,y,l);
	    line(7,x,y,l);
	  } break;
	case 4 : 
	  {
	    line(2,x,y,l);
	    line(3,x,y,l);
	    line(4,x,y,l);		
	    line(6,x,y,l);
	  } break;
	case 5 : 
	  {
	    line(1,x,y,l);
	    line(2,x,y,l);
	    line(4,x,y,l);		
	    line(6,x,y,l);
	    line(7,x,y,l);
	  } break;
	case 6 : 
	  {
	    line(1,x,y,l);
	    line(2,x,y,l);
	    line(4,x,y,l);		
	    line(5,x,y,l);
	    line(6,x,y,l);
	    line(7,x,y,l);
	  } break;
	case 7 : 
	  {
	    line(1,x,y,l);
	    line(3,x,y,l);
	    line(6,x,y,l);
	  } break;
	case 8 : 
	  {
	    line(1,x,y,l);
	    line(2,x,y,l);
	    line(3,x,y,l);
	    line(4,x,y,l);		
	    line(5,x,y,l);
	    line(6,x,y,l);
	    line(7,x,y,l);
	  } break;
	case 9 : 
	  {
	    line(1,x,y,l);
	    line(2,x,y,l);
	    line(3,x,y,l);
	    line(4,x,y,l);		
	    line(6,x,y,l);
	    line(7,x,y,l);
	  } break;
	case 10 : // +
	  {
	    line(8,x,y,l);
	    line(9,x,y,l);
	  } break;
	case 11 : // /
	  {
	    line(10,x,y,l);
	  } break;
	case 12 : // =
	  {
	    line(12,x,y,l);
	  } break;
	case 13 : // -
	  {
	    line(8,x,y,l);
	  } break;
	case 14 : // *
	  {
	    line(10,x,y,l);
	    line(11,x,y,l);
	  } break;
	}
    }
    void print_eq(const std::string ss) {
      std::string::const_iterator it;
      // static int line = 0;
      // if (line >=3) {
      // 	cls();
      // 	line =0;
      // }
      std::stringstream sstr;
      for (it = ss.begin(); it != ss.end(); ++it) {
	if (*it != ' ') 
	  sstr << *it;
      }
      std::string s = sstr.str();

      int i = 0;
      char obj_type;
      for (it = s.begin(); it != s.end(); ++it) {
	switch(*it) {
	case '0' : case '1' : case '2' : case '3' : 
	case '4' : case '5' : case '6' : case '7' : 
	case '8' : case '9' : 	
	  obj_type = *it-'0';
	  break;
	case '+' :
	  {
	    obj_type = 10;
	  } break;
	case '-' :
	  {
	    obj_type = 13;
	  } break;
	case '=' :
	  {
	    obj_type = 12;
	  } break;
	case '/' : 
	  {
	    obj_type = 11;
	  } break;
	case '*' :  
	  {
	    obj_type = 14;
	  } break;

	}

	//	print(0+i*12,18*line,obj_type); 
	print(0+i*12,0,obj_type); 
	++i;
      }
      //      line++;
    }
	

  };

  typedef unsigned char u8;


}; // namespace cs

#endif

