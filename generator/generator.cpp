#include <iostream>
#include <string>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <time.h>

#include <set>
#include <map>
#include <list>
#include <algorithm>

#include "console.h"

#include <ncurses.h>


using namespace std;

namespace consts {
  enum Oper {
    add = '+',
    sub = '-',
    mul = '*',
    div = '/'
  };
}; // namespace consts

#define EQU_SIZE 8
class Equation {
  
  char ctab[EQU_SIZE];  // holder of equ characters
  int itab[EQU_SIZE];  // holder of equ values

  std::string expr; // equation in string form 

  bool can_add[EQU_SIZE]; // mozna dodac zapalke
  bool can_remove[EQU_SIZE]; // mozna odjac zapalke
  bool can_move[EQU_SIZE]; // mozna przesunac zapalke

  bool solved; // true if equation is already solved
  
  bool match_missing; // true if one match was taken from any digit or char
  bool match_added; // true if match was already added to equation

public:
  Equation() : 
    solved(false), 
    match_missing(false), 
    match_added(false) 
  {
    clear_tab();
  }

  ~Equation() {
  }
  // clear equation tab
  void clear_tab() {
    for (int i=0; i < EQU_SIZE; ++i) {

      ctab[i] = 0;
      itab[i] = -1;
      can_add[i] = can_remove[i] = can_move[i] = 0;

    }
  }
 const std::string str() const {
    return expr;
  }

  void string2tab(std::string expr_str);


  void fromExpr(std::string exp) {
    expr = exp;
    string2tab(exp);
  }
};

// ustawia tablice flag dodwania, usuwania i przenoszenia zapalek
// ustawia tablice znakow dzialania : ctab[]
// ustawia tablice liczb dzialania : itab[]
void Equation::string2tab(std::string expr_str) {
  string::iterator it;

  int len = expr_str.length();


  int i =0;
  for (it = expr_str.begin(); it != expr_str.end(); ++it) {
    char c = static_cast<char>(*it);
    ctab[i] = c;
    itab[i] = c - '0';
    switch (c) {
      case '0' : 
	can_add[i] = 1;
	can_move[i] = 1;
	break;
      case '1' :
	can_add[i] = 1;
	break;
      case '2' :
	can_add[i] = 1;
	break;
      case '3' :
	can_add[i] = 1;
	can_move[i] = 1;
	break;
      case '4' :
	break;
      case '5' :
	can_add[i] = 1;
	can_move[i] = 1;
	break;
      case '6' :
	can_add[i] = 1;
	can_move[i] = 1;
	break;
      case '7' :
	can_remove[i] = 1;
	break;
      case '8' :
	can_remove[i] = 1;
	break;
      case '9' :
	can_add[i] = 1;
	can_move[i] = 1;
	can_remove[i] = 1;
	break;
	/*
	  case '+' :
	  can_remove[i] = 1;
	  break;
	  case '-' :
	  can_add[i] = 1;
	  break;
	  case '*' :
	  can_remove[i] = 1;
	  break;
	  case '/' :
	  can_add[i] = 1;
	  break;
	  case '=' :
	  can_remove[i] = 1;
	  break;
	*/
    } // switch
    ++i;
  } // for
}

typedef std::set<Equation> EquationSet;
typedef std::set<Equation>::iterator EquationSet_it;

class Generator {
  Equation equ;
  EquationSet equ_set; // set of 
  EquationSet generated_set; // set of generated equations
  EquationSet_it equ_it;
public:
  Generator() {
  }
  ~Generator() {
  }

  void print() {
    for (equ_it = generated_set.begin(); equ_it != generated_set.end(); ++equ_it) {
      cout << "gen : " << equ_it->str() << endl;
    }
  }

  void generateFromEqu(Equation e) {
    
  }
};

// klasa reprezentujaca wyrazenie
struct Expr {
  string txt; // tekstowa watosc wyrazenia
  int value; // wartosc wyrazenia
  int len; // dlugosc wyrazenia w znakach
  consts::Oper oper; // operator w wyrazeniu
};

// mapowania wynik -> dzialanie
std::multimap< int, Expr > expr_tab3; // 3-znakowe dzialania
std::multimap< int, Expr > expr_tab4; // 4-znakowe dzialania
std::multimap< int, Expr > expr_tab5; // 5-znakowe dzialania
std::multimap< int, Expr > expr_tab6; // 6-znakowe dzialania

std::multimap< int, Expr > expr_tab34; // 6-znakowe dzialania

std::multimap< int, Expr > div_tab; //  dzialania dzielenia
std::multimap< int, Expr > mul_tab; //  dzialania mnozenia
std::multimap< int, Expr > add_tab; //  dzialania dodawania
std::multimap< int, Expr > sub_tab; //  dzialania odejmowania

std::multimap< int, Expr > expr_tab; //  wszystkie dzialania

//std::multimap< int, Expr > dublet_tab; //  wszystkie podwojne dzialania
std::list< string > dublet_list; //  wszystkie podwojne dzialania



typedef std::multimap< int, Expr > Expr_tab;
typedef Expr_tab::iterator Expr_it;
typedef pair < Expr_it, Expr_it> Expr_it_pair;

void makeMatrixFromString(string expr);

char getDigit(char n) {
  char num = n+'0';
  return num;
}

char getOper(char n) {
  switch (n) {
    case 0: return '=';
    case 1: return '+';
    case 2: return '-';
    case 3: return '*';
    case 4: return '/';
  }
}

int getRand(int range) {
  return rand() % (range + 1);
}

// losuje od 0 do 9
int getRand0() {
  return rand() % 10;
}

// losuje od 1 do 9
int getRand1() {
  return rand() % 9 + 1;
}

// losuje od 10 do 99
int getRand10() {
  return rand() % 90 + 10;
}

int getRand100() {
  return rand() % 900 + 100;  
}

bool isMinusFirst() {
  return (bool)getRand(1);
}

bool oneOrTwo() {
  return (bool)getRand(1);
}

bool case2() {
  return (bool)getRand(1);
}

int getNumberWithLength(int digits) {
  switch (digits) {
    case 1: return getRand1();
    case 2: return getRand10();
    case 3: return getRand100();
  }
  return 0;
}

// sprawdza czy liczba jest 3-cyfrowa
// is_3_digit_number(int num)
bool cypher3(int num) {
  int checknum = num - (num % 100);
  return (checknum != 0);
}

// podaje ilosc cyfr w liczbie 
int cypher(int num) { 

  int checknum = num;
  int check = 10;
  int digit_count = 0;

  if (0 == num) {
    return 1;
  }
  else if (num < 0) {
    checknum = -num;
    digit_count++;
  }


  while (checknum != 0) {
    checknum = checknum - (checknum % check);
    check *= 10;
    digit_count++;
  }
  return digit_count;
}

// generuje wyrażenie 3 elementowe
// ktore da sie wyliczyc do liczb calkowitych
// np. : 1+1, 2*2, 8/4 itp.
string formula3() {
}

// generuje wyrazenie 4 elementowe
string formula4() {
}

// generowanie wyrazen 3-el z podzialem na operator
void genAllDivForm3() {
  for (int i=0; i<10; ++i) {
    for (int j=1; j<10; ++j) {
      if (i % j == 0) {
	cout << i << " / " << j << " [ = " << i/j << " ]";
	cout << endl;
      }
    }
  }
}

void genAllMulForm3() {
  for (int i=0; i<10; ++i) {
    for (int j=0; j<10; ++j) {

      stringstream ss;

      ss << i << " * "<< j << " [ = " << i*j << " ]";
      // debug :D
      cout << ss.str() << endl;
    }
  }
}

void genAllAddForm3() {
}

void genAllSubForm3() {
}

void addToExprTab(int i, int j, consts::Oper operation) {
  // wyliczamy wynik
  int value;
  switch (operation) {
    case consts::div : value = i/j; break;
    case consts::mul : value = i*j; break;
    case consts::add : value = i+j; break;
    case consts::sub : value = i-j; break;
  }
  // dlugosc wynikowa (liczba1 + operator + liczba2 + '=' + wynik)
  int len = cypher(i) + cypher(j) + cypher(value) + 2; 
  char op = static_cast<char>(operation);
  if (len <= 8) {
    std::stringstream ss;
    //    ss << i << ' ' << op << ' ' << j;
    ss << i << op << j;
    
    // dlugosc rowna sie dlugosci cyfr plus 1 (operator)
    int expr_len = cypher(i) + cypher(j) + 1; 
    //    consts::Oper op = consts::div;
    
    Expr expr;
    expr.txt = ss.str();
    expr.value = value;
    expr.len = expr_len;
    expr.oper = operation;
    
    expr_tab.insert( pair<int, Expr>(value, expr));
    //	cout << ss.str() << " [ = " << value << " ]" << endl;
    
  } // 	if (len <= 8) {
}


// dodaje dzialanie dzielenia dwoch liczb do tablicy dzialan dzielenia
void addToDivTab(int i, int j) {
  // wyliczamy wynik
  int value = i/j;
  // dlugosc wynikowa (liczba1 + operator + liczba2 + '=' + wynik)
  int len = cypher(i) + cypher(j) + cypher(value) + 2; 
  if (len <= 8) {
    std::stringstream ss;
    //	  ss << i << '/' << j;
    ss << i << ' ' << static_cast<char>(consts::div) << ' ' << j;

    // dlugosc rowna sie dlugosci cyfr plus 1 (operator)
    int expr_len = cypher(i) + cypher(j) + 1; 
    consts::Oper op = consts::div;
	  
    Expr expr;
    expr.txt = ss.str();
    expr.value = value;
    expr.len = expr_len;
    expr.oper = op;
	  
    div_tab.insert( pair<int, Expr>(value, expr));
    //	cout << ss.str() << " [ = " << value << " ]" << endl;

  } // 	if (len <= 8) {
}
// generowanie wyrazen 4-el z podzialem na operator
void genAllDivForm() {

  for (int i=1; i<1000; ++i) {
    for (int j=1; j<100; ++j) {
      if (i % j == 0) {
	addToDivTab(i,j);
	addToDivTab(-i,j);
	addToDivTab(i,-j);
	addToDivTab(-i,-j);
      }
    }
  }
} // void genAllDivForm4() {

void genAllMulForm() {
  consts::Oper op = consts::mul;
  for (int i=-100; i<100; ++i) {
    for (int j=-100; j<100; ++j) {

      // wyliczamy wynik
      int value = i*j;
      // dlugosc wynikowa (liczba1 + operator + liczba2 + '=' + wynik)
      int len = cypher(i) + cypher(j) + cypher(value) + 2; 
      
      if (len <= 8) {
	addToExprTab( i, j, op);
	addToExprTab(-i, j, op);
	addToExprTab( i, -j, op);
	addToExprTab(-i, -j, op);
      }
    }
  }
}

void genOneForm(int min_i, int max_i, int min_j, int max_j, consts::Oper op) {
  //  int value;
  for (int i=min_i; i<max_i; ++i) {
    for (int j=min_j; j<max_j; ++j) {

      // wyliczamy wynik
      // switch (op) {
      // case consts::div : value = i/j; break;
      // case consts::mul : value = i*j; break;
      // case consts::add : value = i+j; break;
      // case consts::sub : value = i-j; break;
      // }
      
      if (consts::div == op) {
	if ( (j>0) && (i%j == 0) ) {
	  //	  if (len <= 8) {
	  addToExprTab( i, j, op);
	  addToExprTab(-i, j, op);
	  //	    addToExprTab( i, -j, op);
	  addToExprTab(-i, -j, op);
	  //	  }
	}
      }
      else {
	addToExprTab( i, j, op);	
	//	addToExprTab(-i, j, op);
	// addToExprTab( i, -j, op);
	// addToExprTab(-i, -j, op);
      }


      // dlugosc wynikowa (liczba1 + operator + liczba2 + '=' + wynik)
      //      int len = cypher(i) + cypher(j) + cypher(value) + 2; 
      
    }
  }

}

void genAllForms() {
}

void genAllAddForm4() {
}

void genAllSubForm4() {
}

// generuje wszystkie mozliwe wyrazenia
// 3 elementowe
void genAllFrom3() {
}

// generuje wszystkie mozliwe wyrazenia
// 4 elementowe
void genAllFrom4() {
}

/*
  tworzy dzialanie tylko z dzieleniem
*/
string generateDivEquation() {
  // losujemy dwie liczby i je mnozymy
  // potem przestawiamy wynik i jeden z czynnikow
  // i dodajemy do nich operator dzielenia

  // losujemy czy ktorys czynnik ma byc ujemny

  // tylko jednen czynnik może być dwucyfrowy

  int factor1, factor2, result;
  stringstream ss;

  // losujemy czy liczba 2-cyfrowa bedzie
  if ( case2() ) {
    factor1 = getRand10();
  } else {
    factor1 = getRand1();
  }

  // druga liczba zawsze jest jednocyfrowa
  factor2 = getRand1();
  
  result = factor1 * factor2;

  // jesli obecny wynik jest 3-cyfrowy nie moze byc minusa
  if (! cypher3(result)) {

    // losujemy czy bedzie minus i dla ktorej liczby
    if (case2()) {
      if (case2() ) {
	factor1 = -factor1;
      } else {
	factor2 = -factor2;
      }
    }
  }

  // wyliczamy wynik :
  result = factor1 * factor2;

  // wrzucamy w strumien :
  ss << result << '/';

  // losujemy czy po wyniku i dzieleniu bedzie 1-sza czy 2-ga z liczb
  if (case2()) {
    ss << factor1 << '=' << factor2;
  } else {
    ss << factor2 << '=' << factor1;
  }
  
  return ss.str();
  
}

/*
  Prymitywna funkcja do generowania dzialan
  od lewej do prawej
*/
string generateLevel() {

  stringstream ss;

  if (isMinusFirst() ) {
    // pierwszy jest minus
    // nie moze byc 3-cyfrowa liczba
    ss << '-';
  } else {
    // bez minusa
    // moze byc 3-cyfrowa liczba
  }
}

// wypakuj z tablicy dzialania o dlugosci 3 i wrzuc do expr_tab3
void extract_tab4(std::multimap<int, Expr> tab) {
  std::multimap<int, Expr>::iterator it;

  for (it = tab.begin(); it != tab.end(); ++it) {
    //    cout << i++ << " : "<< (*it).second.txt << " = " << it->second.value << endl;
    if (3 == it->second.len) {
      expr_tab3.insert( pair<int, Expr> (3, it->second));
    }
      
  }
  
}

// wypakuj z tablicy dzialania o dlugosci 4 i wrzuc do expr_tab4
void extract_tab3(std::multimap<int, Expr> tab) {
  std::multimap<int, Expr>::iterator it;

  for (it = tab.begin(); it != tab.end(); ++it) {
    //    cout << i++ << " : "<< (*it).second.txt << " = " << it->second.value << endl;
    if (4 == it->second.len) {
      expr_tab4.insert( pair<int, Expr> (4, it->second));
    }

  }

}

void extract_tab34(std::multimap<int, Expr> tab) {
  std::multimap<int, Expr>::iterator it;

  for (it = tab.begin(); it != tab.end(); ++it) {
    //    cout << i++ << " : "<< (*it).second.txt << " = " << it->second.value << endl;
    if (4 == it->second.len || 3 == it->second.len) {
      expr_tab34.insert( pair<int, Expr> (it->second.value, it->second));
    }

  }

}

void insertDublet(string s) {
  if (s.size() <= 8) {
    dublet_list.push_back(s);

    cout << s;
    makeMatrixFromString(s);
  }
}

void generateAlldoublets() {
  Expr_it it, it2;
  Expr_it_pair it_pair;


  for (it = expr_tab34.begin(); it != expr_tab34.end(); ++it) {
    it_pair = expr_tab34.equal_range(it->second.value);
    for (it2 = it_pair.first; it2 != it_pair.second; ++it2) {
      std::stringstream ss;
      ss << it->second.txt << "=" << it2->second.txt;
      insertDublet(ss.str());


      //     cout << ss.str()<< "\n";
    }
  }
}

void print_tab(std::multimap<int, Expr> tab) {
  std::multimap<int, Expr>::iterator it;

  int i = 1;
  for (it = tab.begin(); it != tab.end(); ++it) {
    cout << i++ << " : "<< (*it).second.txt << " = " << it->second.value << endl;
  }
}

void print_list(std::list< string > l) {
  std::list< string >::iterator it;
  for (it = l.begin(); it != l.end(); ++it) {
    cout << (*it) << endl;
  }
}

void makeStringFromMatrix(bool char_match[8][7]) {
  char c;
  switch (c) {
  }
}

const std::set<char> doMove(char c) {
  std::set<char> results;
  switch (c) {
    case '2' :
      results.insert('3');
      break;
    case '3' :
      results.insert('2');      
      results.insert('5');      
      break;
    case '5' :
      results.insert('3');      
      break;
    case '6' :
      results.insert('0');      
      results.insert('9');      
      break;
    case '9' :
      results.insert('0');      
      results.insert('6');      
      break;
    case '0' :
      results.insert('6');      
      results.insert('9');      
      break;
  }
  return results;
}

void fn(int a) {
  cout << " a = " << a << endl;
}

void processSpot(char tab[8], int spot, void (*fun)(int)) {
  fn(spot);
}

void test_p(int a) {
  char tab[8];
  processSpot(tab, a, fn);
}

// jaki rodzaj liczby/znaku da się zrobić 
// modyfikujac (zabierajac/dodajac) zapalke w slocie 'slot'
// jesli nic nie da sie zrobic zwracamy pierwotny znak
char doChange(char c, int slot, int to = 0) {

  switch (c) {
    case '0' : 
      if (slot == 4)
	return '8';
      if (3 == slot && to == 4)
	return '6';
      if (5 == slot && to == 4)      
	return '9';
      break;
      
    case '1' : 
      if (1 == slot)
	return '7';
      break;

    case '2' : 
      if (5 == slot && to == 6)
	return '3';
      break;

    case '3' :
      if (3 == slot && 2 == to)
	return '5';
      if (6 == slot && 5 == to)
	return '2';
      if (2 == slot) 
	return '9';
      break;
      
    case '4' :
      break;

    case '5' :
      if (2 == slot && 3 == to)
	return '3';
      if (3 == slot)
	return '9';
      if (5 == slot)
	return '6';
      break;
      
    case '6' :
      if (slot == 5 && to == 3)
	return '9';
      if (slot == 3)
	return '8';
      if (slot == 4 && to == 3)
	return '0';
      if (slot == 5)
	return '5';
      break;

    case '7' :
      if (slot == 1)
	return '1';
      break;

    case '8' :
      if (slot == 3)
	return '6';
      if (slot == 4)
	return '0';
      if (slot == 5)
	return '9';
      break;

    case '9' :
      if (slot == 2)
	return '3';
      if (slot == 3 && to == 5)
	return '6';
      if (slot == 3)
	return '5';
      if (slot == 4 && to == 5)
	return '0';
      if (slot == 5)
	return '8';
      break;

    case '+' :
      return '-';
      break;

    case '-' :
      return '+';
      break;

    case '*' :
      return '/';
      break;

    case '/' :
      return '*';
      break;

    default :
      return c;
      // case '=' :
      //   break;

  } // switch
} // doChange

void applyCommand(int cmd, string expr) {
  // cmd = 1384 - z 1 znaku wez zaplke z pozycji 3 i wstaw do znaku 8 na pozycje 4 
  std::stringstream ss;
  char c;
  int from_char, from_slot, to_char, to_slot;
  ss << cmd;
  ss >> c;
  from_char = c - '0';
  ss >> c;
  from_slot = c - '0';
  ss >> c;
  to_char = c - '0';
  ss >> c;
  to_slot = c - '0';  
}


void makeMatrixFromString(string expr) {
  bool char_match[8][7]; // 8 znakow po 7 zpalek max
  bool can_add[8]; // mozna dodac zapalke
  bool can_remove[8]; // mozna odjac zapalke
  bool can_move[8]; // mozna przesunac zapalke

  int len = expr.length();
  
  for (int i =0; i<8; ++i) {
    can_add[i] = can_remove[i] = can_move[i] = 0;
  }

  string::iterator it;
  int i =0;
  for (it = expr.begin(); it != expr.end(); ++it) {
    char c = static_cast<char>(*it);
    switch (c) {
      case '0' : 
	can_add[i] = 1;
	can_move[i] = 1;
	break;
      case '1' :
	can_add[i] = 1;
	break;
      case '2' :
	can_add[i] = 1;
	break;
      case '3' :
	can_add[i] = 1;
	can_move[i] = 1;
	break;
      case '4' :
	break;
      case '5' :
	can_add[i] = 1;
	can_move[i] = 1;
	break;
      case '6' :
	can_add[i] = 1;
	can_move[i] = 1;
	break;
      case '7' :
	can_remove[i] = 1;
	break;
      case '8' :
	can_remove[i] = 1;
	break;
      case '9' :
	can_add[i] = 1;
	can_move[i] = 1;
	can_remove[i] = 1;
	break;
	/*
	  case '+' :
	  can_remove[i] = 1;
	  break;
	  case '-' :
	  can_add[i] = 1;
	  break;
	  case '*' :
	  can_remove[i] = 1;
	  break;
	  case '/' :
	  can_add[i] = 1;
	  break;
	  case '=' :
	  can_remove[i] = 1;
	  break;
	*/
    } // switch
    ++i;
  } // for
  
  int add_num = std::count(can_add, can_add+8, 1);
  int remove_num = std::count(can_remove, can_remove+8, 1);
  int move_num = std::count(can_move, can_move+8, 1);
  int a = add_num; int r = remove_num; int m = move_num;
  //  cout << " | moves : " << m << " removes : " << r << " | adds : "<< a <<endl;
  cout << "\n";

  for (int i=0; i<len; ++i) {
    // lets try to mess things up :) 
    if (can_remove[i]) {
      //      procSpot(tab, i, 
      
    }
  }
  
}

void makeMess(string expr) {
  bool char_match[8][7]; // 8 znakow po 7 zpalek max
  
  
}


int check_take(char z);
char take(char z, int times);

int check_add(char z);
char add(char z, int times);

int check_move(char z);
char move(char z, int times);


void save_equ_in_level_tab(char equ[]);

void mess_alg(char equ[]);




int main() {

  cs::View view;
  //  view.startup();


  int maxx, maxy;

  initscr();
  getmaxyx(stdscr, maxy, maxx);
  endwin();




  srand(time(NULL));
  // Equals * equals = new Equals;
  // delete equals;

  // 1|2|3|4|5|6|7|8 |
  // --------------- |
  // 9 * 9 0 = 8 1 0
  // 9 * 9 9 = 8 9 1
  // - 9 * 9 = - 8 1
  // 9 * 9 = 1 8 
  // 2 * 3 = 6

  // 1 0 0 / 2 0 = 5
  // 8 9 1 / 9 9 = 9
  // 1 0 0 / 5 = 2 0
  // - 8 1 / 9 = - 9
  // - 8 1 / - 9 = 9
  // 8 0 / 1 0 = 8
  // 9 9 / 9 = 1 1
  // 1 8 / 9 = 2

  // 2 0 / 5 = 2 + 2

  // 2 0 / 5 = 2 * 2
  // 8 4 / 4 = 7 * 3

  // - 1 + 1 1 = 1 0
  // 1 1 + 1 1 = 2 2
  // 1 0 1 - 2 = 9 9
  // 1 + 1 = 3 - 1
  // 9 + 9 = 2 0 - 2
  // 9 + 9 = 1 5 + 3



  /// TESTS
  ///////////////////////////////////////

  cout << "running generator procedures tests... " << endl;
  cout << endl;
  /*
    string s1, s2;
    s1 = getDigit(5);
    s2 = getDigit(0);

    cout<< " number 5 : "<< s1 << endl;
    cout<< " number 0 : "<< s2 << endl;

    cout << "operator 0 : " << getOper(0) << endl;
    cout << "operator 1 : " << getOper(1) << endl;
    cout << "operator 2 : " << getOper(2) << endl;
    cout << "operator 3 : " << getOper(3) << endl;
    cout << "operator 4 : " << getOper(4) << endl;

    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;
    cout << "rand from 0 to 10 : " << getRand(10) << endl;

    cout << "is beginning with minus : " << isMinusFirst() << endl;
    cout << "is beginning with minus : " << isMinusFirst() << endl;
    cout << "is beginning with minus : " << isMinusFirst() << endl;

    for (int i = 10; i<20; ++i) 
    cout << "one digit number ex : " << getRand1() << endl;

    for (int i = 10; i<20; ++i) 
    cout << "two digit number ex : " << getRand10() << endl;

    for (int i = 10; i<20; ++i) 
    cout << "three digit number ex : " << getRand100() << endl;
  */

  // for (int i = 1; i<50; ++i) 
  //   cout << "generated div equation : " << generateDivEquation() << endl;


  ////////////////////////////////////////
  // cypher3 test
  ///////////////////////////////////////
  /*
    for (int i = 19; i<320; i+=36) 
    cout << "is 3 cyphers ? ["<<i<<"] : "<<cypher3(i)<<endl;

    for (int i = 19; i<320; i+=36) 
    cout << "is 3 cyphers ? ["<<-i<<"] : "<<cypher3(-i)<<endl;
  */
  ///////////////////////////////////////
  cout << endl << endl;

  //  	genAllMulForm3();
  //  	genAllDivForm3();

  //	genAllDivForm4();

  cout << endl << endl;

  cout << "end of generator procedures tests... " << endl;
  cout << endl << endl;

  /// END of TESTS
  ///////////////////////////////////////

  /*
    view.draw_eq("101-3=98/2");
    view.draw_eq("11-11=22-2");
	
    view.draw_eq(" 9 * 9 0 = 8 1 0");
    view.draw_eq(" 9 * 9 9 = 8 9 1");
    view.draw_eq(" - 9 * 9 = - 8 1");
    view.draw_eq(" 9 * 9 = 1 8 ");
    view.draw_eq(" 2 * 3 = 6");

    view.draw_eq(" 1 0 0 / 2 0 = 5");
    view.draw_eq(" 8 9 1 / 9 9 = 9");
    view.draw_eq(" 1 0 0 / 5 = 2 0");
    view.draw_eq(" - 8 1 / 9 = - 9");
    view.draw_eq(" - 8 1 / - 9 = 9");
    view.draw_eq(" 8 0 / 1 0 = 8");
    view.draw_eq(" 9 9 / 9 = 1 1");
    view.draw_eq(" 1 8 / 9 = 2");

    view.draw_eq(" 2 0 / 5 = 2 + 2");

    view.draw_eq(" 2 0 / 5 = 2 * 2");
    view.draw_eq(" 8 4 / 4 = 7 * 3");

    view.draw_eq(" - 1 + 1 1 = 1 0");
    view.draw_eq(" 1 1 + 1 1 = 2 2");
    view.draw_eq(" 1 0 1 - 2 = 9 9");
    view.draw_eq(" 1 + 1 = 3 - 1");
    view.draw_eq(" 9 + 9 = 2 0 - 2");
    view.draw_eq(" 9 + 9 = 1 5 + 3");
  */

  // for (int i = -1001; i<1009; i+=9) 
  //   cout << " cyphers ? ["<<i<<"] : "<<cypher(i)<<endl;



  //  	genAllDivForm();
  //	print_tab(div_tab);


  //	genAllMulForm();

  // importante !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       genOneForm(1, 1000, 1, 100, consts::div);
     genOneForm(-100, 100, -100, 100, consts::mul);
     genOneForm(-1000, 1000, 1, 1000, consts::add);
     genOneForm(-1000, 1000, 1, 1000, consts::sub);
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  // extract_tab3(expr_tab);
   //  extract_tab4(expr_tab);

  // importante !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       extract_tab34(expr_tab);
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       //  	print_tab(expr_tab34);



  //	print_list(dublet_list);

  // int i = 1;
  // for (it = div_tab.begin(); it != div_tab.end(); ++it) {
  //   cout <<"expression nr " << i++ << " : "<< (*it).second.txt << " = " << it->second.value << endl;
  // }

  // for (it = expr_tab4.begin(); it != expr_tab4.end(); ++it) {
  //   view.draw_eq(it->second.txt);
  // }

  // importante !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	generateAlldoublets();
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  /*
  cout << "WPISZ KOMENDE : ";
  std::stringstream ss;
  int cmd;
  cin >> cmd;
  cout << " komenda : " << cmd << endl;
	
  ss << cmd;
  char c;
  int a;
  ss >> c;
  a = c -'0';
  cout << " 1 : " << a << endl;


  ss >> c;
  a = c -'0';
  cout << " 1 : " << a << endl;


  ss >> c;
  a = c -'0';
  cout << " 1 : " << a << endl;


  ss >> c;
  a = c -'0';
  cout << " 1 : " << a << endl;
  */

  //  test_p(41);
   /*
  cout << " + - 0 = " << ('+' - '0') << endl;
  cout << " - - 0 = " << ('-' - '0') << endl;
  cout << " * - 0 = " << ('*' - '0') << endl;
  cout << " / - 0 = " << ('/' - '0') << endl;
   */
  Equation equ;
  equ.fromExpr("47-7=8*5");

  Generator gen;
  gen.generateFromEqu(equ);
  gen.print();


  
  cout << "mess algorithm tests... " << endl;
  
  //char equ_tab[9] = "612/68=9";  
  //mess_alg(equ_tab);
}

int check_take(char z) {
  switch(z) {
  case '1' : case '2': case'3': case'4': case'5': case'0': case'-': case'/' :
    return 0;
  case '6': case'7': case'+': case'*': case'=' :
    return 1;
  case '9' : return 2;
  case '8' : return 3;
  }
}

char take(char z, int times) {
  switch (z) {
  case '1': case'2': case'3': case'4': case'5': case'0': case'-': case'/' :
    return z;
    break;
  case '6' : return '5';
    break;
  case '7' : return '1';
    break;
  case '8' :
    switch (times) {
    case 1: return '0';
    case 2: return '6';
    case 3: return '9';
    }
  case '9':
    switch (times) {
    case 1: return '5';
    case 2: return '3';
    }    
  case '+' : return '-';
  case '*' : return '/';
  case '=' : return '-';
  }
}
int check_add(char z) {
  switch(z) {
  case '2': case'4': case'7': case'8': case'+': case'*': case'=' :
    return 0;
  case '1': case'3': case'6': case'9': case'0': case'/':
    return 1;
  case '5': case '-' :
    return 2;
  }
}

char add(char z, int times) {
    switch (z) {
    case '2': case'4': case'7': case'8': case'+': case'*': case'=' :
      return z;
    case '1' : return '7';
    case '3' : return '9';
    case '6' : return '8';
    case '9' : return '8';
    case '0' : return '8';
    case '/' : return '*';
    case '5' :
      switch (times) {
      case 1: return '6';
      case 2: return '9';
      }    

    case '-' :
      switch (times) {
      case 1: return '+';
      case 2: return '=';
      }    
    }
}

int check_move(char z) {
  switch(z) {
  case '1': case'4': case'7': case'8': case'-': case'/': case'*':
    return 0;
  case '2': case'5': case'+': case'=' :
    return 1;
  case '3': case'6': case'9': case'0' :
    return 2;
  }
}

char move(char z, int times) {
  switch(z) {
  case '1': case'4': case'7': case'8': case'-': case'/': case'*':
    return z;
  case '2': return '3';
  case '5' : return '3';
  case '+' : return '=';
  case '=' : return '+';
  case '3':
      switch (times) {
      case 1: return '5';
      case 2: return '2';
      }    
  case '6':
      switch (times) {
      case 1: return '0';
      case 2: return '9';
      }    
  case '9':
      switch (times) {
      case 1: return '0';
      case 2: return '6';
      }    
  case '0':
      switch (times) {
      case 1: return '6';
      case 2: return '9';
      }    
  }
}


void save_equ_in_level_tab(char equ[]) {
  //  cout << equ << endl;
  cout <<"\t<string>"<< equ<<"</string>" << endl;
}

void mess_alg(char equ[9]) {
  //  char equ[9] = "612/68=9";
  //  char work_equ[9] = "612/68=9";
  char work_equ[9];
  for (int i=0; i<9; ++i) {
    work_equ[i]=equ[i];
  }
  for (int i=0; i<8; ++i) {
    for (int j=0; j<8; ++j) {
      if (j!=i) {
	int n_times = check_take(equ[i]);
	// if take returns more than one then do it again !
	while (n_times>0) {
	  work_equ[i] = take(equ[i],n_times);

	  int m_times = check_add(equ[j]);
	  while (m_times >0) {
	    work_equ[j] = add(equ[j], m_times);
	    save_equ_in_level_tab(work_equ);
	    work_equ[i] = equ[i];
	    work_equ[j] = equ[j];
	    --m_times;
	  }
	  --n_times;
	}
      } else {
	// do self_move
	int times = check_move(equ[i]);
	while (times >0) {
	  work_equ[i] = move(equ[i], times);
	  save_equ_in_level_tab(work_equ);
	  work_equ[i] = equ[i];
	  --times;
	}
      }
    }
  }
}
