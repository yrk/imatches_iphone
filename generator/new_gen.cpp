/*
  This code is designed to implement
  logical representation of eguation 
  made from digits and operands
  all consisting of matches 

  I hope it can also be used to implement
  proper structure for displaying them
*/

/* NOTE to self:
   all error or info logs will be to std::cout 
   so it can be changed later
*/

/* NOTE to self:
   most 'int' should be 'unsigned int' or even
   'unsigned char', but its shorter when they are char
 */


enum Match_type {
  MATCH_HORIZONTAL,
  MATCH_VERTICAL,
  MATCH_SLASH,
  MATCH_BACKSLASH
};

// simple definition of match
// match can be : - | / \
// in other words : horizontal, vertical and like slash
class Match {
  int type; // Match_type

}

// - element can be digit or operand
// - base for operands or digits
class Element {
  // I want to number slots from 1 to 12th
  // slots[0] may not be used at all - we'll see
  int slots[13]; 

  char val; // what it looks like in real world (of ASCII)
  // can be : 0-9,-,+,/,*,=

public:
  // if match cannot be added return false
  // return: 'can this match be added to this element ?'
  bool add_match(Match* match);
  // return : 'can match be taken from this slot'
  bool take_match(int slot);

  char get_val();
};


class Digit : public Element {

public:
  int get_num_val();
};

class Operand : public Element {
};


// match_slot can contain match or operand
class Match_slot {

  Element * content;
  
public:
  // helper functions
  bool can_add_match();
  bool can_remove_match();
  bool is_taken();
  bool is_digit();
  bool is_oper();
  bool is_equ();
  
  // standard shit
  Match_slot();
  ~Match_slot();  
};


// for now it's for gen Equ, setting and solving it :)
class Equation {
};



bool Element::add_match(Match* match) {
}

bool Element::take_match(int slot) {
}




bool Match_slot::can_add_match() {
}
bool Match_slot::can_remove_match() {
}
bool Match_slot::is_taken() {
}
bool Match_slot::is_digit() {
}
bool Match_slot::is_oper() {
}
bool Match_slot::is_equ() {
}


Match_slot::Match_slot() {
  content = NULL;
}
Match_slot::~Match_slot() {
  if (content != NULL) {
    delete content;
    content = NULL;
  }
}
