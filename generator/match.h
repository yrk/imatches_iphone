#ifndef MATCH_H_
#define MATCH_H_

#include <cstdio>
#include <iostream>
#include <string>
#include <time.h>
#include <cstdlib>

namespace imatch {

class View {
	
public:
	
	static void ansi(int code) {
		printf("%c[%dm",27,code);
	}
	static void ansi2(int code) {
		printf("%c[%d;%dm",1,27,code);
	}
	static void cls(int x=0, int y=0) {
		printf("%c[2J%c[%d;%dH",27,27,y,x);
	}
	void move(int x=0,int y=0) {
		printf("%c[%d;%dH",27,y,x);  
	}
	// colors :
	/*
	  # 31 -red
	  # 32 - green
	  # 33 - yellow
	  # 34 - blue
	  # 35 - purple
	  # 36 - aquamarine
	  # 37 - white
	*/
	void hline(int x, int y,int l) {
		for (int i=1; i<=l; i++) {
			move(x+i,y);
			printf("%c",'-');
		}
	}
	void vline(int x, int y,int l) {
		for (int i=1; i<=l; i++) {
			move(x,y+i);
			printf("%c",'|');
		}
	}
	void sline(int x, int y,int l) { // slash line
		for (int i=1; i<=l; i++) {
			move(x+l-i,y+i);
			printf("%c",'/');
		}
	}
	void bsline(int x, int y,int l) { // back-slash line
		for (int i=1; i<=l; i++) {
			move(x+i,y+i);
			printf("%c",'\\');
		}
	}
	void line(int nr,int x, int y, int l) { //nr : 1-7
		switch(nr) {
		case 1 : hline(x+1,y,l); break;
		case 2 : vline(x,y+1,l); break;
		case 3 : vline(x+l+2,y+1,l); break;
		case 4 : hline(x+1,y+l+2,l); break;
		case 5 : vline(x,y+l+2,l); break;
		case 6 : vline(x+l+2,y+l+2,l); break;
		case 7 : hline(x+1,y+2*l+3,l); break;
		case 8 : hline(x+1,y+l+2,l); break;// -
		case 9 : vline(x+1+l/2,y+2+l/2,l-1); break;// | od +
		case 10: sline(x+1,y+2+l/2,l); break;// /
		case 11 : bsline(x+1, y+2+l/2,l); break;// \ od *
		case 12 : {
			hline(x+1,y+l+2-1,l);
			hline(x+1,y+l+2+1,l);
		} break; // =
		}
	}

	void print(int x, int y,int val) {
		const int l=6;
		switch(val) 
			{
			case 0 : 
				{
					line(1,x,y,l);
					line(2,x,y,l);
					line(3,x,y,l);
					line(5,x,y,l);
					line(6,x,y,l);
					line(7,x,y,l);
				} break;
			case 1 : 
				{
					line(3,x,y,l);
					line(6,x,y,l);
				} break;
			case 2 : 
				{
					line(1,x,y,l);
					line(3,x,y,l);
					line(4,x,y,l);		
					line(5,x,y,l);
					line(7,x,y,l);
				} break;
			case 3 : 
				{
					line(1,x,y,l);
					line(3,x,y,l);
					line(4,x,y,l);		
					line(6,x,y,l);
					line(7,x,y,l);
				} break;
			case 4 : 
				{
					line(2,x,y,l);
					line(3,x,y,l);
					line(4,x,y,l);		
					line(6,x,y,l);
				} break;
			case 5 : 
				{
					line(1,x,y,l);
					line(2,x,y,l);
					line(4,x,y,l);		
					line(6,x,y,l);
					line(7,x,y,l);
				} break;
			case 6 : 
				{
					line(1,x,y,l);
					line(2,x,y,l);
					line(4,x,y,l);		
					line(5,x,y,l);
					line(6,x,y,l);
					line(7,x,y,l);
				} break;
			case 7 : 
				{
					line(1,x,y,l);
					line(3,x,y,l);
					line(6,x,y,l);
				} break;
			case 8 : 
				{
					line(1,x,y,l);
					line(2,x,y,l);
					line(3,x,y,l);
					line(4,x,y,l);		
					line(5,x,y,l);
					line(6,x,y,l);
					line(7,x,y,l);
				} break;
			case 9 : 
				{
					line(1,x,y,l);
					line(2,x,y,l);
					line(3,x,y,l);
					line(4,x,y,l);		
					line(6,x,y,l);
					line(7,x,y,l);
				} break;
			case 10 : // +
				{
					line(8,x,y,l);
					line(9,x,y,l);
				} break;
			case 11 : // /
				{
					line(10,x,y,l);
				} break;
			case 12 : // =
				{
					line(12,x,y,l);
				} break;
			case 13 : // -
				{
					line(8,x,y,l);
				} break;
			case 14 : // *
				{
					line(10,x,y,l);
					line(11,x,y,l);
				} break;
			}
	}
	void print_eq(const std::string s) {
		std::string::const_iterator it;
		static int line = 0;
		if (line >=3) {
			cls();
			line =0;
		}
		int i = 0;
		char obj_type;
		for (it = s.begin(); it != s.end(); ++it) {
			switch(*it) {
			case '0' : case '1' : case '2' : case '3' : 
			case '4' : case '5' : case '6' : case '7' : 
			case '8' : case '9' : 	
				obj_type = *it-'0';
				break;
			case '+' :
				{
					obj_type = 10;
				} break;
			case '-' :
				{
					obj_type = 13;
				} break;
			case '=' :
				{
					obj_type = 12;
				} break;
			case '/' : 
			  {
			    obj_type = 11;
			  } break;
			case '*' :  
			  {
			    obj_type = 14;
			  } break;

			}

			print(0+i*12,18*line,obj_type); 
			++i;
		}
		line++;
	}
	

};

typedef unsigned char u8;

}; // namespace imatch

#endif
