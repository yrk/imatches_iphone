#include "console.h"

namespace cs {
  void View::draw(int lines) {
    printf("\n");
    if (lines > maxy)
      lines = maxy;
    
    for (int i=0; i<lines; ++i) {
      for (int j=0; j<maxx; ++j) {
	printf("%c", screen[i][j]);
      }
      printf("\n");
    }

    printf("\n");
  }

  void View::csvline(int x, int y,int l) {
      for (int i=0; i<l; i++) {
	screen[y+i][x] = '|';
      }
    }

  void View::draw_eq(const std::string s) {
    clear();
    print_eq(s);
    draw();
  }
  
};

