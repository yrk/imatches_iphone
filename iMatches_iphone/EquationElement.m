//
//  EquationElement.m
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 20.07.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import "EquationElement.h"


@implementation EquationElement

-(char) getCharValue {
    CCLOG(@"[%@] %@ : this is interface method - should not be used !!!", self, NSStringFromSelector(_cmd));
    return '0';
}

-(BOOL)canRecieveMatch {
    CCLOG(@"[%@] %@ : this is interface method - should not be used !!!", self, NSStringFromSelector(_cmd));
    return NO;
}

-(BOOL)canGiveMatch {
    CCLOG(@"[%@] %@ : this is interface method - should not be used !!!", self, NSStringFromSelector(_cmd));
    return NO;
}


-(void)updatePhysicsBodies {
    CCLOG(@"[%@] %@ : this is interface method - should not be used !!!", self, NSStringFromSelector(_cmd));
    return;
}

-(int)getMatchedSlot:(CGPoint)location {
    CCLOG(@"[%@] %@ : this is interface method - should not be used !!!", self, NSStringFromSelector(_cmd));
    return 0;
}

-(CCSprite*)getMatchFromSlot:(int)slot {
    CCLOG(@"[%@] %@ : this is interface method - should not be used !!!", self, NSStringFromSelector(_cmd));
    return nil;
}

@end
