//
//  PhysicsManager.h
//  iMatches_iphone
//
//  Created by Admin on 27.01.2014.
//
//

#import "chipmunk.h"
#import "cocos2d.h"

//#import "ChipmunkSprite.h"

@interface PhysicsManager : CCNode {
    cpSpace * space;
}

//@property (nonatomic, retain) NSString * text;
@property (nonatomic, readonly) cpSpace * space;

+(id) instance;


-(void)updatePhysics:(ccTime)dt;

@end
