/* Copyright (c) 2007 Scott Lembcke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <string.h>

#import "cocos2d.h"
#define CP_ALLOW_PRIVATE_ACCESS 1
#include "chipmunk.h"
#include "chipmunk_private.h"
#include "drawSpace.h"

/*
 IMPORTANT - READ ME!
 
 This file sets up a simple interface that the individual demos can use to get
 a Chipmunk space running and draw what's in it. In order to keep the Chipmunk
 examples clean and simple, they contain no graphics code. All drawing is done
 by accessing the Chipmunk structures at a very low level. It is NOT
 recommended to write a game or application this way as it does not scale
 beyond simple shape drawing and is very dependent on implementation details
 about Chipmunk which may change with little to no warning.
 */

#define LINE_COLOR 1.0f, 1.0f, 1.0f, 1.0f
#define COLLISION_COLOR 1.0f, 0.0f, 0.0f, 1.0f
#define BODY_COLOR 0.0f, 0.0f, 1.0f, 1.0f


static inline void
glColor_from_color(ccColor4B color){
    glColor4f((GLfloat)color.r, (GLfloat)color.g, (GLfloat)color.b, (GLfloat)color.a);
}

static void
glColor_from_pointer(void *ptr)
{
    unsigned long val = (long)ptr;
    
    // hash the pointer up nicely
    val = (val+0x7ed55d16) + (val<<12);
    val = (val^0xc761c23c) ^ (val>>19);
    val = (val+0x165667b1) + (val<<5);
    val = (val+0xd3a2646c) ^ (val<<9);
    val = (val+0xfd7046c5) + (val<<3);
    val = (val^0xb55a4f09) ^ (val>>16);
    
    //      GLfloat v = (GLfloat)val/(GLfloat)ULONG_MAX;
    //      v = 0.95f - v*0.15f;
    //
    //      glColor3f(v, v, v);
    
    GLubyte r = (val>>0) & 0xFF;
    GLubyte g = (val>>8) & 0xFF;
    GLubyte b = (val>>16) & 0xFF;
    
    GLubyte max = r>g ? (r>b ? r : b) : (g>b ? g : b);
    
    const int mult = 127;
    const int add = 63;
    r = (r*mult)/max + add;
    g = (g*mult)/max + add;
    b = (b*mult)/max + add;
    
    glColor4ub(r, g, b, 255);
}

static const GLfloat circleVAR[] = {
    0.0000f,  1.0000f,
    0.2588f,  0.9659f,
    0.5000f,  0.8660f,
    0.7071f,  0.7071f,
    0.8660f,  0.5000f,
    0.9659f,  0.2588f,
    1.0000f,  0.0000f,
    0.9659f, -0.2588f,
    0.8660f, -0.5000f,
    0.7071f, -0.7071f,
    0.5000f, -0.8660f,
    0.2588f, -0.9659f,
    0.0000f, -1.0000f,
    -0.2588f, -0.9659f,
    -0.5000f, -0.8660f,
    -0.7071f, -0.7071f,
    -0.8660f, -0.5000f,
    -0.9659f, -0.2588f,
    -1.0000f, -0.0000f,
    -0.9659f,  0.2588f,
    -0.8660f,  0.5000f,
    -0.7071f,  0.7071f,
    -0.5000f,  0.8660f,
    -0.2588f,  0.9659f,
    0.0000f,  1.0000f,
    0.0f, 0.0f, // For an extra line to see the rotation.
};
static const int circleVAR_count = sizeof(circleVAR)/sizeof(GLfloat)/2;

static void
drawCircleShape(cpBody *body, cpCircleShape *circle)
{
    glVertexPointer(2, GL_FLOAT, 0, circleVAR);
    
    // Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
    // Needed states:  GL_VERTEX_ARRAY,
    // Unneeded states: GL_TEXTURE_2D, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
    
    glPushMatrix(); {
        cpVect center = circle->tc;
        glTranslatef(center.x, center.y, 0.0f);
        glRotatef(body->a*180.0f/(float)M_PI, 0.0f, 0.0f, 1.0f);
        glScalef(circle->r, circle->r, 1.0f);
        
        if(!circle->shape.sensor){
            glColor_from_pointer(circle);
            glDrawArrays(GL_TRIANGLE_FAN, 0, circleVAR_count - 1);
        }
        
        glColor4f(LINE_COLOR);
        glDrawArrays(GL_LINE_STRIP, 0, circleVAR_count);
    } glPopMatrix();
    
    // restore default GL state
    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}

static const GLfloat pillVAR[] = {
    0.0000f,  1.0000f, 1.0f,
    0.2588f,  0.9659f, 1.0f,
    0.5000f,  0.8660f, 1.0f,
    0.7071f,  0.7071f, 1.0f,
    0.8660f,  0.5000f, 1.0f,
    0.9659f,  0.2588f, 1.0f,
    1.0000f,  0.0000f, 1.0f,
    0.9659f, -0.2588f, 1.0f,
    0.8660f, -0.5000f, 1.0f,
    0.7071f, -0.7071f, 1.0f,
    0.5000f, -0.8660f, 1.0f,
    0.2588f, -0.9659f, 1.0f,
    0.0000f, -1.0000f, 1.0f,
    
    0.0000f, -1.0000f, 0.0f,
    -0.2588f, -0.9659f, 0.0f,
    -0.5000f, -0.8660f, 0.0f,
    -0.7071f, -0.7071f, 0.0f,
    -0.8660f, -0.5000f, 0.0f,
    -0.9659f, -0.2588f, 0.0f,
    -1.0000f, -0.0000f, 0.0f,
    -0.9659f,  0.2588f, 0.0f,
    -0.8660f,  0.5000f, 0.0f,
    -0.7071f,  0.7071f, 0.0f,
    -0.5000f,  0.8660f, 0.0f,
    -0.2588f,  0.9659f, 0.0f,
    0.0000f,  1.0000f, 0.0f,
};
static const int pillVAR_count = sizeof(pillVAR)/sizeof(GLfloat)/3;

static void
drawSegmentShape(cpBody *body, cpSegmentShape *seg)
{
    cpVect a = seg->ta;
    cpVect b = seg->tb;
    
    if(seg->r){
        glVertexPointer(3, GL_FLOAT, 0, pillVAR);
        
        // Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
        // Needed states:  GL_VERTEX_ARRAY,
        // Unneeded states: GL_TEXTURE_2D, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
        glDisable(GL_TEXTURE_2D);
        glDisableClientState(GL_COLOR_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        
        
        glPushMatrix(); {
            cpVect d = cpvsub(b, a);
            cpVect r = cpvmult(d, seg->r/cpvlength(d));
            
            const GLfloat matrix[] = {
                r.x, r.y, 0.0f, 0.0f,
                -r.y, r.x, 0.0f, 0.0f,
                d.x, d.y, 0.0f, 0.0f,
                a.x, a.y, 0.0f, 1.0f,
            };
            glMultMatrixf(matrix);
            
            if(!seg->shape.sensor){
                glColor_from_pointer(seg);
                glDrawArrays(GL_TRIANGLE_FAN, 0, pillVAR_count);
            }
            
            glColor4f(LINE_COLOR);
            glDrawArrays(GL_LINE_LOOP, 0, pillVAR_count);
        } glPopMatrix();
        
        // restore default GL state
        glEnable(GL_TEXTURE_2D);
        glEnableClientState(GL_COLOR_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        
    } else {
        glColor4f(LINE_COLOR);
        ccDrawLine(a,b);
    }
}

static void
drawPolyShape(cpBody *body, cpPolyShape *poly)
{
    int count = poly->numVerts;
#if CP_USE_DOUBLES
    glVertexPointer(2, GL_DOUBLE, 0, poly->tVerts);
#else
    glVertexPointer(2, GL_FLOAT, 0, poly->tVerts);
#endif
    
    // Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
    // Needed states:  GL_VERTEX_ARRAY,
    // Unneeded states: GL_TEXTURE_2D, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
    
    if(!poly->shape.sensor){
        glColor_from_pointer(poly);
        glDrawArrays(GL_TRIANGLE_FAN, 0, count);
    }
    
    glColor4f(LINE_COLOR);
    glDrawArrays(GL_LINE_LOOP, 0, count);
    
    // restore default GL state
    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}

void ChipmunkDebugDrawPolygon(int count, cpVect *verts, ccColor4B lineColor, ccColor4B fillColor)
{
#if CP_USE_DOUBLES
    glVertexPointer(2, GL_DOUBLE, 0, verts);
#else
    glVertexPointer(2, GL_FLOAT, 0, verts);
#endif
    
    if(fillColor.a > 0){
        glColor_from_color(fillColor);
        glDrawArrays(GL_TRIANGLE_FAN, 0, count);
    }
    
    if(lineColor.a > 0){
        glColor_from_color(lineColor);
        glDrawArrays(GL_LINE_LOOP, 0, count);
    }
}

void ChipmunkDebugDrawPoints(cpFloat size, int count, cpVect *verts, ccColor4B color){
#if CP_USE_DOUBLES
    glVertexPointer(2, GL_DOUBLE, 0, verts);
#else
    glVertexPointer(2, GL_FLOAT, 0, verts);
#endif
    
    glPointSize(size);
    glColor_from_color(color);
    glDrawArrays(GL_POINTS, 0, count);
}


void
ChipmunkDebugDrawBB(cpBB bb, ccColor4B color)
{
    cpVect verts[] = {
        cpv(bb.l, bb.b),
        cpv(bb.l, bb.t),
        cpv(bb.r, bb.t),
        cpv(bb.r, bb.b),
    };
    //ChipmunkDebugDrawPolygon(4, verts, LAColor(0, 0), color);
    ChipmunkDebugDrawPolygon(4, verts, ccc4(255, 255, 255, 255), color);
}

static void
drawObject(void *ptr, void *unused)
{
    cpShape *shape = (cpShape *)ptr;
    cpBody *body = shape->body;
    
    switch(shape->klass->type){
        case CP_CIRCLE_SHAPE:
            drawCircleShape(body, (cpCircleShape *)shape);
            break;
        case CP_SEGMENT_SHAPE:
            drawSegmentShape(body, (cpSegmentShape *)shape);
            break;
        case CP_POLY_SHAPE:
            drawPolyShape(body, (cpPolyShape *)shape);
            break;
        default:
            printf("Bad enumeration in drawObject().\n");
    }
}

static const GLfloat springVAR[] = {
    0.00f, 0.0f,
    0.20f, 0.0f,
    0.25f, 3.0f,
    0.30f,-6.0f,
    0.35f, 6.0f,
    0.40f,-6.0f,
    0.45f, 6.0f,
    0.50f,-6.0f,
    0.55f, 6.0f,
    0.60f,-6.0f,
    0.65f, 6.0f,
    0.70f,-3.0f,
    0.75f, 6.0f,
    0.80f, 0.0f,
    1.00f, 0.0f,
};
static const int springVAR_count = sizeof(springVAR)/sizeof(GLfloat)/2;

static void
drawSpring(cpDampedSpring *spring, cpBody *body_a, cpBody *body_b)
{
    cpVect a = cpvadd(body_a->p, cpvrotate(spring->anchr1, body_a->rot));
    cpVect b = cpvadd(body_b->p, cpvrotate(spring->anchr2, body_b->rot));
    
    glPointSize(5.0f);
    ccDrawPoint(a);
    ccDrawPoint(b);
    
    cpVect delta = cpvsub(b, a);
    
    // Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
    // Needed states:  GL_VERTEX_ARRAY,
    // Unneeded states: GL_TEXTURE_2D, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glVertexPointer(2, GL_FLOAT, 0, springVAR);
    glPushMatrix(); {
        GLfloat x = a.x;
        GLfloat y = a.y;
        GLfloat cos = delta.x;
        GLfloat sin = delta.y;
        GLfloat s = 1.0f/cpvlength(delta);
        
        const GLfloat matrix[] = {
            cos,    sin, 0.0f, 0.0f,
            -sin*s,  cos*s, 0.0f, 0.0f,
            0.0f,   0.0f, 1.0f, 0.0f,
            x,      y, 0.0f, 1.0f,
        };
        
        glMultMatrixf(matrix);
        glDrawArrays(GL_LINE_STRIP, 0, springVAR_count);
    } glPopMatrix();
    
    // restore default GL state
    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
}

static void
drawConstraint(cpConstraint *constraint)
{
    cpBody *body_a = constraint->a;
    cpBody *body_b = constraint->b;
    
    const cpConstraintClass *klass = constraint->klass;
    if(klass == cpPinJointGetClass()){
        cpPinJoint *joint = (cpPinJoint *)constraint;
        
        cpVect a = cpvadd(body_a->p, cpvrotate(joint->anchr1, body_a->rot));
        cpVect b = cpvadd(body_b->p, cpvrotate(joint->anchr2, body_b->rot));
        
        glPointSize(5.0f);
        ccDrawPoint(a);
        ccDrawPoint(b);
        ccDrawLine(a, b);
        
    } else if(klass == cpSlideJointGetClass()){
        cpSlideJoint *joint = (cpSlideJoint *)constraint;
        
        cpVect a = cpvadd(body_a->p, cpvrotate(joint->anchr1, body_a->rot));
        cpVect b = cpvadd(body_b->p, cpvrotate(joint->anchr2, body_b->rot));
        
        glPointSize(5.0f);
        ccDrawPoint(a);
        ccDrawPoint(b);
        ccDrawLine(a, b);
        
    } else if(klass == cpPivotJointGetClass()){
        cpPivotJoint *joint = (cpPivotJoint *)constraint;
        
        cpVect a = cpvadd(body_a->p, cpvrotate(joint->anchr1, body_a->rot));
        cpVect b = cpvadd(body_b->p, cpvrotate(joint->anchr2, body_b->rot));
        
        glPointSize(10.0f);
        ccDrawPoint(a);
        ccDrawPoint(b);
    } else if(klass == cpGrooveJointGetClass()){
        cpGrooveJoint *joint = (cpGrooveJoint *)constraint;
        
        cpVect a = cpvadd(body_a->p, cpvrotate(joint->grv_a, body_a->rot));
        cpVect b = cpvadd(body_a->p, cpvrotate(joint->grv_b, body_a->rot));
        cpVect c = cpvadd(body_b->p, cpvrotate(joint->anchr2, body_b->rot));
        
        glPointSize(5.0f);
        ccDrawPoint(c);
        ccDrawLine(a, b);
    } else if(klass == cpDampedSpringGetClass()){
        drawSpring((cpDampedSpring *)constraint, body_a, body_b);
    } else {
        //              printf("Cannot draw constraint\n");
    }
}

static void
drawShapeBB(cpShape *shape, void *unused)
{
    //ChipmunkDebugDrawBB(shape->bb, RGBAColor(0.3f, 0.5f, 0.3f, 1.0f));
    ChipmunkDebugDrawBB(shape->bb, ccc4BFromccc4F(ccc4f(0.3f, 0.5f, 0.3f, 1.0f)));
}


void
drawSpace(cpSpace *space, drawSpaceOptions *options)
{
    glLineWidth(options->lineThickness);
    if(options->drawShapes){
        cpSpatialIndexEach(space->activeShapes, (cpSpatialIndexIteratorFunc)drawObject, space);
        cpSpatialIndexEach(space->staticShapes, (cpSpatialIndexIteratorFunc)drawObject, space);
    }
    
    glLineWidth(1.0f);
    if(options->drawBBs){
        glColor4f(0.3f, 0.5f, 0.3f,1);
        cpSpatialIndexEach(space->activeShapes, (cpSpatialIndexIteratorFunc)drawShapeBB, NULL);
        cpSpatialIndexEach(space->staticShapes, (cpSpatialIndexIteratorFunc)drawShapeBB, NULL);
    }
    
    cpArray *constraints = space->constraints;
    
    glColor4f(0.5f, 1.0f, 0.5f, 1);
    for(int i=0, count = constraints->num; i<count; i++){
        drawConstraint((cpConstraint *)constraints->arr[i]);
    }
    
    if(options->bodyPointSize){
        glPointSize(options->bodyPointSize);
        
        cpArray *bodies = space->bodies;
        
        glColor4f(LINE_COLOR);
        for(int i=0, count = bodies->num; i<count; i++){
            cpBody *body = (cpBody *)bodies->arr[i];
            ccDrawPoint(body->p);
        }
        
        //                      glColor3f(0.5f, 0.5f, 0.5f);
        //                      cpArray *components = space->components;
        //                      for(int i=0; i<components->num; i++){
        //                              cpBody *root = components->arr[i];
        //                              cpBody *body = root, *next;
        //                              do {
        //                                      next = body->node.next;
        //                                      glVertex2f(body->p.x, body->p.y);
        //                              } while((body = next) != root);
        //                      }
    }
    
    if(options->collisionPointSize){
        glPointSize(options->collisionPointSize);
        cpArray *arbiters = space->arbiters;
        for(int i=0; i<arbiters->num; i++){
            cpArbiter *arb = (cpArbiter*)arbiters->arr[i];
            
            glColor4f(COLLISION_COLOR);
            for(int i=0; i<arb->numContacts; i++){
                cpVect v = arb->contacts[i].p;
                ccDrawPoint(v);
            }
        }
    }
}
