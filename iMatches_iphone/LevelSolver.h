//
//  LevelSolver.h
//  iMatches_iphone
//
//  Created by Admin on 12.06.2014.
//
//

#import "cocos2d.h"
#import "My_constants.h"
//#import "CCNode.h"

@interface LevelSolver : CCNode {
    
}


-(BOOL) equationStringSolved:(NSString*)equation;


-(int) computeStr:(NSString*)str;

-(NSString*) computeMultiplications:(NSString*) str;
-(NSString*) resolveMultString:(NSString*) str;

-(NSString*) computeDivisions:(NSString*) str;
-(NSString*) resolveDivString:(NSString*) str;

-(BOOL) isEqualCleared:(NSString*)equation;


@end
