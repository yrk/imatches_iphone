//
//  LevelPickerScene.m
//  iMatches_iphone
//
//  Created by Admin on 11.06.2014.
//
//

#import "LevelPickerScene.h"
#import "GameManager.h"

@implementation LevelPickerScene

@synthesize menuScene;
@synthesize panGestureRecognizer;

@synthesize levelManager;

-(id) init {
  if ((self = [super init])) {
    
    layer = [CCLayer node];
    //[self addNewLabel];
    
    [self initExitButton];
    
    CCLayerColor * bgLayer = [[[CCLayerColor alloc] initWithColor:ccc4(0, 0, 0, 255)] autorelease];
    [self addChild:bgLayer z:0];
    
    //[self panGestureRecognizerInit];
    
    levelManager = [GameManager instance].levelManager;
    
    [self addChild:layer z:10];
    
    [self setupPositions];
    
//    [self addTinyLevel];
    for (int i=0; i<12; i++) {
      [self addLevelLabel:[levelManager getLevelString:i] inPosition:ccp(20, 285-25*i)];
      [self addLevelLabel:[levelManager getLevelString:i+12] inPosition:ccp(20+250, 285-25*i)];
    }
  }
  
  return self;
}

-(void) addLevelLabel:(NSString*)levelString inPosition:(CGPoint)position {
  CCLabelTTF * newLabel = [[CCLabelTTF alloc] initWithString:levelString fontName:@"AppleGothic" fontSize:24];
  
  [newLabel setPosition:ccp([newLabel contentSize].width/2 + position.x, position.y)];
  [self addChild:newLabel];
  
}

// creates and adds level made from tiny matches for overview
-(void) addTinyLevel {
  CGSize size = [[CCDirector sharedDirector] winSize];
  
  CCSprite * tiny1 = [[CCSprite alloc] initWithFile:@"match1tiny.png"];
  CCSprite * tiny2 = [[CCSprite alloc] initWithFile:@"match2tiny.png"];
  CCSprite * tiny3 = [[CCSprite alloc] initWithFile:@"match3tiny.png"];
  CCSprite * tiny4 = [[CCSprite alloc] initWithFile:@"match4tiny.png"];
  
  //for (int i=1; i<10; i++) {
  [tiny1 setPosition:ccp(15+5, size.height/2)];
  [self addChild:tiny1];
  [tiny2 setPosition:ccp(35+5, size.height/2)];
  [self addChild:tiny2];
  [tiny3 setPosition:ccp(55+5, size.height/2)];
  [self addChild:tiny3];
  [tiny4 setPosition:ccp(75+5, size.height/2)];
  [self addChild:tiny4];
  
  //}
  
}

-(void) addSprite:(CGPoint)location withType:(int)type{
  NSString * string = [self tinyMatchType:type];
  CCSprite * sprite = [[CCSprite alloc] initWithFile:string];
  
  [sprite setPosition:location];
  [self addChild:sprite];
}

-(void) addSprite:(CGPoint)location withType:(int)type inPlace:(int)place {
  NSString * string = [self tinyMatchType:type];
  CCSprite * sprite = [[CCSprite alloc] initWithFile:string];
  
  CGPoint pos = ccp(location.x + equPositions[place].x, location.y+equPositions[place].y);
  
  [sprite setPosition:pos];
  [self addChild:sprite];
}

-(NSString*) tinyMatchType:(int)type {
  switch (type) {
    case 1:
      return @"match1tiny.png";
      break;
    case 2:
      return @"match2tiny.png";
    case 3:
      return @"match3tiny.png";
    case 4:
      return @"match4tiny.png";
    default:
      return nil;
      break;
  }
}

-(int) getTypeForSlot:(int)slot {
  switch (slot) {
    case 1: return 1;
    case 2: return 2;
    case 3: return 2;
    case 4: return 1;
    case 5: return 2;
    case 6: return 2;
    case 7: return 1;
    case 8: return 2;
    case 9: return 1;
    case 10: return 1;
    case 11: return 3;
    case 12: return 4;
      
    default:
      break;
  }
  return 0;
}

-(void) setupPositions {
  
  // match dimensions for convinience
  float mHeight = 32.0;
  float mWidth = 6.0;
  float mHalfHeight = 16.0;
  float mHalfWidth = 3.0;
  
  float x_offset = 10.0; // level offset
  float y_offset = 300.0;
  float x_space = 3.0; // space between characters
  
  for (int i=0; i<8; i++) {
    equPositions[i] = ccp((2*mWidth + mHeight+x_space+2)*i+x_offset, y_offset);
  }
  
  digitPositions[0] = CGPointZero;
  digitPositions[1] = ccp(0, 2+mWidth+mHeight);
  digitPositions[7] = ccp(0, -(2+mWidth+mHeight));
  digitPositions[2] = ccp(-(1+mHalfHeight+mHalfWidth), 1+mHalfWidth+mHalfHeight);
  digitPositions[3] = ccp(1+mHalfHeight+mHalfWidth, 1+mHalfWidth+mHalfHeight);
  digitPositions[5] = ccp(-(1+mHalfHeight+mHalfWidth), -(1+mHalfWidth+mHalfHeight));
  digitPositions[6] = ccp(1+mHalfHeight+mHalfWidth, -(1+mHalfWidth+mHalfHeight));
  
  digitPositions[4] = CGPointZero;
  digitPositions[8] = CGPointZero;
  digitPositions[11] = CGPointZero;
  digitPositions[12] = CGPointZero;
  digitPositions[9] = ccp(0, mWidth);
  digitPositions[10] = ccp(0, -mWidth);
}

-(void) drawMatchInSlot:(int)slot {
  [self addSprite:digitPositions[slot] withType:[self getTypeForSlot:slot]];
}

-(void) drawTinyLevelFromString:(NSString*)equString {
  for (int i = 0; i < [equString length]; ++i) {
    char element = [equString characterAtIndex:i];
    
/// TO BE CONTINUED
    
    //[self placeNextElement:element onLayer:levelLayer];
  }
}

//-(void)makeNumber:(int)num
-(void) makeLevelChar:(char)c {
  //[self addNumberBg];
  switch (c) {
    case '1':
      [self drawMatchInSlot:3];
      [self drawMatchInSlot:6];
      break;
    case '2':
      [self drawMatchInSlot:1];
      [self drawMatchInSlot:3];
      [self drawMatchInSlot:4];
      [self drawMatchInSlot:5];
      [self drawMatchInSlot:7];
      break;
    case '3':
      [self drawMatchInSlot:1];
      [self drawMatchInSlot:3];
      [self drawMatchInSlot:4];
      [self drawMatchInSlot:6];
      [self drawMatchInSlot:7];
      break;
    case '4':
      [self drawMatchInSlot:2];
      [self drawMatchInSlot:3];
      [self drawMatchInSlot:4];
      [self drawMatchInSlot:6];
      break;
    case '5':
      [self drawMatchInSlot:1];
      [self drawMatchInSlot:2];
      [self drawMatchInSlot:4];
      [self drawMatchInSlot:6];
      [self drawMatchInSlot:7];
      break;
    case '6':
      [self drawMatchInSlot:1];
      [self drawMatchInSlot:2];
      [self drawMatchInSlot:4];
      [self drawMatchInSlot:5];
      [self drawMatchInSlot:6];
      [self drawMatchInSlot:7];
      break;
    case '7':
      [self drawMatchInSlot:1];
      [self drawMatchInSlot:3];
      [self drawMatchInSlot:6];
      break;
    case '8':
      [self drawMatchInSlot:1];
      [self drawMatchInSlot:2];
      [self drawMatchInSlot:3];
      [self drawMatchInSlot:4];
      [self drawMatchInSlot:5];
      [self drawMatchInSlot:6];
      [self drawMatchInSlot:7];
      break;
    case '9':
      [self drawMatchInSlot:1];
      [self drawMatchInSlot:2];
      [self drawMatchInSlot:3];
      [self drawMatchInSlot:4];
      [self drawMatchInSlot:6];
      [self drawMatchInSlot:7];
      break;
    case '0':
      [self drawMatchInSlot:1];
      [self drawMatchInSlot:2];
      [self drawMatchInSlot:3];
      [self drawMatchInSlot:5];
      [self drawMatchInSlot:6];
      [self drawMatchInSlot:7];
      break;
    case '-':
      [self drawMatchInSlot:4];
      break;
    case '+':
      [self drawMatchInSlot:4];
      [self drawMatchInSlot:8];
      break;
    case '=':
      [self drawMatchInSlot:9];
      [self drawMatchInSlot:10];
      break;
    case '/':
      [self drawMatchInSlot:11];
      break;
      case '*':
      [self drawMatchInSlot:11];
      [self drawMatchInSlot:12];
      break;
    default:
      break;
  }
}



-(void) panGestureRecognizerInit {
  panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveLayer:)];
  [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:panGestureRecognizer];
}

-(void) moveLayer:(UIPanGestureRecognizer*)paramSender {
  if (paramSender.state == UIGestureRecognizerStateBegan) {
    lastLocation = [paramSender locationInView:paramSender.view.superview];
  }
  
  if (paramSender.state == UIGestureRecognizerStateChanged) {
    newLocation = [paramSender locationInView:paramSender.view.superview];
    difference = ccpSub(newLocation, lastLocation);
    
    //[aboutTextLabel setPosition:CGPointMake(aboutTextLabel.position.x, aboutTextLabel.position.y + difference.x)];
    
    lastLocation = newLocation;
  }
  
  if (paramSender.state == UIGestureRecognizerStateEnded ||
      paramSender.state == UIGestureRecognizerStateCancelled) {
    
  }
}

-(void) initExitButton {
  CGSize size = [[CCDirector sharedDirector] winSize];
  int buttonOffsetY = 50;
  
  CCSprite * exitButtonNormal = [CCSprite spriteWithFile:@"cancel-icon.png"];
  CCSprite * exitButtonSelected = [CCSprite spriteWithFile:@"cancel-solid-icon.png"];
  
  CCMenuItemSprite * exitMenuButton = [CCMenuItemSprite itemFromNormalSprite:exitButtonNormal selectedSprite:exitButtonSelected target:self selector:@selector(exitButtonPressed)];
  
  CCMenu * buttonMenu = [CCMenu menuWithItems:exitMenuButton, nil];
  //[buttonMenu setPosition:CGPointMake(size.width/2 + exitButtonNormal.contentSize.width/2, buttonOffsetY + exitButtonNormal.contentSize.height/2)];
  [buttonMenu setPosition:CGPointMake(3*size.width/4, buttonOffsetY + exitButtonNormal.contentSize.height/2)];
  //        [menubar alignItemsHorizontallyWithPadding:100];
  [self addChild:buttonMenu z:35];
}

-(void)exitButtonPressed {
  [[SimpleAudioEngine sharedEngine] playEffect:@"pstryk.wav"];
  
  CCTransitionMoveInB * moveInBot = [CCTransitionMoveInB transitionWithDuration:0.5f scene:menuScene];
  
  [[[CCDirector sharedDirector] openGLView] removeGestureRecognizer:panGestureRecognizer];
  
  [[CCDirector sharedDirector] replaceScene:moveInBot];
}

-(void) dealloc {
  
  [panGestureRecognizer release];
  
  [super dealloc];
}

@end
