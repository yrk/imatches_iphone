//
//  PhysicsWorks.h
//  iMatches_iphone
//
//  Created by Admin on 11.06.2014.
//
//

#import <Foundation/Foundation.h>

@protocol PhysicsWorks <NSObject>

-(void)updatePhysicsBodies;

@end
