//
//  Slot.h
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 20.11.2011.
//  Copyright 2011 yarkill@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCSprite.h"
#import "Matches_types.h"

@interface Slot : CCSprite {

    //CCSprite * sprite; // wskaznik do sprajta w slocie
    BOOL taken; // czy zajety czy nie 
    CGRect rect; // wymiary slotu do kolizji
    float x,y; // polozenie CGRect
    //Segment_type segment;

}


@property (nonatomic,readwrite,assign) CGRect rect;
@property (nonatomic,readwrite,assign) float x,y;
//@property (nonatomic,readwrite,assign) Segment_type segment;


@end

