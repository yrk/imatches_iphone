//
//  AppDelegate.m
//  iMatches_iphone
//
//  Created by Admin on 25.01.2014.
//  Copyright __MyCompanyName__ 2014. All rights reserved.
//




//
//  AppDelegate.m
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 11.10.2011.
//  Copyright yarkill@gmail.com 2011. All rights reserved.
//

#import "chipmunk.h"

#import "cocos2d.h"

#import "AppDelegate.h"
#import "GameConfig.h"
#import "GameLayer.h"
#import "LevelManager.h"
#import "RootViewController.h"

@implementation AppDelegate

@synthesize window;

- (void) removeStartupFlicker
{
	//
	// THIS CODE REMOVES THE STARTUP FLICKER
	//
	// Uncomment the following code if you Application only supports landscape mode
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
    /*
	CC_ENABLE_DEFAULT_GL_STATES();
	CCDirector *director = [CCDirector sharedDirector];
	CGSize size = [director winSize];
	CCSprite *sprite = [CCSprite spriteWithFile:@"Default.png"];
	sprite.position = ccp(size.width/2, size.height/2);
	sprite.rotation = -90;
	[sprite visit];
	[[director openGLView] swapBuffers];
	CC_ENABLE_DEFAULT_GL_STATES();
	*/
#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController
}
- (void) applicationDidFinishLaunching:(UIApplication*)application
{
	// Init the window
	window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	
	// Try to use CADisplayLink director
	// if it fails (SDK < 3.1) use the default director
	if( ! [CCDirector setDirectorType:kCCDirectorTypeDisplayLink] )
		[CCDirector setDirectorType:kCCDirectorTypeDefault];
	
	
    //    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    //    BOOL isDebugMode = [defaults boolForKey:@"debug_mode_is_on"];
    //    BOOL isBackgroundEnabled = [defaults boolForKey:@"background_is_on"];
    
    
	CCDirector *director = [CCDirector sharedDirector];
	
	// Init the View Controller
	viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
	viewController.wantsFullScreenLayout = YES;
	
    

    
	//
	// Create the EAGLView manually
	//  1. Create a RGB565 format. Alternative: RGBA8
	//	2. depth format of 0 bit. Use 16 or 24 bit for 3d effects, like CCPageTurnTransition
	//
	//
    /*
	EAGLView *glView = [EAGLView viewWithFrame:[window bounds]
								   pixelFormat:kEAGLColorFormatRGB565	// kEAGLColorFormatRGBA8
								   depthFormat:0						// GL_DEPTH_COMPONENT16_OES
						];
    */
    // usuwa warrningi
    EAGLView *glView = [EAGLView viewWithFrame:[window bounds]
								   pixelFormat:kEAGLColorFormatRGBA8
								   depthFormat:0						// GL_DEPTH_COMPONENT16_OES
						];
    /**/
 
	// attach the openglView to the director
	[director setOpenGLView:glView];
	
#define MY_RETINA_SETUP
    
#ifdef MY_RETINA_SETUP
    // Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	if( ! [director enableRetinaDisplay:YES] )
		CCLOG(@"Retina Display Not supported");
#endif
    
	//
	// VERY IMPORTANT:
	// If the rotation is going to be controlled by a UIViewController
	// then the device orientation should be "Portrait".
	//
	// IMPORTANT:
	// By default, this template only supports Landscape orientations.
	// Edit the RootViewController.m file to edit the supported orientations.
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
#else
	[director setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
#endif
	
	[director setAnimationInterval:1.0/60];
	[director setDisplayFPS:YES];
	
	
	// make the OpenGLView a child of the view controller
	[viewController setView:glView];
	
    
    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: viewController.view];
    }
    else
    {
        // use this mehod on ios6
        [window setRootViewController:viewController];
    }
    
	[window makeKeyAndVisible];
	
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
    //[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGB565];
	
	// Removes the startup flicker
	[self removeStartupFlicker];
    
    //scene = [GameScene node];
    
    [GameManager instance].menuScene = [MenuScene node];
    [GameManager instance].gameScene = [GameScene node];
    
    //menuScene = [MenuScene node];
    
    // just for init before all inits :D
    SimpleAudioEngine * soundEngine = [SimpleAudioEngine sharedEngine];
    [soundEngine playEffect:@"ntyk.wav"];
    //gameLayer = [GameLayer node];
    //[scene addChild:gameLayer z:10 tag:EGameLayerTag];
    
    //    menuLayer = [MenuLayer node];
    //    [scene addChild:menuLayer z:20 tag:EMenuLayerTag];
	
	// Run the intro Scene
    
//    NSString * fileName = [[NSBundle mainBundle] pathForResource:@"b1" ofType:@"png"];
//    NSLog(@"%@ : %@", @"path for b1.png ", fileName );
    
    

    
    // In applicationDidFinishLaunching, right before call to runWithScene
    cpInitChipmunk();
    
    
    
	[[CCDirector sharedDirector] runWithScene: [[GameManager instance]menuScene]];
}


- (void)applicationWillResignActive:(UIApplication *)application {
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    printf("jestem kurwa aktywowany do huja kurwa ======= !\n");
	[[CCDirector sharedDirector] resume];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationDidEnterBackground:(UIApplication*)application {
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application {
    //CCLayer* gameLayer = (GameLayer*) [scene getChildByTag:EGameLayerTag];
    //LevelManager * levelManager = (LevelManager*) [gameLayer getChildByTag:ELevelManagerTag];
    //[levelManager reloadConfig];
    
    /*
    
    [[gameLayer levelManager] reloadConfig];
    
    printf("Wrocilem z tla kurwa !\n");
     */

  
  CCDirector *director = [CCDirector sharedDirector];
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
#else
	[director setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
#endif
  
	[[CCDirector sharedDirector] startAnimation];
  
}

- (void)applicationWillTerminate:(UIApplication *)application {
	CCDirector *director = [CCDirector sharedDirector];
	
	[[director openGLView] removeFromSuperview];
    
    
	
	[viewController release];
	
	[window release];
	
	[director end];
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}




- (void)dealloc {
	[[CCDirector sharedDirector] end];
	[window release];
	[super dealloc];
}

@end













