//
//  LevelManager.h
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 03.03.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Digit.h"
#import "Number.h"
#import "My_constants.h"

//#import "LevelGenerator.h"
#import "LevelSolver.h"
//#import "MenuBar.h"


#import "chipmunk.h"

#import "PhysicsManager.h"

@class GameLayer;

/*
 Level Manager:
 - przechowuje levele w tablicy
 - udostepnia levele do przegladania
 + sprawdza wynik aktualnego levelu
 + zadaje kolejny level
 + ? pomaga ustawic obiekty na scenie/layerze ?
 */

@interface LevelManager : CCNode {
    
    UILongPressGestureRecognizer *lpgRecognizer;
    
    //LevelGenerator * levelSolver;
    LevelSolver * levelSolver;
    
    // level management :
    unsigned int max_levels;
    NSArray * levelsArray;
    
    NSString * current_level;
    
    unsigned int levelNumber;
    
    // layers for levels
    GameLayer * newLevelLayer;
    GameLayer * currentLevelLayer;
    GameLayer * prevLevelLayer;
    GameLayer * nextlevelLayer;
    
    CGSize size;
    int nextPos; // for placement
    
    
    CGPoint places[8];
    /*
    
    CCSprite * selSprite;
    CGPoint selPos;
    int selSlot;
    

    
    Digit * selDigit;

    SelectedSprite selectedSprite;

    
    //MenuBar * menuBar;
    
    CCArray* numbers;
    CCArray * equElements;
    
    Equation level;
    
    CGPoint oldLocation;
    CGPoint newLocation;
    
    CGPoint startTouchLocation;
    
    
    BOOL levelCleared;
    
    BOOL isDebugMode;
    BOOL isBackgroundEnabled;
    

    
    unsigned int levelNumber;
    
    */
    
    cpSpace * space;

}

//@property (nonatomic, strong) MenuBar * menuBar;
@property (nonatomic, retain) UILongPressGestureRecognizer *lpgRecognizer;
@property (nonatomic, assign) BOOL isLevelCleared;


-(void) initPlaces;

-(BOOL) checkResultForString:(NSString*) stringToCheck;

//-(void)setupNewLevel:(uint)newLevel forLayer:(GameLayer*) levelLayer;
-(void)setupNewLevelForLayer:(GameLayer*) levelLayer;


-(BOOL)placeElementsOnLayer:(GameLayer*)levelLayer forLevelString:(NSString*)equString;

-(void) placeNextElement:(char)element onLayer:(GameLayer*) levelGameLayer;


-(unsigned int) getMaxLevelCount;

-(NSString*)getLevelString:(int)level;


-(BOOL) isEqualClearedInLevel;

/*

// preconfiguration
//-(void) reloadConfig;

-(void) addBackground;
-(void) removeBackground;


// level managment
-(void) initLevel;

-(void) reset;
-(void) resetWithDemoLevel:(int)level;

-(BOOL) checkResult;
//-(BOOL) isLevelCleared;

-(BOOL) placeElementsOnScene:(NSString*)equString;
-(void) placeNextElement:(char)element;


// Touch handling
-(void) startTouch:(CGPoint)location;
-(void) moveTouch:(CGPoint)location;
-(void) endTouch:(CGPoint)location;
-(void) abortTouch;

-(void) selectSpriteForTouch:(CGPoint)touchLocation;
-(void) panForTranslation:(CGPoint)translation;

// returns slot to update and digit to update from parameter
-(int) getDigitCloseToTouch:(CGPoint)location toDigit:(Digit*)digit;
*/

@end
