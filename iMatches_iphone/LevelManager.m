//
//  LevelManager.m
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 03.03.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import "LevelManager.h"
#import "Operation.h"

#import "Equals.h"

#import "macros.h"
#import "My_constants.h"

#import "SimpleAudioEngine.h"
#import "EquationElement.h"

#import "GameLayer.h"

#define GAME_LAYER_DEBUG
#define BACKGROUND_ENABLED

//#define LEVEL_MAX 19
#define LEVEL_START 28

@implementation LevelManager

//@synthesize menuBar;
@synthesize lpgRecognizer;

@synthesize isLevelCleared;
/************************************************************************
//  loading config info
 ************************************************************************
-(void)reloadConfig {


    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    isDebugMode = [defaults boolForKey:@"debug_mode_is_on"];
    isBackgroundEnabled = [defaults boolForKey:@"background_is_on"];
    
    if (isBackgroundEnabled) {
        [self addBackground];
    } else {
        [self removeBackground];
    }
    
//    [self scheduleUpdate];
}
*/


//-(void)update:(ccTime)delta {
//
//}
/*
-(void)addBackground:(CCLayer *) layer {
#ifdef BACKGROUND_ENABLED
    CCSprite * bg = [CCSprite spriteWithFile:@"imatches-bg1.png"];
    [bg setPosition:ccp(size.width/2, size.height/2)];
    [layer addChild:bg z:0 tag:EBackgroundTag];
#endif
}

-(void)removeBackground {
        [self removeChildByTag:EBackgroundTag cleanup:YES];
}

*/

-(id)init //WithSpace:(cpSpace*)newSpace
{
    self = [super init];
    if (self) {
        
        //levelNumber = 1;
        levelNumber = LEVEL_START;
        
        space = [[PhysicsManager instance] space];

        levelSolver = [[LevelSolver alloc] init];
        nextPos = 0;
        
        
        
        
        NSString * levelsFilePath = [[NSBundle mainBundle] pathForResource:@"iMatchesConstLevels" ofType:@"plist"];
        levelsArray = [[NSArray alloc] initWithContentsOfFile:levelsFilePath];
        max_levels = [levelsArray count];
        
        
/*
        // Non transparent background fix
        CCLayerColor * bgLayer = [[[CCLayerColor alloc] initWithColor:ccc4(0, 0, 0, 255)] autorelease];
        [self addChild:bgLayer z:0];
*/
        size = [[CCDirector sharedDirector] winSize];
        [self initPlaces];
/*
        levelNumber = LEVEL_START;
        
        //levelCleared = NO;

                
        numbers = [[CCArray alloc] initWithCapacity:2];
        
        equElements = [[CCArray alloc] init];
        
        [self initLevel];
        
        
 */
        
    }
    return self;
}

-(void) initPlaces {
    printf("init places loaded....\n");
  /*
    float part = size.width/4;
   
    for (int i=0; i < 4; ++i) {
        places[i] = ccp(part*i + half_ne_width , size.height - half_ne_height);
        places[i+4] = ccp(part*i + half_ne_width , half_ne_height);
    }
  */
  
  // const float half_ne_width = NUMBER_ELEMENT_WIDTH * 0.5;
  // const float half_ne_height = NUMBER_ELEMENT_HEIGHT * 0.5;
  /// new setup of places :
  float offset = 5;
//  float next_place_x = NUMBER_ELEMENT_WIDTH+8;
//  float next_place_y = NUMBER_ELEMENT_HEIGHT+8;
  
  float next_place_x = NUMBER_ELEMENT_WIDTH+12;
  float next_place_y = NUMBER_ELEMENT_HEIGHT+12;
  
  for (int i=0; i < 4; ++i) {
    places[i] = ccp(offset + next_place_x * i + half_ne_width , size.height - half_ne_height - offset);
    places[i+4] = ccp(offset + next_place_x * i + half_ne_width , size.height - next_place_y - half_ne_height-offset);
  }
}

/*
-(void)prepareNewLevelLayer:(CCLayer *) levelLayer {
    if (levelLayer != nil) {
        [levelLayer removeAllChildrenWithCleanup:YES];
    } else {
        levelLayer = [CCLayer node];
    }
    
    [self addBackground];
    
    
}
*/

-(void)dealloc {
    
    [newLevelLayer removeAllChildrenWithCleanup:YES];
    [newLevelLayer release];
    [currentLevelLayer removeAllChildrenWithCleanup:YES];
    [currentLevelLayer release];
    [prevLevelLayer removeAllChildrenWithCleanup:YES];
    [prevLevelLayer release];
    [nextlevelLayer removeAllChildrenWithCleanup:YES];
    [nextlevelLayer release];

    [levelSolver release];
    [levelsArray release];
    
    [super dealloc];
}
/*
-(void)resetSelectedSprite {
    selectedSprite.digit = nil;
    selectedSprite.position = CGPointZero;
    selectedSprite.slot = 0;
    selectedSprite.sprite = nil;
    selectedSprite.updated = NO;
}

-(void) update {
    for (Digit* d in numbers) {
        [d update];
    }
    
    for (EquationElement * op in equElements) {
        [op update];
    }
    
}

-(void)reset {
    levelCleared = YES;
    

    for (EquationElement* number in equElements) {
        [number removeFromParentAndCleanup:YES];
        
    }
    
    [numbers removeAllObjects];
    [equElements removeAllObjects];

    // using random values from 1 to 4
    //    int demo = 1+ CCRANDOM_0_1() * 4;
    
    nextPos = 0;
}
*/

-(NSString*)getLevelString:(int)level {

    NSString * new_level;
    
    if (level <= max_levels) {
        new_level = levelsArray[level];
    } else {
        new_level = @"1+3=2";
    }
    current_level = new_level;
    return new_level;
}

-(unsigned int) getMaxLevelCount {
    return max_levels;
}

-(BOOL) isEqualClearedInLevel {
    return [levelSolver isEqualCleared:current_level ];
}


//-(void)resetWithDemoLevel:(int)newLevel {
//-(void)setupNewLevel:(uint)newLevel forLayer:(GameLayer*) levelLayer {
-(void)setupNewLevelForLayer:(GameLayer*) levelLayer {
    
    NSLog(@"setupNewLevel is working...");
    
    

    
    //NSString * newLevelString = [levelGenerator generateLevelString:newLevel];
    // getLevelsFromPlist
    //NSString * newLevelString = [levelGenerator getLevelsFromPlist:newLevel];
    NSString * newLevelString = [self getLevelString:levelNumber];
    
    [levelSolver equationStringSolved:newLevelString];
    
    NSLog(@"New level is : %@", newLevelString);
    
    printf("LEVEL number is : %d\n", levelNumber);
    
    [self placeElementsOnLayer:levelLayer forLevelString:newLevelString];

    if (levelNumber < [self getMaxLevelCount]) {
        ++levelNumber;
    } else levelNumber = 1;
    
    NSLog(@"setupNewLevel is finished");
    nextPos = 0;
}

//-(BOOL)placeElementsOnScene:(NSString*)equString {
-(BOOL)placeElementsOnLayer:(GameLayer*)levelLayer forLevelString:(NSString*)equString {
    
    printf("\n place elements on scene is working hard right now ... \n\n");
    
    for (int i = 0; i < [equString length]; ++i) {
        char element = [equString characterAtIndex:i];

        //NSLog(@"level string character is : %c", element);
        //NSLog(@"Placing element [ %c ] on scene ...", element);
        
        [self placeNextElement:element onLayer:levelLayer];
    }
    
    return YES; // all elements fits the scene, we are good :)
}

//-(void) placeNextElement:(char)element {
-(void) placeNextElement:(char)element onLayer:(GameLayer*) levelGameLayer {
    //static int nextPos = 0;

    Operation * plusSign;
    Operation * minusSign;
    Operation * divSign;
    Operation * mulSign;
    
    Operation * equals;
    //Equals * equals;
    Digit * digit;
    
    switch (element) {
        case '1': case '2': case '3':
        case '4': case '5': case '6':
        case '7': case '8': case '9':
        case '0' :
            digit = [[Digit alloc] init];
            //digit = [[Digit alloc] initWithSpace:space];
            [digit makeNumber:(element-'0')];
            //NSLog(@"element - '0' = %d", element - '0');
            [digit setPosition:places[nextPos]];
            
            [levelGameLayer addNewEquationElement:digit];
            break;
            
        case '-':
            minusSign = [[[Operation alloc] initWithOp:EMinusOperator] autorelease];
            [minusSign setPosition:places[nextPos]];
            [levelGameLayer addNewEquationElement:minusSign];
            break;
            
        case '+':
            plusSign = [[[Operation alloc] initWithOp:EPlusOperator] autorelease];
            [plusSign setPosition:places[nextPos]];
            [levelGameLayer addNewEquationElement:plusSign];
            break;

        case '/':
            divSign = [[[Operation alloc] initWithOp:EDivOperator] autorelease];
            [divSign setPosition:places[nextPos]];
            [levelGameLayer addNewEquationElement:divSign];
            break;

        case '*':
            mulSign = [[[Operation alloc] initWithOp:EMulOperator] autorelease];
            [mulSign setPosition:places[nextPos]];
            [levelGameLayer addNewEquationElement:mulSign];
            break;

        case '=':
            equals = [[[Operation alloc] initWithOp:EEquOperator] autorelease];
            [equals setPosition:places[nextPos]];
            [levelGameLayer addNewEquationElement:equals];
            break;
            
        default:
            break;
    }
    nextPos++;    
}

-(BOOL) checkResultForString:(NSString*) stringToCheck {
    
    BOOL result = [levelSolver equationStringSolved:stringToCheck];
    
    if (result == YES) {
        // update positions of all matches to world positions for its bodies
        
        
       //[self updatePhysicsBodies];
    }
    
    //[current_level release];
    return result;
}

@end
