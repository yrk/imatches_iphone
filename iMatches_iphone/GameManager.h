//
//  GameManager.h
//  iMatches_iphone
//
//  Created by Admin on 28.01.2014.
//
//

#import "CCNode.h"

@class GameLayer;
@class MenuScene;
@class GameScene;
@class LevelManager;
//@class MenuBar;

#include "MenuBar.h"

@interface GameManager : CCNode {

/*
    GameScene * gameScene;
    GameLayer * gameLayer;
    MenuScene * menuScene;
 */
}

@property (nonatomic, retain) GameScene * gameScene;
@property (nonatomic, retain) GameLayer * gameLayer;
@property (nonatomic, retain) MenuScene * menuScene;
@property (nonatomic, retain) MenuBar * menuBar;
@property (nonatomic, retain) LevelManager * levelManager;

+(GameManager*)instance;


@end
