//
//  GameManager.m
//  iMatches_iphone
//
//  Created by Admin on 28.01.2014.
//
//

#import "GameManager.h"
#import "MenuBar.h"
#import "LevelManager.h"

static GameManager * inst = nil;

@implementation GameManager

@synthesize gameScene;
@synthesize gameLayer;
@synthesize menuScene;
@synthesize menuBar;

@synthesize levelManager;

+(GameManager*)instance {
    @synchronized(self) {
        if (!inst) {
            inst = [[GameManager alloc] init];
        }
    }
    
    return inst;
}

-(id)init {
    if((self = [super init])) {
      
      if (levelManager == nil) {
        levelManager = [[LevelManager alloc] init];
      }
        
    }
    
    return self;
}

-(void)dealloc {
    [menuBar release];
    
    [super dealloc];
}
@end
