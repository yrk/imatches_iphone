//
//  Number.h
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 01.03.2012.
//  Copyright (c) 2012 yarkill@gmail.com. All rights reserved.
//

#import "cocos2d.h"

#import "CCNode.h"
#import "Digit.h"

#import "My_constants.h"

@interface Number : CCNode {

    unsigned int how_many_digits;
    NumberType numberType;
    CCArray* digits;
   
    EquationElementType equElementType;

}

// make 1 digit number
-(void)makeNumber1:(int)num;
// make 2 digit number
-(void)makeNumber2:(int)num;
// make 3 digit number
-(void)makeNumber3:(int)num;
// conviniece method for creating 1-3 digit number automatically
-(void)makeNumber:(int)num;

// reset number, remove all digits, remove all childs, reset values and arrays
-(void)reset;

// create object with desired number using [self makeNumber: int] method
-(id)initWithNumber:(int)num;
-(id)initWithNumber:(int)num andType:(EquationElementType)etype;

-(void)setEquElementType:(EquationElementType)etype;
-(EquationElementType)getEquElementType;

-(SelectedSprite)getMatchedSlot:(CGPoint)location;

-(int)getValue;

-(Digit*)getDigitToProcessTouch:(CGPoint)location;



@end
