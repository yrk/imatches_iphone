//
//  ChipmunkSprite.h
//  iMatches_iphone
//
//  Created by Admin on 27.01.2014.
//
//

#import "CCSprite.h"

#import "chipmunk.h"

@interface ChipmunkSprite : CCSprite {


    cpBody *body;
    cpShape *shape;
    cpSpace *space;
    BOOL canBeDestroyed;
    
    
}



@property (assign) cpBody *body;
@property (nonatomic, readwrite) CGPoint someLocation;

- (id)initWithSpace:(cpSpace *)theSpace location:(CGPoint)location spriteFileName:(NSString *)spriteFileName;
- (void)update;
- (void)createBodyAtLocation:(CGPoint)location;
- (void)destroy;

@end
