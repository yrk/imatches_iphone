//
//  MySprite.m
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 15.11.2011.
//  Copyright 2011 yarkill@gmail.com. All rights reserved.
//

#import "MatchSprite.h"

@implementation MatchSprite

//@synthesize last_loc;
//@synthesize match_type;
//@synthesize segment;



//- (id)initWithSpace:(cpSpace *)theSpace location:(CGPoint)location forSlot:(uint)slot {
//- (id)initWithLocation:(CGPoint)location forSlot:(uint)slot {
- (id)initWithLocation:(CGPoint)location forSlot:(uint)slot {

    current_location = location;
    
   NSString * matchFileName;
   switch (slot) {
        case 1: case 4: case 7: case match_horizontal:
            matchFileName = @"match1small.png";
            break;
            
       case 2: case 3: case 5: case 6: case match_vertical:
            matchFileName = @"match2small.png";
            break;
           
       case match_slash:
           matchFileName = @"match3.png";
           break;
        
       case match_backslash:
           matchFileName = @"match4.png";
           break;
           
    }
    
    cpSpace * theSpace = [[PhysicsManager instance] space];
    
    if (self = [super initWithSpace:theSpace location:location spriteFileName:matchFileName] ) {
    
        // Initialization code here.
    
    }
    
    return self;
}


-(void)updateBody {
    body->p = [self convertToWorldSpace:self.position];
    [self update];
}


- (void)dealloc {
    [super dealloc];
}

@end
