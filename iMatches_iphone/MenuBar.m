//
//  MenuBar.m
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 24.07.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import "MenuBar.h"
#import "LevelManager.h"
#import "macros.h"
#import "GameManager.h"


@implementation MenuBar

-(id) init {
    if (self = [super init]) {
        
        size = [[CCDirector sharedDirector] winSize];
        
        
        menuBarTop = [CCLayer node];
        menuBarBottom = [CCLayer node];
        
        [self addChild:menuBarTop];
        [self addChild:menuBarBottom];
        
        [menuBarBottom setPosition:CGPointMake(0, -size.height/4)];
        [menuBarTop setPosition:CGPointMake(0, size.height)];
        
        menuBarBackgroundBottom = [CCSprite spriteWithFile:@"menubar.png"];
        [menuBarBackgroundBottom setPosition:CGPointMake(size.width/2, menuBarBackgroundBottom.contentSize.height/2)];

        [menuBarBackgroundBottom setOpacity:150];
        //[self addChild:menuBarBackgroundBottom z:30];
        [menuBarBottom addChild:menuBarBackgroundBottom z:30];
        
        
        menuBarBackgroundTop = [CCSprite spriteWithFile:@"menubar.png"];
        [menuBarBackgroundTop setPosition:CGPointMake(size.width/2, menuBarBackgroundTop.contentSize.height/2)];
        
        [menuBarBackgroundTop setOpacity:150];
        //[self addChild:menuBarBackgroundTop z:30];
        [menuBarTop addChild:menuBarBackgroundTop z:30];
        
        gameScene = [[GameManager instance] gameScene];
        
        //[self addButtons];
     
        [self prepareActions];
        
        [self addButtons];
        
        
        
        scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Game time : %.1f",0.0] fontName:@"AppleGothic" fontSize:14];
        //[scoreLabel setPosition:CGPointMake(size.width - scoreLabel.contentSize.width, size.height- scoreLabel.contentSize.height)];
        [scoreLabel setPosition:CGPointMake(15+ scoreLabel.contentSize.width/2,scoreLabel.contentSize.height)];
        [menuBarTop addChild:scoreLabel z:30];
        
        levelTimeLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Level time : %.1f", 0.0] fontName:@"AppleGothic" fontSize:14];
        //[levelTimeLabel setPosition:CGPointMake(10 + levelTimeLabel.contentSize.width, size.height- levelTimeLabel.contentSize.height)];
        [levelTimeLabel setPosition:CGPointMake(scoreLabel.contentSize.width + levelTimeLabel.contentSize.width, levelTimeLabel.contentSize.height)];
        [menuBarTop addChild:levelTimeLabel z:30];
    }

    return self;
}

-(void)updateTimeElapsed:(float) timeElapsed andLevelTime:(float) levelTimeElapsed {
    
//    [scoreLabel setString:[NSString stringWithFormat:@"Game time : %.1f", timeElapsed]];
//    [levelTimeLabel setString:[NSString stringWithFormat:@"Level time : %.1f", levelTimeElapsed]];
}

/*
-(id) initForParent:(LevelManager*)parent {
    //levelManager = [parent retain];
    levelManager = parent;

    CCLOG(@" levelManager = %@", levelManager);
    
    [self init];
    
  //  [self addButtons];
    
    return self;
}
*/

-(void) dealloc {
    [menuBarUp release];
    [menuBarDown release];
    
    
    [menuBarBottomShow release];
    [menuBarBottomHide release];
    [menuBarTopShow release];
    [menuBarTopHide release];
    
    [menuBarTop removeAllChildrenWithCleanup:YES];
    [menuBarTop release];
    [menuBarBottom removeAllChildrenWithCleanup:YES];
    [menuBarBottom release];
    
    //[levelManager release];
    [super dealloc];
}

-(void) menuBarAction {
    if (menuBarIsHidden) {

    } else {

    }
    menuBarIsHidden = !menuBarIsHidden;
    NSLog(@"%@ : %i", @"MenuBarIsHidden value ", menuBarIsHidden);
}

-(BOOL) isHidden {
    return menuBarIsHidden;
}

-(void) menuBarHide {
    NSLog(@"menuBar shuld go down now....");
    //[self runAction:menuBarDown];
    //menuBarIsDown = YES;
    
    // stopping all actions:
    [menuBarBottom stopAllActions];
    [menuBarTop stopAllActions];
    
    [menuBarBottom runAction:menuBarBottomHide];
    [menuBarTop runAction:menuBarTopHide];
    
    menuBarIsHidden = YES;
}

-(void) menuBarShow {
    NSLog(@"menuBar shuld go up here....");
    //[self runAction:menuBarUp];
    //menuBarIsDown = NO;
    
    // stopping all actions:
    [menuBarBottom stopAllActions];
    [menuBarTop stopAllActions];
    
    [menuBarBottom runAction:menuBarBottomShow];
    [menuBarTop runAction:menuBarTopShow];

    menuBarIsHidden = NO;
}


-(void) prepareActions {
    //menuBarUp = [[CCMoveTo actionWithDuration:0.5 position:CGPointMake(0, 0)] retain];
    //menuBarDown = [[CCMoveTo actionWithDuration:0.5 position:CGPointMake(0, -size.height/4)] retain];
    
    menuBarBottomShow = [[CCMoveTo actionWithDuration:0.5 position:CGPointMake(0, 0)] retain];
    menuBarBottomHide = [[CCMoveTo actionWithDuration:0.5 position:CGPointMake(0, -size.height/4)] retain];
    
    menuBarTopShow = [[CCMoveTo actionWithDuration:0.5 position:CGPointMake(0, 3*size.height/4)] retain];
    menuBarTopHide = [[CCMoveTo actionWithDuration:0.5 position:CGPointMake(0, size.height)] retain];
    
    //        CCEaseInOut
    
//    menuBarIsDown = YES;
    menuBarIsHidden = YES;
}

-(void) backButtonPressed {
    CCLOG(@"%@ - %@ ", self, NSStringFromSelector(_cmd));
    [gameScene backButtonPressed];
}

-(void) forwardButtonPressed {
    //CCLOG(@"%@ - %@ ", self, _CMD);
    [gameScene forwardButtonPressed];
}

-(void) menuButtonPressed {
    CCLOG(@"%@ - %@ ", self, _CMD);
    [gameScene menuButtonPressed];
}

-(void) addButtons {
    
    CCSprite * backButtonNormal = [CCSprite spriteWithFile:@"rewind-icon.png"];
    CCSprite * backButtonSelected = [CCSprite spriteWithFile:@"rewind-icon-solid.png"];
    
    CCMenuItemSprite * backButton = [CCMenuItemSprite itemFromNormalSprite:backButtonNormal selectedSprite:backButtonSelected target:self selector:@selector(backButtonPressed)];
    
    
//    CCSprite * forwardButtonNormal = [CCSprite spriteWithFile:@"f1.png"];
    CCSprite * forwardButtonSelected = [CCSprite spriteWithFile:@"fast-forward-icon-solid.png"];
    
    CCSprite * forwardButtonNormal = [CCSprite spriteWithFile:@"fast-forward-icon.png"];
    
    CCMenuItemSprite * forwardButton = [CCMenuItemSprite itemFromNormalSprite:forwardButtonNormal selectedSprite:forwardButtonSelected target:self selector:@selector(forwardButtonPressed)];
    
    
    CCSprite * menuButtonNormal = [CCSprite spriteWithFile:@"eject-icon.png"];
    CCSprite * menuButtonSelected = [CCSprite spriteWithFile:@"eject-icon-solid.png"];
    
    CCMenuItemSprite * menuButton = [CCMenuItemSprite itemFromNormalSprite:menuButtonNormal selectedSprite:menuButtonSelected target:self selector:@selector(menuButtonPressed)];
    
    
    
    CCMenu * menubar = [CCMenu menuWithItems:backButton, menuButton, forwardButton, nil];
//    menubar.position = CGPointMake(size.width/2, size.height/2);
  //CGSize button_size = [menuButtonNormal contentSize];
  
    //[menubar setPosition:CGPointMake(size.width/2, menuBarBackgroundBottom.contentSize.height/2)];
  [menubar setPosition:CGPointMake(size.width/2, menuButtonNormal.contentSize.height/2 +1)];
    [menubar alignItemsHorizontallyWithPadding:100];
    [menuBarBottom addChild:menubar z:35];
//    [menuBarBackgroundBottom addChild:menubar z:35];
}




@end
