//
//  MenuBar.h
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 24.07.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class LevelManager;
@class GameScene;

@interface MenuBar : CCLayer {
    
    CGSize size;
    
    CCSprite * menuBarBackgroundBottom;
    CCSprite * menuBarBackgroundTop;
//    CCSprite * menuBarTop;
    
    LevelManager* levelManager;
    GameScene * gameScene;
    
    CCAction * menuBarUp;
    CCAction * menuBarDown;
    BOOL menuBarIsDown;
    
    CCLayer * menuBarTop;
    CCLayer * menuBarBottom;
    
    CCAction * menuBarBottomShow;
    CCAction * menuBarBottomHide;
    CCAction * menuBarTopShow;
    CCAction * menuBarTopHide;
    BOOL menuBarIsHidden;

    
    CCLabelTTF * scoreLabel;
    CCLabelTTF * levelTimeLabel;
    
//    BOOL isForwardButtonPressed;
//    BOOL isBackButtonPressed;
//    BOOL isMenuButtonPressed;
    
}

-(void)updateTimeElapsed:(float) timeElapsed andLevelTime:(float) levelTimeElapsed;
//-(id) initForParent:(LevelManager*)parent;

-(void) addButtons;

-(void) prepareActions;

/// to be deleted
-(void) menuBarAction;


-(void) menuBarHide;
-(void) menuBarShow;

-(BOOL) isHidden;
   

-(void) backButtonPressed;
-(void) forwardButtonPressed;
-(void) menuButtonPressed;


@end
