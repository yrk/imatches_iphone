//
//  Equals.m
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 01.03.2012.
//  Copyright (c) 2012 yarkill@gmail.com. All rights reserved.
//

#import "Equals.h"
#import "My_constants.h"


@implementation Equals

-(id)init {
    self = [super init];
    if(self) {
        
        matches = [[CCArray alloc] init];
        
        //CCSprite *match = [CCSprite spriteWithFile:@"match1.png"];
        //[match setPosition:ccp(0, MATCH_WIDTH)];
        MatchSprite * match = [[MatchSprite alloc] initWithLocation:ccp(0, MATCH_WIDTH) forSlot:match_horizontal];
        [match setPosition:ccp(0, MATCH_WIDTH)];
        [matches addObject:match];
        [self addChild:match z:20 tag:1];
        
        //match = [CCSprite spriteWithFile:@"match1.png"];
        //[match setPosition:ccp(0, -MATCH_WIDTH)];
        match = [[MatchSprite alloc] initWithLocation:ccp(0, -MATCH_WIDTH) forSlot:match_horizontal];
        [match setPosition:ccp(0, -MATCH_WIDTH)];
        [matches addObject:match];
        [self addChild:match z:20 tag:2];
    }
    
    return self;
}


// Changer methods - double tap on operator to change into other with same amount of matches
-(void) makePlus {

}



// changes '=' to '+'
-(void) changeEqualsToPlus {
    for (MatchSprite * match in matches) {
        [match setPosition:CGPointMake(0, 0)];
        [match release];
    }
    [matches removeAllObjects];
    
    
}

// changes '+' to '-'
-(void) changeToEquals {
    
}

-(void)freakEffect {
    /*
    CGSize size = [[CCDirector sharedDirector] winSize];
    CGPoint target = CGPointMake(size.width/2, size.height/2);
    rotRight = [CCRotateBy actionWithDuration:0.5 angle:360.0];
    CCMoveTo * moveTo = [CCMoveTo actionWithDuration:0.5 position:target];
*/
    //[self runAction:moveTo];
    //[self runAction:rotRight];
    //CCSequence * rotSeq = [CCSequence actions:rotLeft, rotCenter, rotRight, rotCenter, nil];
    //[self runAction:[CCRepeatForever actionWithAction:rotSeq]];
    //CCRepeat actionWithAction:<#(CCFiniteTimeAction *)#> times:<#(NSUInteger)#>
}

-(void)updatePhysicsBodies {
    //[self freakEffect];
    
    for (MatchSprite* m in matches) {
        [m update];
    //    [m runAction:rotRight];
    }

}

-(char) getCharValue {
    return '=';
}

-(void)dealloc {
    [matches removeAllObjects];
    [matches release];
    
    [super dealloc];
}

@end
