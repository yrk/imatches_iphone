//
//  Digit.m
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 20.11.2011.
//  Copyright 2011 yarkill@gmail.com. All rights reserved.
//

#import "Digit.h"
#import "CCDrawingPrimitives.h"
#import "My_constants.h"

@implementation Digit

#define MATCH_DBG

-(void)initPoints
{
  points[1] = CGPointMake(0,half_ne_height-half_match_width);
  points[2] = CGPointMake(-half_ne_width+half_match_width, half_ne_height/2);
  points[3] = CGPointMake(half_ne_width-half_match_width, half_ne_height/2);
  points[4] = CGPointMake(0, 0);
  points[5] = CGPointMake(-half_ne_width+half_match_width, -half_ne_height/2);
  points[6] = CGPointMake(half_ne_width-half_match_width, -half_ne_height/2);
  points[7] = CGPointMake(0, -half_ne_height+half_match_width);
  
}
-(CGRect)makeRectFromPoints:(CGPoint)pos andSize:(CGPoint)size
{
  return CGRectMake(pos.x, pos.y, size.x, size.y);
}
-(void)initSlots
{
  CGPoint slot2 = CGPointMake(MATCH_WIDTH, MATCH_HEIGHT);
  CGPoint slot1 = CGPointMake(MATCH_HEIGHT, MATCH_WIDTH);
  
  slots[1] = [self makeRectFromPoints:points[1] andSize:slot1];
  slots[2] = [self makeRectFromPoints:points[2] andSize:slot2];
  slots[3] = [self makeRectFromPoints:points[3] andSize:slot2];
  slots[4] = [self makeRectFromPoints:points[4] andSize:slot1];
  slots[5] = [self makeRectFromPoints:points[5] andSize:slot2];
  slots[6] = [self makeRectFromPoints:points[6] andSize:slot2];
  slots[7] = [self makeRectFromPoints:points[7] andSize:slot1];
  
  int i;
  for (i=0; i<8; ++i)
    slotsTaken[i] = NO;
  
}

-(NSString*) getMatchStringFromSlot:(int)slot {
  return [self getMatchStringFromSlot:slot withBg:NO];
}

-(NSString*) getMatchStringFromSlot:(int)slot withBg:(BOOL)bg {
  NSString * match_str_1;
  NSString * match_str_2;
  
  if (bg == YES) {
    match_str_1 = @"match-bg1small.png";
    match_str_2 = @"match-bg2small.png";
  } else {
    match_str_1 = @"match1.png";
    match_str_2 = @"match2.png";
  }
  NSString * match_str;
  switch (slot) {
    case 1: case 4: case 7:
      match_str = match_str_1;
      break;
      
    default:
      match_str = match_str_2;
      break;
  }
  return match_str;
}

-(void)addBackgroundMatchNumber:(int)slot {
  NSString * match_str = [self getMatchStringFromSlot:slot withBg:YES];
  
  CCSprite *match_bg = [CCSprite spriteWithFile:match_str];
  [match_bg setPosition:points[slot]];
  [match_bg setOpacity:90];
  
  [self addChild:match_bg z:15];
}

-(void)update:(ccTime)dt {
  
  if (slotHighlighted) {
    [self changeHighlightOpacity];
  }
  
}

-(void) clearHighlight {
  if (slotHighlighted) {
    slotHighlighted = NO;
    
    if (highlightMatch != nil) {
      [highlightMatch removeFromParentAndCleanup:YES];
    }
    slotToHighlight = 0;
    
    highlightOpacityAscending = YES;
  }
}

-(void) highlightSlot:(int) slot {
  if (slot == 0 ) return;
  
  
  if (slot != slotToHighlight) {
    [self clearHighlight];
  }
  
  if (!slotHighlighted) {
    slotToHighlight = slot;
    slotHighlighted = YES;
    
    highlightOpacityAscending = YES;
    
    highlightMatch = [[MatchSprite alloc] initWithLocation:points[slot] forSlot:slot];
    [highlightMatch setPosition:points[slot]];
    [highlightMatch setOpacity:highlightOpacity];
    
    [self addChild:highlightMatch z:25 tag:slot];
  }
}



-(void) changeHighlightOpacity {
  if (highlightOpacity <140 && highlightOpacityAscending) {
    highlightOpacity += 4;
  } else if (highlightOpacityAscending) {
    highlightOpacityAscending = NO;
  }
  
  if (!highlightOpacityAscending && highlightOpacity > 50) {
    highlightOpacity -= 4;
  } else if (!highlightOpacityAscending) {
    highlightOpacityAscending = YES;
  }
  
  //CCLOG(@"highlightOpacity : %d   ascending: %d",highlightOpacity, highlightOpacityAscending);
  [highlightMatch setOpacity:highlightOpacity];
  
}

-(void)addMatchNumber:(int)slot
{
  
  //NSString * txt = [[PhysicsManager instance] text];
  
  //NSLog(@"%@ : %@", @"wiadomosc od PhysicsManager z Digit", txt);
  //NSString * match_str = [self getMatchStringFromSlot:slot];
  
  //CGPoint absolutePos = CGPointMake(self.position.x + points[slot].x, self.position.y + points[slot].y);
  
  // match with chipmunk physics
  //MatchSprite * match = [[MatchSprite alloc] initWithSpace:space location:points[slot] forSlot:slot];
  MatchSprite * match = [[MatchSprite alloc] initWithLocation:points[slot] forSlot:slot];
  
  //MatchSprite * match = [[MatchSprite alloc] initWithSpace:space location:absolutePos forSlot:slot];
  
  //    CCSprite *match = [CCSprite spriteWithFile:match_str];
  [match setPosition:points[slot]];
  //NSLog(@"%@ : %f, %f", @"match.someLocation", match.someLocation.x, match.someLocation.y);
  //NSLog(@"%@ : %f, %f", @"match.position", match.position.x, match.position.y);
  
  [self addChild:match z:25 tag:slot];
  
  [matches addObject:match];
  slotsTaken[slot] = YES;
  unsigned char code = 128 >> slot;
  //printf("for slot : %d added code : %d\n", slot, code);
  numberCode += code;
}

-(BOOL)isNewMatchInSlotValid:(int)slot {
  BOOL result = NO;
  unsigned char code = 128 >> slot;
  //    printf("for slot : %d added code : %d\n", slot, code);
  numberCode += code;
  if ([self getValue] != -1) {
    result = YES;
  }
  
  code = 128 >> slot;
  numberCode -= code;
  
  return result;
}

-(BOOL)isMatchValidWithoutSlot:(int)slot {
  unsigned char codeToRemove = 128 >> slot;
  numberCode -= codeToRemove;
  BOOL result = NO;
  if ([self getValue] != -1) {
    result = YES;
  }
  numberCode += codeToRemove;
  //printf("Digit : isMatchValidWithoutSlot = %d\n",result);
  return result;
}

-(BOOL)isNewMatchInSlotValid:(int)slot without:(int)slotRemoved {
  unsigned char codeToRemove = 128 >> slotRemoved;
  numberCode -= codeToRemove;
  BOOL result = NO;
  if ([self isNewMatchInSlotValid:slot]) {
    result = YES;
  }
  numberCode += codeToRemove;
  return result;
}

/*
 numberCode represents 10 based number of binary representation of slots
 */
-(int)getValue {
  switch (numberCode) {
    case 119 : return 0;    // 0 1110111
    case 18  : return 1;    // 0 0010010
    case 93  : return 2;
    case 91  : return 3;
    case 58  : return 4;
    case 107 : return 5;
    case 111 : return 6;
    case 82  : return 7;
    case 127 : return 8;    // 0 1111111
    case 123 : return 9;
      
    default:
      //return numberCode;
      return -1;
  }
  return -1; // should never go here
}

-(char) getCharValue {
  return [self getValue] + '0';
}

-(BOOL)canRecieveMatch {
  int val = [self getValue];
  
  switch (val) {
    case 0: case 1:
    case 3: case 5:
    case 6: case 9:
      return YES;
    default:
      return NO;
  }
  return NO;
}

-(BOOL)canGiveMatch {
  int val = [self getValue];
  
  switch (val) {
    case 6: case 7:
    case 8: case 9:
      return YES;
      
    default:
      return NO;
  }
  return NO;
}

-(BOOL)canMoveMatch {
  int val = [self getValue];
  switch (val) {
    case 0: case 2:
    case 3: case 5:
    case 6: case 9:
      return YES;
      
    default:
      return NO;
  }
  return NO;
}

-(void)removeMatch:(CCSprite*)match withSlot:(int)slot {
  [match removeFromParentAndCleanup:YES];
  [matches removeObject:match];
  slotsTaken[slot] = NO;
  unsigned char code = 128 >> slot;
  numberCode -= code;
  
}

/*********************************************
 returns slot taken by match
 and in colision with location point
 *********************************************/
-(int)getMatchedSlot:(CGPoint)location {
  float collisionRadius = MATCH_HEIGHT * 0.3;
  int i;
  for (i=1; i<8; ++i) {
    if (slotsTaken[i]) {
      if (CGRectContainsPoint(slots[i], location) || ccpDistance(points[i], location) < collisionRadius) {
        // remember to add validation
        printf("slot to update : %d \t distance : %f \t colisionRad : %f \n", i,
               ccpDistance(points[i], location), collisionRadius);
        CCLOG(@"[%@] %@ : fits to slot %d", self, NSStringFromSelector(_cmd), i);
        return i;
      }
    }
  }
  return 0;
}

-(int)getSlotIn:(CGPoint)location taken:(BOOL)isTaken{
  float collisionRadius = MATCH_HEIGHT * 0.3;
  int i;
  for (i=1; i<8; ++i) {
    if ( (isTaken) ? (slotsTaken[i]) : (!slotsTaken[i]) ) {
      if (CGRectContainsPoint(slots[i], location) || ccpDistance(points[i], location) < collisionRadius) {
        // remember to add validation
        printf("slot to update : %d \t distance : %f \t colisionRad : %f \n", i,
               ccpDistance(points[i], location), collisionRadius);
        CCLOG(@"[%@] %@ : fits to slot %d", self, NSStringFromSelector(_cmd), i);
        return i;
      }
    }
  }
  return 0;
}

/*********************************************
 returns slot not taken by match
 but in colision with location point
 *********************************************/
-(int)getNotMatchedSlot:(CGPoint)location {
  float collisionRadius = MATCH_HEIGHT * 0.3;
  int i;
  for (i=1; i<8; ++i) {
    if (!slotsTaken[i]) {
      if (CGRectContainsPoint(slots[i], location) || ccpDistance(points[i], location) < collisionRadius) {
        // remember to add validation
        printf("slot to update : %d \t distance : %f \t colisionRad : %f \n", i,
               ccpDistance(points[i], location),
               collisionRadius);
        CCLOG(@"[%@] %@ : fits to slot %d", self, NSStringFromSelector(_cmd), i);
        return i;
      }
    }
  }
  return 0;
}

-(CCSprite*)getMatchFromSlot:(int)slot {
  return (CCSprite*) [self getChildByTag:slot];
  //    return (CCSprite*) [matches objectAtIndex:slot];
}

//-(BOOL)fitsToSlot:(CGPoint)loc
//{
//    int i;
//    for (i=1; i<8; ++i) {
//        if (CGRectContainsPoint(slots[i], loc) ) {
//            // remember to add validation
//            slotToUpdate = i;
//            printf("slot to update : %d\n", i);
//            return YES;
//        }
//    }
//    return NO;
//}

-(void)reset
{
  //    selectedSprite = nil;
  
  [self initPoints];
  [self initSlots];
  
  
  // tlo
#ifdef MATCH_DBG
  //    CCSprite * match_bg1 = [CCSprite spriteWithFile:@"match-bg1.png"];
  //    [match_bg1 setPosition:CGPointMake(0, 0)];
  //    [self addChild:match_bg1 z:0 tag:100];
  
  //    CCSprite * numberElement = [CCSprite spriteWithFile:@"numberElement.png"];
  //    [self addChild:numberElement z:0 tag:100];
  
#endif
  
  // matches = [[CCArray alloc] initWithCapacity:7];
  matches = [[CCArray alloc] init];
  
}

/*
 -(id)initWithSpace:(cpSpace *)newSpace {
 if ((self = [super init])) {
 [self reset];
 space = newSpace;
 }
 return self;
 }
 */
- (id)init
{
  self = [super init];
  if (self) {
    
    [self reset];
    
    space = [[PhysicsManager instance] space];
    
    highlightOpacity = 50;
    //        int i;
    //        for (i=1; i<8; ++i)  {
    //            [self addMatchNumber:i];
    //        }
    [self scheduleUpdate];
  }
  
  return self;
}

-(void)setEquElementType:(EquationElementType)etype {
  equElementType = etype;
}

-(EquationElementType)getEquElementType {
  return equElementType;
}

-(id)initWithNumber:(int)num
{
  self = [super init];
  if (self) {
    [self reset];
    [self makeNumber:num];
  }
  return self;
}

-(id)initWithNumber:(int)num andType:(EquationElementType)etype
{
  self = [super init];
  if (self) {
    [self reset];
    [self setEquElementType:etype];
    [self makeNumber:num];
  }
  return self;
}

-(void)addNumberBg {
  for (int i = 1; i<8; ++i) {
    [self addBackgroundMatchNumber:i];
  }
}

-(void)makeNumber:(int)num
{
  [self addNumberBg];
  switch (num) {
    case 1:
      [self addMatchNumber:3];
      [self addMatchNumber:6];
      break;
    case 2:
      [self addMatchNumber:1];
      [self addMatchNumber:3];
      [self addMatchNumber:4];
      [self addMatchNumber:5];
      [self addMatchNumber:7];
      break;
    case 3:
      [self addMatchNumber:1];
      [self addMatchNumber:3];
      [self addMatchNumber:4];
      [self addMatchNumber:6];
      [self addMatchNumber:7];
      break;
    case 4:
      [self addMatchNumber:2];
      [self addMatchNumber:3];
      [self addMatchNumber:4];
      [self addMatchNumber:6];
      break;
    case 5:
      [self addMatchNumber:1];
      [self addMatchNumber:2];
      [self addMatchNumber:4];
      [self addMatchNumber:6];
      [self addMatchNumber:7];
      break;
    case 6:
      [self addMatchNumber:1];
      [self addMatchNumber:2];
      [self addMatchNumber:4];
      [self addMatchNumber:5];
      [self addMatchNumber:6];
      [self addMatchNumber:7];
      break;
    case 7:
      [self addMatchNumber:1];
      [self addMatchNumber:3];
      [self addMatchNumber:6];
      break;
    case 8:
      [self addMatchNumber:1];
      [self addMatchNumber:2];
      [self addMatchNumber:3];
      [self addMatchNumber:4];
      [self addMatchNumber:5];
      [self addMatchNumber:6];
      [self addMatchNumber:7];
      break;
    case 9:
      [self addMatchNumber:1];
      [self addMatchNumber:2];
      [self addMatchNumber:3];
      [self addMatchNumber:4];
      [self addMatchNumber:6];
      [self addMatchNumber:7];
      break;
    case 0:
      [self addMatchNumber:1];
      [self addMatchNumber:2];
      [self addMatchNumber:3];
      [self addMatchNumber:5];
      [self addMatchNumber:6];
      [self addMatchNumber:7];
      break;
      
    default:
      break;
  }
}

-(void)updatePhysicsBodies {
  for (MatchSprite * m in matches) {
    //[m updateBody];
    [m update];
  }
}

- (void)dealloc {
  [matches removeAllObjects];
  [matches release];
  //matches = nil;
  
  [super dealloc];
}

@end
