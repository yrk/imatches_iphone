//
//  Operation.h
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 01.03.2012.
//  Copyright (c) 2012 yarkill@gmail.com. All rights reserved.
//

#import "cocos2d.h"

#import "My_constants.h"
#import "EquationElement.h"
#import "MatchSprite.h"
//#import "chipmunk.h"
#import "PhysicsManager.h"
#import "MatchesCanChange.h"
#import "PhysicsWorks.h"

@interface Operation : EquationElement <MatchesCanChange, PhysicsWorks> {
    CCArray * matches;
    OperatorType opType;
    char char_value;
    BOOL slot_taken;
}

-(id)initWithOp:(int)op;

-(void)makeMinus;
-(void)addPlus;

-(OperatorType)getOperatorType;


-(void)makeOp:(int)op;

// from PhysicsWorks protocol :
//-(void)updatePhysicsBodies;

//-(int)getMatchedSlot:(CGPoint)location;
//-(CCSprite*)getMatchFromSlot:(int)slot;

-(void) change_oper_value:(BOOL)match_added;


-(BOOL)operatorIntersectLocation:(CGPoint)location;

// changes '=' to '+'
-(void) changeEqualsToPlus;

// changes '+' to '-'
-(void) changeToEquals;

-(void) addMatch;

@end
