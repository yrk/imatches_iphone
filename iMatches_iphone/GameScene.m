//
//  GameScene.m
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 30.06.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import "GameScene.h"
#import "My_constants.h"
#import "GameManager.h"

#import "drawSpace.h"
#import "macros.h"

//#define DO_DRAW_PHYSICS

@implementation GameScene

@synthesize gameLayer;
@synthesize menuBar;
@synthesize levelManager;

@synthesize currentLevelIndex;

@synthesize swipe_recognizer;
@synthesize longPressRecognizer;
@synthesize double_tap_recognizer;


+(id) scene
{
	// 'scene' is an autorelease object.
	GameScene *scene = [[[GameScene alloc] init] autorelease];
  
	// return the scene
	return scene;
}

-(id) init {
  if (self = ([super init])) {
    
    srandom(time(NULL));
    
    longPressedFlag = NO;
    
    size = [[CCDirector sharedDirector] winSize];
    
    //        levelManager = [[LevelManager alloc] init];
    levelManager = [GameManager instance].levelManager;
    
    //[self addChild:levelManager z:5 tag:ELevelManagerTag];
    //[levelManager initLevel];
    
    // to remove transparency issues :
    //GameLayer *layer = [[[GameLayer alloc] initWithColor:ccc4(100, 100, 100, 255)] autorelease];

    
    // Non transparent background fix
    CCLayerColor * bgLayer = [[[CCLayerColor alloc] initWithColor:ccc4(0, 0, 0, 255)] autorelease];
    [self addChild:bgLayer z:0];
    
    levels = [[CCArray alloc] init];
    
    
    [self setupGestureRecognizers];
    
    currentLevelIndex = 0;
    
    /// GAME LAYER
    /// create Game Layer
    gameLayer = [[GameLayer alloc] initWithLevelManager:levelManager];
    gameLayer.levelManager = levelManager;
    [[GameManager instance] setGameLayer:gameLayer];
    
    /// add Game Layer to Game Scene
    [self addChild:gameLayer z:30 tag:EGameLayerTag];
    
    
    [self addGestureRecognizersForGameLayer:gameLayer];
    [self enableDoubleTapRecognizer];
    
    [levels addObject:gameLayer];
    
    [self addNewGameLayer];
    [self addNewGameLayer];
    
    // setup for GameManager
    [[GameManager instance] setGameScene:self];
    
    // what with LevelManager ??
    //[[GameManager instance] setLevelManager:levelManager];
    
    [self addMenuBar];
  }
  
  return self;
}

/// setup gesture recognizers to use with Game Layer
-(void) setupGestureRecognizers {
  
  //[self setupSwipeGestureRecognizer];
  
  //self.longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:game_layer action:@selector(handleLongPress:)];
  self.longPressRecognizer = [[UILongPressGestureRecognizer alloc] init];
  //self.longPressRecognizer.numberOfTapsRequired = 1;
  self.longPressRecognizer.allowableMovement = 4.0f;
  self.longPressRecognizer.minimumPressDuration = 0.6;
  
  [longPressRecognizer addTarget:self action:@selector(handleLongPress:)];
  [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:self.longPressRecognizer];
  
  //self.double_tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:game_layer action:@selector(handleDoubleTap:)];
  self.double_tap_recognizer = [[UITapGestureRecognizer alloc] init];
  [self.double_tap_recognizer setNumberOfTapsRequired:2];
  
}

/// remove all gesture recognizers from Game Layer
-(void) removeGestureRecognizersFromGameLayer:(GameLayer*) game_layer {
  [double_tap_recognizer removeTarget:game_layer action:@selector(handleDoubleTap:)];
  //[longPressRecognizer removeTarget:game_layer action:@selector(handleLongPress:)];
  
  //[[[CCDirector sharedDirector] openGLView] removeGestureRecognizer:game_layer.longPressRecognizer];
  [[[CCDirector sharedDirector] openGLView] removeGestureRecognizer:game_layer.double_tap_recognizer];
  
  //game_layer.longPressRecognizer = nil;
  game_layer.double_tap_recognizer = nil;
  
  [[CCTouchDispatcher sharedDispatcher] removeDelegate:game_layer];
}

/// add all gesture recognizers to Game Layer
-(void) addGestureRecognizersForGameLayer:(GameLayer*) game_layer {
  
  /*
   [double_tap_recognizer addTarget:game_layer action:@selector(handleDoubleTap:)];
   game_layer.double_tap_recognizer = self.double_tap_recognizer;
   [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:game_layer.double_tap_recognizer];
   */
  // disable Long Press Gesture Recognizer
  //[longPressRecognizer setEnabled:NO];
  
  /// Set Game Layer to response to touches :
  [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:game_layer priority:0 swallowsTouches:YES];
}

/// disable double tap for current Game Layer
-(void) disableDoubleTapRecognizer {
  [double_tap_recognizer removeTarget:gameLayer action:@selector(handleDoubleTap:)];
  [[[CCDirector sharedDirector] openGLView] removeGestureRecognizer:gameLayer.double_tap_recognizer];
  gameLayer.double_tap_recognizer = nil;
  
}

/// enable double tap for current Game Layer
-(void) enableDoubleTapRecognizer {
  [double_tap_recognizer addTarget:gameLayer action:@selector(handleDoubleTap:)];
  gameLayer.double_tap_recognizer = self.double_tap_recognizer;
  [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:gameLayer.double_tap_recognizer];
}

-(void) onEnterTransitionDidFinish {
  [gameLayer onEnterTransitionDidFinish];
}

-(void) onExitTransitionDidStart {
  [gameLayer onExitTransitionDidStart];
}

-(void) addMenuBar {
  //CGSize size = [[CCDirector sharedDirector] winSize];
  
  menuBar = [[MenuBar alloc] init];
  //[menuBar setPosition:CGPointMake(0, -size.height/4)];
  [menuBar setPosition:CGPointMake(0, 0)];
  
  [self addChild:menuBar z:50];
  
  [GameManager instance].menuBar = menuBar;
}

-(void) addNewGameLayer {
  /// create Game Layer
  GameLayer * game_layer = [[GameLayer alloc] initWithLevelManager:levelManager];
  //gameLayer.levelManager = levelManager;
  
  //[[GameManager instance] setGameLayer:gameLayer];
  [game_layer setPosition:CGPointMake(size.width, 0)];
  
  /// add Game Layer to Game Scene
  [self addChild:game_layer z:30];
  /// add gesture recognizers to this Game Layer
  //[self addGestureRecognizersForGameLayer:gameLayer];
  
  [levels addObject:game_layer];
  
}

/// makes next Game Layer in stack an active Game Layer
-(void) nextGameLayer {
  //CCLOG(@"%@ - %@ ", self, _CMD);
  
  GameLayer * game_layer = (GameLayer*) [self getChildByTag:EGameLayerTag];
  CGPoint point = [game_layer position];
  //CCLOG(@"game layer position : %f, %f", point.x, point.y);
  [game_layer setPosition:ccp(point.x+5, point.y)];
  // test
  
  //CCLOG(@"current level index : %d", currentLevelIndex);
  //CCLOG(@"[levels count] : %d", [levels count]);
  
  if ( currentLevelIndex < [levels count]-1) {
    currentLevelIndex++;
  } else {
    currentLevelIndex = 0;
  }
  
  [self removeGestureRecognizersFromGameLayer:gameLayer];
  [gameLayer setPosition:CGPointMake(-size.width, 0)];
  
  gameLayer = [levels objectAtIndex:currentLevelIndex];
  [gameLayer setPosition:CGPointMake(0, 0)];
  [self addGestureRecognizersForGameLayer:gameLayer];
  
  [[GameManager instance] setGameLayer:gameLayer];
  
  
  
}

/// makes previous Game Layer in stack an active Game Layer
-(void) prevGameLayer:(GameLayer*) game_layer {
  
}

-(void) backButtonPressed {
  [[SimpleAudioEngine sharedEngine] playEffect:@"dzum.wav"];
  //CCLOG(@"%@ - %@ ", self, NSStringFromSelector(_cmd));
  
  if (currentLevelIndex >0) {
    // go to prevLevel
    --currentLevelIndex;
    // some method to change level with nice and fast transitions
  } else {
    // basically do nothing :D
  }
}

-(void) forwardButtonPressed {
  [[SimpleAudioEngine sharedEngine] playEffect:@"dzum.wav"];
  //CCLOG(@"%@ - %@ ", self, _CMD);
  
  [self nextGameLayer];
}

-(void) menuButtonPressed {
  [[SimpleAudioEngine sharedEngine] playEffect:@"dzum.wav"];
  //CCLOG(@"%@ - %@ ", self, _CMD);

  // hide menu bar when in main menu
  [menuBar menuBarHide];
  [self enableDoubleTapRecognizer];
  
  
  CCTransitionMoveInB * moveInBot = [CCTransitionMoveInB transitionWithDuration:0.5f scene:[[GameManager instance] menuScene]];
  
  
  
  //[[[CCDirector sharedDirector] openGLView] removeGestureRecognizer:panGestureRecognizer];
  
  [[CCDirector sharedDirector] replaceScene:moveInBot];
}


-(void)handleLongPress:(UILongPressGestureRecognizer *)paramSender {
  //CCLOG(@"================ LONG PRESS handler working ...............");
  if (paramSender.state == UIGestureRecognizerStateBegan) {
  
//  if (!longPressedFlag) {
    
    if ([menuBar isHidden]) {
      [self disableDoubleTapRecognizer];
      [menuBar menuBarShow];
    } else {
      [menuBar menuBarHide];
      [self enableDoubleTapRecognizer];
    }
    
    //[[[GameManager instance] menuBar] menuBarAction];
    CCLOG(@"================ LONG PRESS DETECTED ! =============");
  }
  //longPressedFlag = !longPressedFlag;
}

- (void)draw {
  
#ifdef DO_DRAW_PHYSICS
  
  glPushMatrix();
  
  glTranslatef(240.0f, 180.0f, 0.0f);
  
  drawSpaceOptions options = {
    0, // drawHash
    0, // drawBBs,
    1, // drawShapes
    4.0, // collisionPointSize
    4.0, // bodyPointSize,
    2.0 // lineThickness
  };
  
  drawSpace( [gameLayer getSpace], &options);
  glPopMatrix();
#endif
}

-(void) dealloc {
  [menuBar release];
  [gameLayer release];
  
  [levels removeAllObjects];
  [levels release];
  
  [self removeAllChildrenWithCleanup:YES];
  
  
  [super dealloc];
}


@end
