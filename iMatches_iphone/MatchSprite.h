//
//  MySprite.h
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 15.11.2011.
//  Copyright 2011 yarkill@gmail.com. All rights reserved.
//

#import "Matches_types.h"

#import "My_constants.h"
#import "ChipmunkSprite.h"
#import "PhysicsManager.h"

@interface MatchSprite : ChipmunkSprite {
    
    //CGPoint last_loc;
    
   // Match_type match_type;
    //Segment_type segment;
    
    CGPoint parentDigitPosition, // pozycja cyfry (rodzica zapalki) na scenie
            inDigitPosition, // pozycja zapalki wewnatrz cyfry
            worldPosition, // pozycja zapalki na scenie/layerze
    oldGamePosition; // pozycja zapalki w grze przed effektami
    
    
    CGPoint current_location;
    //cpSpace * spacePointer;
    
    
}

//@property (nonatomic,readwrite,assign) CGPoint last_loc;
//@property (nonatomic,readwrite,assign) Match_type match_type;
//@property (nonatomic,readwrite,assign) Segment_type segment;


//- (id)initWithSpace:(cpSpace *)theSpace location:(CGPoint)location forSlot:(uint)slot;
- (id)initWithLocation:(CGPoint)location forSlot:(uint)slot;

-(void)updateBody;

@end
