//
//  Operation.m
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 01.03.2012.
//  Copyright (c) 2012 yarkill@gmail.com. All rights reserved.
//

#import "Operation.h"

@implementation Operation 

-(id)init {
    
    if(self = [super init]) {
        matches = [[CCArray alloc] init];
        
        slot_taken = NO;
    }
    
    return self;
}

-(void)makeMinus {
    char_value = '-';
    //CCSprite *match = [CCSprite spriteWithFile:@"match1.png"];
    MatchSprite *match = [[MatchSprite alloc] initWithLocation:CGPointMake(0, 0) forSlot:1];
    [matches addObject:match];
    [self addChild:match z:20 tag:EMinusOperator];
}

-(void)addPlus {
    char_value = '+';
//    CCSprite *match = [CCSprite spriteWithFile:@"match2.png"];
    MatchSprite *match = [[MatchSprite alloc] initWithLocation:CGPointMake(0, 0) forSlot:2];
    [matches addObject:match];
    [self addChild:match z:20 tag:EPlusOperator];
    slot_taken = YES;
}

-(void) makeDiv {
    char_value = '/';
//    CCSprite * match = [CCSprite spriteWithFile:@"match3.png"];
    MatchSprite *match = [[MatchSprite alloc] initWithLocation:CGPointMake(0, 0) forSlot:match_slash];
    [matches addObject:match];
    [self addChild:match z:20 tag:EDivOperator];
}

-(void) addMul {
    char_value = '*';
    MatchSprite *match = [[MatchSprite alloc] initWithLocation:CGPointMake(0, 0) forSlot:match_backslash];
    [matches addObject:match];    
//    CCSprite * match = [CCSprite spriteWithFile:@"match4.png"];
    [self addChild:match z:20 tag:EMulOperator];
    slot_taken = YES;
}

-(void) makeEquals {
    char_value = '=';
    //CCSprite *match = [CCSprite spriteWithFile:@"match1.png"];
    //[match setPosition:ccp(0, MATCH_WIDTH)];
    MatchSprite * match = [[MatchSprite alloc] initWithLocation:ccp(0, MATCH_WIDTH) forSlot:match_horizontal];
    [match setPosition:ccp(0, MATCH_WIDTH)];
    [matches addObject:match];
    [self addChild:match z:20 tag:1];
    
    //match = [CCSprite spriteWithFile:@"match1.png"];
    //[match setPosition:ccp(0, -MATCH_WIDTH)];
    match = [[MatchSprite alloc] initWithLocation:ccp(0, -MATCH_WIDTH) forSlot:match_horizontal];
    [match setPosition:ccp(0, -MATCH_WIDTH)];
    [matches addObject:match];
    [self addChild:match z:20 tag:2];
    slot_taken = YES;
}

// reset Operation data to make new Operation
-(void) cleanOp {
    for (MatchSprite * match in matches) {
        [match setPosition:CGPointMake(0, 0)];
        [match release];
    }
    [self removeAllChildrenWithCleanup:YES];
    [matches removeAllObjects];
}

// changes '=' to '+'
-(void) changeEqualsToPlus {
    [self cleanOp];
    
    [self makeMinus];
    [self addPlus];
}

// changes '+' to '-'
-(void) changeToEquals {
    [self cleanOp];
    
    [self makeEquals];
}

// ret val -> is equ changed ?
-(BOOL) tapEqual {
    if (char_value == '+') {
        [self changeToEquals];
        return YES;
    } else if (char_value == '=') {
        [self changeEqualsToPlus];
        return YES;
    }
    return NO;
}


// check collision with touchLocation
-(BOOL)operatorIntersectLocation:(CGPoint)location {
    for (MatchSprite * match in matches) {
        //CGSize match_size = [match contentSize];
        if (CGRectContainsPoint(match.boundingBox, location)) {
            return YES;
        }
    }
    return NO;
}

-(void) addMatch {
    switch (char_value) {
        case '-':
            [self addPlus];
            break;
        case '/':
            [self addMul];
            break;
        default:
            break;
    }
}


-(void)updatePhysicsBodies {
    for (MatchSprite* m in matches) {
        [m update];
    }
}

-(char) getCharValue {
    return char_value;
}

-(id)initWithOp:(int)op {
    
    self = [super init];
    if (self) {
        matches = [[CCArray alloc] init];
        
        [self makeOp:op];
        
        
    }
    
    return self;
}


-(int)getMatchedSlot:(CGPoint)location {
    float collisionRadius = MATCH_HEIGHT * 0.3;
 
    // if there is no match to take than ommit all checks - prevents stupid errors :)
    if (!slot_taken) return 0;

    MatchSprite * sel_sprite = [matches objectAtIndex:1];
    CGRect frame = CGRectMake([self position].x, [self position].y, [sel_sprite contentSize].width, [sel_sprite contentSize].height);
    
    if (slot_taken) {
            if (CGRectContainsPoint(frame, location) || ccpDistance([self position], location) < collisionRadius) {

                return 1;
            }
    }

    return 0;
}

-(CCSprite*)getMatchFromSlot:(int)slot {
//    slot_taken = NO;
    return (CCSprite*) [matches objectAtIndex:1];
}


-(BOOL)isMatchValidWithoutSlot:(int)slot {
    return slot_taken;
}


-(void)addMatchNumber:(int)slot
{
    CGPoint zero_point = CGPointMake(0, 0);
    int one_slot = 1;
    
    MatchSprite * match = [[MatchSprite alloc] initWithLocation:zero_point forSlot:one_slot];
    
    [match setPosition:zero_point];
    
    [self addChild:match z:25 tag:slot];
    
    [matches addObject:match];
    slot_taken = YES;
    
    [self change_oper_value:YES];

}


-(void) update_oper_value {
    switch (char_value) {
        case '+':
            char_value = '-';
            break;

        case '*':
            char_value = '/';
            break;
            
        default:
            break;
    }
}

-(void) change_oper_value:(BOOL)match_added {
    if (!match_added) { // remove match
        switch (char_value) {
            case '+':
                char_value = '-';
                break;
            
            case '*':
                char_value = '/';
                break;
            case '=':
                char_value = '-';
                break;
            
            default:
                break;
        }
    } else { // we are adding match
        switch (char_value) {
            case '-':
                char_value = '+';
                break;
                
            case '/':
                char_value = '*';
                break;
                
            default:
                break;
        }
    }
}


-(void)removeMatch:(CCSprite*)match withSlot:(int)slot {
    [match removeFromParentAndCleanup:YES];
    [matches removeObject:match];
    slot_taken = NO;
    [self change_oper_value:NO];
    //[self update_oper_value];
    //char_value = '-';
}

-(void)makeOp:(int)op {

    switch (op) {
        case EMinusOperator:
            [self makeMinus];
            opType = EMinusOperator;
            break;
        case EPlusOperator:
            opType = EPlusOperator;
            [self makeMinus];
            [self addPlus];
            break;
        case EDivOperator:
            opType = EDivOperator;
            [self makeDiv];
            break;
        case EMulOperator:
            opType = EMulOperator;
            [self makeDiv];
            [self addMul];
            break;
        case EEquOperator:
            opType = EEquOperator;
            [self makeEquals];
            break;
            
        default:
            break;
    }
}

-(BOOL) canGiveMatch {
    switch (char_value) {
        case '+': case '*': case '=':
            return YES;
            
            break;
            
        default:
            return NO;
            break;
    }
    return NO;
}

-(BOOL) canRecieveMatch {
    switch (char_value) {
        case '-': case '/':
            return YES;
            break;
            
        default:
            return NO;
            break;
    }
    return NO;
}



-(OperatorType)getOperatorType {
    return opType;
}


-(void)dealloc {
    [matches removeAllObjects];
    [matches dealloc];
    
    [super dealloc];
}
@end
