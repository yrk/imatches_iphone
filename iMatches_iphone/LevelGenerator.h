//
//  LevelGenerator.h
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 01.03.2012.
//  Copyright (c) 2012 yarkill@gmail.com. All rights reserved.
//

#import "cocos2d.h"
#import "My_constants.h"

/*
 Level Generator:
 - mial generowac levele - ale to juz bedzie robione zewnetrznie
 - nadal sprawdza poprawnosc rozwiazania -> chyba bez sensu !!!!!
 - w zasadzie to nie ma juz racji bytu
 */

@interface LevelGenerator : CCNode {
//    int numbers[4];
//    int size;
    

}

-(Equation)generateFirstLevel;


//-(void)messThingsUp:(Equation) equ;

-(BOOL)matchCanBeRemoved:(int)val;
-(BOOL)matchCanBeAdded:(int)val;
-(BOOL)matchCanBeMoved:(int)val;

-(int)removeMatch:(int)a;
-(int)addMatch:(int)a;

//-(Equation)messThingsUp:(int[])numbers withSize:(int)size;

-(NSString*)generateLevelString:(int)level;
-(Equation)generateDemoLevel:(int)level;

-(void) genTest;





@end
