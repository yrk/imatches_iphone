//
//  PhysicsManager.m
//  iMatches_iphone
//
//  Created by Admin on 27.01.2014.
//
//

#import "PhysicsManager.h"

static PhysicsManager * inst = nil;

@implementation PhysicsManager

//@synthesize text;
@synthesize space;


+(id)instance {
    @synchronized(self) {
        if (!inst) {
            inst = [[PhysicsManager alloc] init];
        }
    }
    return inst;
}

-(id)init {
    if ((self = [super init])) {
     
        // chipmunk - init
        [self createSpace];
        //[self createGround];
        //[self makeGround];
        
        //text = @"Pochodze z jebanego kurwa singletona made by YRK kurwa !!!!!!!";
    }
    return self;
}

-(void)updatePhysics:(ccTime)dt {
    cpSpaceStep(space, dt);
}

- (void)createSpace {
    space = cpSpaceNew();
    space->gravity = ccp(0, -750);
    //cpSpaceResizeStaticHash(space, 400, 200);
    //cpSpaceResizeActiveHash(space, 200, 200);
}

- (void)createGround {
    // 1
    CGSize winSize = [CCDirector sharedDirector].winSize;
    //CGPoint lowerLeft = ccp(-winSize.width/2, -winSize.height+20);
    //CGPoint lowerRight = ccp(winSize.width/2, -winSize.height+20);
    //CGPoint lowerLeft = ccp(0, winSize.height);
    //CGPoint lowerRight = ccp(0, 0);

    cpVect lowerLeft = cpv(0, 0);
    cpVect lowerRight = cpv(winSize.width, 0);
    
    
    // 2
    cpBody *groundBody = cpBodyNewStatic();
    
    // 3
    float radius = 10.0;
    cpShape *groundShape = cpSegmentShapeNew(groundBody, lowerLeft, lowerRight, radius);
    

    
    // 4
    groundShape->e = 0.5; // elasticity
    groundShape->u = 1.0; // friction
    
    // 5
    cpSpaceAddShape(space, groundShape);
    
    
}

- (void)createBoxAtLocation:(CGPoint)location {
    
    float boxSize = 60.0;
    float mass = 1.0;
    cpBody *body = cpBodyNew(mass, cpMomentForBox(mass, boxSize, boxSize));
    body->p = location;
    cpSpaceAddBody(space, body);
    
    //cpShape * matchShape = cpBoxShapeNew(body, match.width, match.height);
    
    cpShape *shape = cpBoxShapeNew(body, boxSize, boxSize);
    shape->e = 1.0;
    shape->u = 1.0;
    cpSpaceAddShape(space, shape);
    
}

-(void) countObjects {
  /// this is to check if all spirtes were removed properly !
//  cpSpaceContainsBody(space, body);
}

/*
-(void)makeGround {
 
    cpBody *body;
    cpShape *shape;
    
    
    
    CCSprite * sprite = [[CCSprite alloc] initWithFile:@"menubar.png"];
    
    //float mass = 1.0;
    body = cpBodyNewStatic();
    //cpBodyNew(mass, cpMomentForBox(mass, sprite.contentSize.width, sprite.contentSize.height));
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CGPoint location = ccp(winSize.width/2, winSize.height+20);
//    CGPoint lowerRight = ccp(winSize.width/2, -winSize.height+20);
    
    body->p = location;
    //body->data = self;
    //cpSpaceAddBody(space, body);
    
    shape = cpBoxShapeNew(body, sprite.contentSize.width, sprite.contentSize.height);
    shape->e = 0.3;
    shape->u = 1.0;
    //shape->data = self;
    cpSpaceAddShape(space, shape);
}
*/
-(void)dealloc {
    //[text release];
    
    cpSpaceFree(space);
    
    [super dealloc];
}

@end
