//
//  AboutScene.m
//  iMatches_iphone
//
//  Created by Admin on 27.01.2014.
//
//

#import "AboutScene.h"

@implementation AboutScene

@synthesize menuScene;
@synthesize panGestureRecognizer;

-(id) init {
    if ((self = [super init])) {
        
        layer = [CCLayer node];
        [self addNewLabel];
        
        [self initButton];
        
        CCLayerColor * bgLayer = [[[CCLayerColor alloc] initWithColor:ccc4(0, 0, 0, 255)] autorelease];
        [self addChild:bgLayer z:0];
        
        //[self panGestureRecognizerInit];
        
        [self addChild:layer z:10];
        
    }
    
    return self;
}

-(void) panGestureRecognizerInit {
    panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveLayer:)];
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:panGestureRecognizer];
}

-(void) moveLayer:(UIPanGestureRecognizer*)paramSender {
    if (paramSender.state == UIGestureRecognizerStateBegan) {
        lastLocation = [paramSender locationInView:paramSender.view.superview];
        //paramSender.view.center = location
    }
    
    if (paramSender.state == UIGestureRecognizerStateChanged) {
        newLocation = [paramSender locationInView:paramSender.view.superview];
        difference = ccpSub(newLocation, lastLocation);
        
        //[aboutTextLabel setPosition:CGPointMake(aboutTextLabel.position.x, aboutTextLabel.position.y + difference.y)];
        [aboutTextLabel setPosition:CGPointMake(aboutTextLabel.position.x, aboutTextLabel.position.y + difference.x)];
//        aboutTextLabel.position.y += difference.y;
        
        lastLocation = newLocation;
    }
    
    if (paramSender.state == UIGestureRecognizerStateEnded ||
        paramSender.state == UIGestureRecognizerStateCancelled) {
        
    }
    
    
}

-(void) initButton {
    CGSize size = [[CCDirector sharedDirector] winSize];
    int buttonOffsetY = 50;
    
    CCSprite * exitButtonNormal = [CCSprite spriteWithFile:@"cancel-icon.png"];
    CCSprite * exitButtonSelected = [CCSprite spriteWithFile:@"cancel-solid-icon.png"];
    
    CCMenuItemSprite * exitMenuButton = [CCMenuItemSprite itemFromNormalSprite:exitButtonNormal selectedSprite:exitButtonSelected target:self selector:@selector(exitButtonPressed)];
    
    CCMenu * buttonMenu = [CCMenu menuWithItems:exitMenuButton, nil];
    //[buttonMenu setPosition:CGPointMake(size.width/2 + exitButtonNormal.contentSize.width/2, buttonOffsetY + exitButtonNormal.contentSize.height/2)];
    [buttonMenu setPosition:CGPointMake(size.width/4, buttonOffsetY + exitButtonNormal.contentSize.height/2)];
    //        [menubar alignItemsHorizontallyWithPadding:100];
    [self addChild:buttonMenu z:35];
}

-(void)exitButtonPressed {
    [[SimpleAudioEngine sharedEngine] playEffect:@"pstryk.wav"];
    
    CCTransitionMoveInB * moveInBot = [CCTransitionMoveInB transitionWithDuration:0.5f scene:menuScene];
    
    [[[CCDirector sharedDirector] openGLView] removeGestureRecognizer:panGestureRecognizer];
    
    [[CCDirector sharedDirector] replaceScene:moveInBot];
}

-(void)addNewLabel {
    
    int x,y;
    CGSize size = [[CCDirector sharedDirector] winSize];
    x = size.width/2;
    y = size.height/4;
    
    [self aboutTextInit];
    
    aboutTextLabel = [CCLabelTTF labelWithString:aboutText dimensions:CGSizeMake(360.0f, 350.0f) alignment:UITextAlignmentCenter fontName:@"Courier New" fontSize:14.0f];
    
    //CCLabelTTF * newLabel = [CCLabelTTF labelWithString:@"made by Yrk @ 2014 " fontName:@"Marker Felt" fontSize:20];
    //newLabel.position = ccp(x + newLabel.contentSize.width/2, y);
    aboutTextLabel.position = ccp(x , y);
    
    
    [layer addChild:aboutTextLabel];
}

-(void) aboutTextInit {
    aboutText = @"This game was created with use of COCOS2D-iPhone library\n\
    Special thanks to :\n\
    Krzysztof Bittner,\n\
    Przemysław Borowski,\n\
    Przemysław Winsyk,\n\
    Rafał Stański,\n\
    and many thanks to my Brother.\n\
    \n\
    Icons made by:\n\
    endlessicons.com\n\
    \n\
    Thanks for playing!\n\
    made by Yrk @ 2014";
}

-(void) dealloc {
    
    [panGestureRecognizer release];
    
    [super dealloc];
}

@end
