//
//  MatchPlaceHolder.h
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 03.02.2012.
//  Copyright (c) 2012 yarkill@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MatchPlaceHolder : NSObject
{
    CGRect holdingRect;

}

-(void)initWithRect:(CGRect)rect;

@property (readwrite, assign) CGRect holdingRect;

@end