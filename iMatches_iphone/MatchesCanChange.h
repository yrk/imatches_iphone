//
//  MatchesCanChange.h
//  iMatches_iphone
//
//  Created by Admin on 10.06.2014.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@protocol MatchesCanChange <NSObject>

-(int) getMatchedSlot:(CGPoint)location;

-(CCSprite*) getMatchFromSlot:(int)slot;


//-(BOOL)isNewMatchInSlotValid:(int)slot;

//-(BOOL)isMatchValidWithoutSlot:(int)slot;




@end
