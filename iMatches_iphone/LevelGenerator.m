//
//  LevelGenerator.m
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 01.03.2012.
//  Copyright (c) 2012 yarkill@gmail.com. All rights reserved.
//

#import "LevelGenerator.h"

@implementation LevelGenerator


-(id)init
{
    self = [super init];
    if (self) {
//        printf("========================================================\n");
        

    }
    return self;
}

-(Equation)makeEquA:(int)a B:(int)b Op:(int)op withResult:(int)result {
//    Equation equ = {.a = a,.b = b,.op = op,.result = result};
    Equation equ = {a,b,op,result};
    return equ;
}


-(NSString*)generateLevelString:(int)level {
    switch (level) {
        case 0: return @"8+0=18";
        case 1: return @"6+7=9";
        case 2: return @"8+6=5";
        case 3: return @"8+9=8";
        case 4: return @"1+2=3-5";
        case 5: return @"-3+5=5-2";
        case 6: return @"68+9=107";
        case 7: return @"7+7=-1+5";
        case 8: return  @"3-2=3";
        case 9: return @"8-9=1";
        case 10: return @"1-9=2";
        case 11: return @"5-6=9";
        case 12: return @"1+1=3";
        case 13: return @"5+6=72";
        case 14: return @"58-3=41";
        case 15 : return @"0+7=9";
            
            
        case 16: return @"2*3=9";
        case 17: return @"2*3=9*6";
        case 18: return @"4*5=3*10";
        case 19: return @"-5-4=-18";
            
        //case 100: return @"88=88888";
            
        //        case : return             
        default: return @"0+7=9";
            break;
    }
    return @"1+1=3";
}






-(Equation)generateDemoLevel:(int)level {
    Equation equ;
    switch (level) {
        case 1: equ = [self makeEquA:6 B:7 Op:EPlusOperator withResult:9]; break;
        case 2: equ = [self makeEquA:8 B:6 Op:EPlusOperator withResult:5]; break;            
        case 3: equ = [self makeEquA:8 B:9 Op:EPlusOperator withResult:8]; break;       
            
        case 4:
            equ.a = 3;
            equ.b = 2;
            equ.op = EMinusOperator;
            equ.result = 3;
            break;
            /// 4 levels prepared so far
        case 5:
            equ.a = 8;
            equ.b = 9;
            equ.op = EMinusOperator;
            equ.result = 1;
            break;
        case 6:
            equ.a = 1;
            equ.b = 9;
            equ.op = EMinusOperator;
            equ.result = 2;
            break;
        case 7:
            equ.a = 5;
            equ.b = 6;
            equ.op = EMinusOperator;
            equ.result = 9;
            break;            
        case 8:
            equ.a = 5;
            
            equ.b = 5;
            equ.op = EPlusOperator;
            equ.result = 8;
            break;
        case 9:
            equ.a = 1;
            equ.b = 1;
            equ.op = EPlusOperator;
            equ.result = 3;
            break;
        case 10:
            equ.a = 5;
            equ.b = 6;
            equ.op = EPlusOperator;
            equ.result = 72;
            break;
            
        case 11:
            equ.a = 58;
            equ.b = 3;
            equ.op = EMinusOperator;
            equ.result = 41;
        default:
            break;
    }
    return equ;
}

// checks if level is already in our database
-(BOOL)weHaveThisLevel:(NSString*) levelString {
    // to do : add saving string levels to file
    return NO; // stub 
}

// checks if level is solved
-(BOOL)equIsValid:(NSString*) levelString {
    return NO;
}

-(BOOL)genLevelValid:(NSString*)equ {

    if ( [self weHaveThisLevel:equ]) {
        return NO; // we have this level, so we don't need same level again
    }
    
    if ( [self equIsValid:equ]) {
        return NO; // generated level is already solved
    }
    return YES;
}

-(void) genTest {
    //NSString * newLevelStr = [NSString stringWithCString:"12+5=8+4"];
    NSString * newLevelStr = [[NSString alloc] initWithString:@"12+5=8+4"];
//    NSString * newLevelStr = [NSString stringWithString:@"12+5=8+4"];
    
    
    NSArray * chunkL = [newLevelStr componentsSeparatedByString:@"="];

    for (NSString * str in chunkL) {
        //NSLog(@"string : %@", str);

        [self computeStr:str];

        // break into chars :
        for (int i=0; i< [str length]; ++i) {
            //NSLog(@"element is : %c", [str characterAtIndex:i] );
        }
        
    }
    
    NSString * wynik = [self computeMultiplications:@"-10*5*8+4"];
    NSLog(@"wynik usuwania mnozen : %@", wynik);
    
    NSLog(@"===========================================================");
    
//    NSLog(@"Trying to solve equation : 12+5=8+4");
//    [self equationStringSolved:@"12+5=8+4"];
//    
//    NSLog(@"Trying to solve equation : 1+5=2+4");
//    [self equationStringSolved:@"1+5=2+4"];
//    
//    NSLog(@"Trying to solve equation : 5=5");
//    [self equationStringSolved:@"5=5"];

    
//    newLevelStr 
}
/*
-(BOOL) equationStringSolved:(NSString*)equation {
    BOOL result = NO;

    NSArray * expressions = [equation componentsSeparatedByString:@"="];
    if ([expressions count] < 2) {
        return NO;
    }

    //NSLog(@" array items count = %d", [expressions count]);
    
    NSString * leftAfterMult = [self computeMultiplications:(NSString*)[expressions objectAtIndex:0]];
    
    NSString * leftAfterDiv = [self computeDivisions:leftAfterMult];
    
    NSString * rightAfterMult = [self computeMultiplications:(NSString*)[expressions objectAtIndex:1]];
    
    NSString * rightAfterDiv = [self computeDivisions:rightAfterMult];
    
    

    //int leftSide = [self computeStr:leftAfterMult];
    //int rightSide = [self computeStr:rightAfterMult];
    
    int leftSide = [self computeStr:leftAfterDiv];
    int rightSide = [self computeStr:rightAfterDiv];

//    int leftSide = [self computeStr:(NSString*)[expressions objectAtIndex:0]];
//    int rightSide = [self computeStr:(NSString*)[expressions objectAtIndex:1]];
    
    NSLog(@"leftSide = %d", leftSide);
    NSLog(@"rightSide = %d", rightSide);
        
    result = (leftSide == rightSide) ? YES : NO;
    
    if (result) NSLog(@"equation solved !");
    else NSLog(@"equation not solved :(");
    
    return result;
}

-(int) computeStr:(NSString*)str {

    NSArray * dirtyNumbers = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"+-/*"]];

    NSArray * dirtyOperands = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]];

    NSMutableArray * numbers = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray * operands = [NSMutableArray arrayWithCapacity:1];
    
    for (NSString * intStr in dirtyNumbers) {
        if ([intStr length] != 0) {
            int val = [intStr intValue];
            NSValue * num = [NSValue value:&val withObjCType:@encode(int)];

            [numbers addObject:num];
        }
    }
    
    for (NSString * opStr in dirtyOperands) {
        if ([opStr length] != 0) {
            
            char op = [opStr characterAtIndex:0];
            NSValue * oper = [NSValue value:&op withObjCType:@encode(char)];
            [operands addObject:oper];
            
        }
    }

    // this section is trying to COMPUTE whole expersion and return result
    
    // computed output :
    int firstNumber = 0;
    
    // iterators :
    int operIt = 0;
    int numberIt = 0;
    if ([operands count] == [numbers count]) {
        // we have 1 operand befor first value 
        NSValue * firstSign = (NSValue*)[operands objectAtIndex:0];
        char sign;
        [firstSign getValue:&sign];
        
        NSValue * currNumber = (NSValue*)[numbers objectAtIndex:0];
        int currNumberInt;
        [currNumber getValue:(&currNumberInt)];
        
        if (sign == '-') 
            firstNumber -= currNumberInt;
        else {
            firstNumber += currNumberInt;
        }
        
        // pierwsza cyfra ujemna lub dodatnia uwzgledniona
        // teraz trzeba operowac na kolejnych
        operIt++;
        numberIt++;
    } else {
        NSValue * currNumber = (NSValue*)[numbers objectAtIndex:0];
        int currNumberInt;
        [currNumber getValue:(&currNumberInt)];
        firstNumber += currNumberInt;
        numberIt++;
    }
    
    while (operIt < [operands count] && numberIt < [numbers count]) {
        NSValue * operand = [operands objectAtIndex:operIt];
        char oper;
        [operand getValue:(&oper)];
        
        NSValue * nextNum = [numbers objectAtIndex:numberIt];
        int num;
        [nextNum getValue:(&num)];
        
        switch (oper) {
            case '-':
                firstNumber -= num;
                break;
                
            case '+':
                firstNumber += num;
                break;
                
            default:
                break;
        }
        operIt++;
        numberIt++;
        
    }

    
    // wynik jest poprawnie wyliczony tylko dla rownan bez * i /
    printf("wynik : %d \n", firstNumber);
    return firstNumber;
}

-(NSString*) computeMultiplications:(NSString*) str {

    NSLog(@"%@ : %@", @"computeMultiplications (str) ", str);
    
    NSError * error = NULL;
    
    NSRegularExpression * regexp = [NSRegularExpression regularExpressionWithPattern:@"[0-9]*\\*[0-9]*" options:NSRegularExpressionCaseInsensitive error:(&error)];
    
    NSTextCheckingResult * result = [regexp firstMatchInString:str options:0 range:NSMakeRange(0, [str length])];
    
    NSRange resultRange = [result range];
    NSLog(@"range len: %d", [result range].length);
    NSLog(@"range loc: %d", [result range].location);
    
    NSLog(@"result : %@", result);

    //    NSInteger offset = 0;
    //    resultRange.location += offset;
//    if (resultRange.length > 0) {
    if (result != nil) {
    
        NSMutableString * strCopy = [str mutableCopy];
    
        NSString * match = [regexp replacementStringForResult:result inString:strCopy offset:0 template:@"$0"];
    
        NSLog(@"regexp match : %@", match);

        //    [strCopy replaceCharactersInRange:resultRange withString:@"(DUPA)"];
        [strCopy replaceCharactersInRange:resultRange withString:[self resolveMultString:match]];
        
        
        return strCopy;
    }
    else {
        return str;
    }
}
*/
/*
-(NSString*) computeDivisions:(NSString*) str {

    NSLog(@"%@ : %@", @"computeDivisions (str) ", str);
    
    NSError * error = NULL;
    
    NSRegularExpression * regexp = [NSRegularExpression regularExpressionWithPattern:@"[0-9]*\\/[0-9]*" options:NSRegularExpressionCaseInsensitive error:(&error)];
    
    NSTextCheckingResult * result = [regexp firstMatchInString:str options:0 range:NSMakeRange(0, [str length])];
    
    NSRange resultRange = [result range];
    NSLog(@"range len: %d", [result range].length);
    NSLog(@"range loc: %d", [result range].location);
    
    NSLog(@"result : %@", result);
    
    //    NSInteger offset = 0;
    //    resultRange.location += offset;
    //    if (resultRange.length > 0) {
    if (result != nil) {
        
        NSMutableString * strCopy = [str mutableCopy];
        
        NSString * match = [regexp replacementStringForResult:result inString:strCopy offset:0 template:@"$0"];
        
        NSLog(@"regexp match : %@", match);
        
        //    [strCopy replaceCharactersInRange:resultRange withString:@"(DUPA)"];
        [strCopy replaceCharactersInRange:resultRange withString:[self resolveDivString:match]];
        
        return strCopy;
    }
    else {
        return str;
    }
}
*/
/*
// solving only strings like : 2*4 or 15*3 (number op number)
-(NSString*) resolveMultString:(NSString*) str {
    
    int result = 0; // zero is assumed as usually wrong solution !!!
    
    NSArray * dirtyNumbers = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"*"]];

    if ([dirtyNumbers count] > 1 ) {
        int first = [[dirtyNumbers objectAtIndex:0] intValue];
        int second = [[dirtyNumbers objectAtIndex:1] intValue];
    
        result = first * second;
    }
    
    return [NSString stringWithFormat:@"%d", result];
}

-(NSString*) resolveDivString:(NSString*) str {
    
    int result = 0; // zero is assumed as usually wrong solution !!!
    
    NSArray * dirtyNumbers = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/"]];
    
    if ([dirtyNumbers count] > 1 ) {
        int first = [[dirtyNumbers objectAtIndex:0] intValue];
        int second = [[dirtyNumbers objectAtIndex:1] intValue];
    
        result = first / second;
    }
        
    return [NSString stringWithFormat:@"%d", result];
}
*/
/*
-(Equation)generateNewLevel {
    
}
*/

-(Equation)generateFirstLevel {
    
    Equation equation;
    
    
    equation.a = CCRANDOM_0_1() * 10;
//    while (![self matchCanBeAdded:equation.a] || ![self matchCanBeRemoved:equation.a]) {
//        equation.a = CCRANDOM_0_1() * 10;
//    }
    equation.b = CCRANDOM_0_1() * 10;
//    while (![self matchCanBeAdded:equation.b] || [self matchCanBeRemoved:equation.b]) {
//        equation.b = CCRANDOM_0_1() * 10;
//    }

    equation.op = CCRANDOM_0_1()*2;
    if (equation.op)
        equation.result = equation.a + equation.b;
    else {
        if (equation.a < equation.b) {
            int tmp = equation.a;
            equation.a = equation.b;
            equation.b = tmp;
        }
        equation.result = equation.a - equation.b;
    }
    printf("random number 1: %d\t number 2: %d\t op: %d\n", equation.a, equation.b, equation.op);
    
    int size = 2;
    // hardcode double numbers
    if (equation.result > 9) {
        size = 4;
    }
        
    
    return equation;
}

//-(Equation)messThingsUp:(int[])numbers withSize:(int)size {
//    const int size =4;
//    //int numbers[size];
//    Equation equ;
//    int i,j;
//    for (i=0; i<size; ++i) {
//        for (j=0; j<size; ++j) {
//            if (j != i) {
//                if ( [self matchCanBeRemoved:numbers[i]] && [self matchCanBeAdded:numbers[j]]) {
//                    [self removeMatch:numbers[i]];
//                    [self addMatch:numbers[j]];
//                    return;
//                }
//                else if ( [self matchCanBeRemoved:numbers[j]] && [self matchCanBeAdded:numbers[i]]) {
//                        [self removeMatch:numbers[j]];
//                        [self addMatch:numbers[j]];
//                        return;
//                    }
//
//            }
//        }
//    }
//    
//}

-(int)removeMatch:(int)a {
// add static tables with possible numbers generated from digit 
    BOOL which = (CCRANDOM_0_1()*2 > 0);
    switch (a) {
        case 6:
            return 5;
            break;
        case 7:
            return 1;
            break;
        case 8:
            if (which)
                return 0;
            else 
                return 9;
            break;
        case 9:
            if (which)
                return 3;
            else
                return 5;
            break;
            
        default:
            break;
    }
    printf("WARRNING SOME BAD SHIT HAPPEND IN : LevelGenerator -> removeMatch\n");
    return -1;
}

-(int)addMatch:(int)a {
    BOOL which = (CCRANDOM_0_1()*2 > 0);
    switch (a) {
        case 0:
            return 8;
            break;
        case 1:
            return 7;
            break;
        case 3:
            return 9;
            break;
        case 5:
            if (which)
                return 6;
            else
                return 9;
            break;
        case 6:
            return 8;
            break;
        case 9:
            return 8;
            break;
        default:
            break;
    }
    printf("WARRNING SOME BAD SHIT HAPPEND IN : LevelGenerator -> removeMatch\n");
    return -1;
}

-(BOOL)matchCanBeRemoved:(int)val {
    switch (val) {
        case 6: case 7 : case 8: case 9:
            return YES;
            break;
            
        default:
            return NO;
            break;
    }
    return NO; // never reach this
}

-(BOOL)matchCanBeAdded:(int)val {
    switch (val) {
        case 0: case 1: case 3: case 5: case 6: 
        case 9:
            return YES;
            break;
            
        default:
            return NO;
            break;
    }
    return NO; // never reach this
}

-(BOOL)matchCanBeMoved:(int)val {
    switch (val) {
        case 0: case 2: case 3: case 5:
        case 6: case 9:
            return YES;
            break;
            
        default:
            return NO;
            break;
    }
    return NO; // never reach this
}

-(void)dealloc {

    [super dealloc];
}


@end
