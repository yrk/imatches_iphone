//
//  GameScene.h
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 30.06.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


#import "GameLayer.h"
#import "MenuBar.h"
#import "LevelManager.h"
#import "MenuScene.h"

@interface GameScene : CCScene {
    CCArray * levels;
    
    CGSize size;
    
    BOOL longPressedFlag;

} 

@property (nonatomic, retain) GameLayer * gameLayer;
@property (nonatomic, retain) LevelManager * levelManager;

@property (nonatomic, strong) MenuBar * menuBar;

@property (nonatomic, assign) uint currentLevelIndex;


@property (nonatomic, strong) UISwipeGestureRecognizer * swipe_recognizer;
@property (nonatomic, strong) UILongPressGestureRecognizer * longPressRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer * double_tap_recognizer;


+(id) scene;

/// setup gesture recognizers to use with Game Layer
-(void) setupGestureRecognizers;

/// remove all gesture recognizers from Game Layer
-(void) removeGestureRecognizersFromGameLayer:(GameLayer*) game_layer;

/// add all gesture recognizers to Game Layer
-(void) addGestureRecognizersForGameLayer:(GameLayer*) game_layer;
    
    
    
@end
