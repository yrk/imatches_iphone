//
//  LevelPickerScene.h
//  iMatches_iphone
//
//  Created by Admin on 11.06.2014.
//
//

//#import "CCScene.h"

#import "cocos2d.h"
#import "SimpleAudioEngine.h"
#import "LevelManager.h"

@interface LevelPickerScene : CCScene {
  
  
  CCLayer * layer;
  CCScene * menuScene;
  
  CCLabelTTF *aboutTextLabel;
  
  NSString * aboutText;
  
  CGPoint lastLocation;
  CGPoint newLocation;
  CGPoint difference;
  
  
  // positions in equ
  CGPoint equPositions[8];
  // positions in digit/operation - 0 is none, 8-12 are operators
  CGPoint digitPositions[13];
  
  //UIPanGestureRecognizer * panGestureRecognizer;
}

@property(nonatomic, retain) CCScene *menuScene;
@property(nonatomic, retain) UIPanGestureRecognizer * panGestureRecognizer;

@property (nonatomic, retain) LevelManager * levelManager;

/// setups tiny match positions
-(void) setupPositions;

@end
