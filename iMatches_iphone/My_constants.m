//
//  My_constants.m
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 21.11.2011.
//  Copyright 2011 yarkill@gmail.com. All rights reserved.
//

#import "My_constants.h"


//const int MATCH_HEIGHT = 75;
//const int MATCH_WIDTH = 16;


const float half_ne_width = NUMBER_ELEMENT_WIDTH * 0.5;      
const float half_ne_height = NUMBER_ELEMENT_HEIGHT * 0.5;

const float ne_offset = 10;

const float half_match_width = MATCH_WIDTH * 0.5;

unsigned int NumbersToAdd[] = {0, 1, 3, 5, 6, 9};
unsigned int NumbersToRemove[] = {6, 7, 8, 9};
unsigned int NumbersToMove[] = {0, 2, 3, 5, 6, 9};