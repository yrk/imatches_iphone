//
//  AboutScene.h
//  iMatches_iphone
//
//  Created by Admin on 27.01.2014.
//
//

//#import "CCScene.h"

#import "cocos2d.h"
#import "SimpleAudioEngine.h"

@interface AboutScene : CCScene {

    CCLayer * layer;
    CCScene * menuScene;
    
    CCLabelTTF *aboutTextLabel;
    
    NSString * aboutText;
    
    CGPoint lastLocation;
    CGPoint newLocation;
    CGPoint difference;
    
    //UIPanGestureRecognizer * panGestureRecognizer;
}

@property(nonatomic, retain) CCScene *menuScene;
@property(nonatomic, retain) UIPanGestureRecognizer * panGestureRecognizer;

@end
