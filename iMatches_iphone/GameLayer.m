//
//  HelloWorldLayer.m
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 11.10.2011.
//  Copyright yarkill@gmail.com 2011. All rights reserved.
//


// Import the interfaces
#import "GameLayer.h"
#import "CCDrawingPrimitives.h"
#import "SimpleAudioEngine.h"

#import "LevelManager.h"
#import "EquationElement.h"
#import "Digit.h"
#import "Operation.h"
#import "Equals.h"

#define LEVEL_MAX 19
#define LEVEL_START 0


// HelloWorldLayer implementation
@implementation GameLayer

@synthesize swipe_recognizer;
@synthesize longPressRecognizer;
@synthesize double_tap_recognizer;

@synthesize levelManager;

-(id)initWithLevelManager:(LevelManager*) lvlMgr{
  if (self= [self init]) {
    self.levelManager = lvlMgr;
    [self initLevel];
  } else {
    NSLog(@"GameLayer [initWithLevelManager] - cannot init self");
  }
  
  return self;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
    
    space = [[PhysicsManager instance] space];
    //CGSize size = [[CCDirector sharedDirector] winSize];
    
    // timers should be moved to GameScene class
    timeElapsed = 0.0f;
    levelTimeElapsed = 0.0f;
    /////////////////////////
    
    
    
    // TAKEN FROM LevelManager :
    
    
     // Non transparent background fix
     //CCLayerColor * bgLayer = [[[CCLayerColor alloc] initWithColor:ccc4(0, 0, 0, 255)] autorelease];
     //[self addChild:bgLayer z:0];
    
    
    levelNumber = LEVEL_START;
    
    //levelCleared = NO;
    
    size = [[CCDirector sharedDirector] winSize];
    
    //[self reloadConfig];
    
    //numbers = [[CCArray alloc] initWithCapacity:2];
    
    equElements = [[CCArray alloc] initWithCapacity:2];
    
    //[self initLevel];
    //[self addMenuBar];
    
    //nextPos = 0;
    
    NSLog(@"Running [GameLayer initLevel");
    
    //[self initLevel];
    
    levelEnded = NO;
    equalsCleared = NO;
    
    /*
     Digit * digit = [[Digit alloc] init];
     [digit makeNumber:8];
     [digit setPosition:ccp(0,0)];
     [self addChild:digit];
     */
    
  }
  
  soundEngine = [SimpleAudioEngine sharedEngine];
  
  [self schedule:@selector(update:) interval:0.0f];
  
	return self;
}

// to be removed
-(void)addNewDigit:(Digit*) digit {
  [self addChild:digit z:25];
  [equElements addObject:digit];
  
}

-(void)addNewEquationElement:(EquationElement*) element {
  NSLog(@"%@ : %c", @"[GameLayer addNewEqatuionElement] with value :", [element getCharValue]);
  [self addChild:element z:25];
  [equElements addObject:element];
}

-(void)initLevel {
  
  levelCleared = NO;
  
  
  // where the fuck is this used ?!
  // this is used for pan and translation purpouses
  oldLocation = CGPointZero;
  newLocation = CGPointZero;
  
  NSLog(@"Running [LevelManager setupNewLevel:]");
  
  [levelManager setupNewLevelForLayer:self];
  
  equalsCleared = [levelManager isEqualClearedInLevel];
  
  NSLog(@"Finished [GameLayer initLevel]");
  
}



-(void)setupSwipeGestureRecognizer {
  swipe_recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
  
  [swipe_recognizer setDirection:UISwipeGestureRecognizerDirectionDown];
  
  swipe_recognizer.numberOfTouchesRequired =1;
  
  // DISABLE SWIPE RECOGNIZER !!!
  // [[[CCDirector sharedDirector] openGLView].superview addGestureRecognizer:swipe_recognizer];
}

-(void)addCongratsLabel {
  congratsLabel = [CCLabelTTF labelWithString:@"Level complete !" fontName:@"Marker Felt" fontSize:64];
  //[congratsLabel setAnchorPoint:ccp(0,0)];
  [congratsLabel setPosition:ccp(240,160)];
  [self addChild:congratsLabel z:50 tag:100];
}

-(void)addBackground {
  
#ifdef BACKGROUND_ENABLED
  
  CCSprite * bg = [CCSprite spriteWithFile:@"imatches-bg1.png"];
  [bg setPosition:ccp(size.width/2, size.height/2)];
  [self addChild:bg z:0 tag:EBackgroundTag];
#endif
  
}

-(void) onExitTransitionDidStart {
  
  CCLOG(@"[%@] : %@ ", self, NSStringFromSelector(_cmd));
  
  [self stopAllActions];
  [self pauseSchedulerAndActions];
  //  [self unscheduleUpdate];
}

-(void)onEnterTransitionDidFinish {

  CCLOG(@"[%@] : %@ ", self, NSStringFromSelector(_cmd));
  
  [self resumeSchedulerAndActions];
  [self schedule:@selector(update:)];
//  [self schedule:@selector(update:) interval:0.0f];
}

-(void) handleDoubleTap:(UITapGestureRecognizer *) paramSender {
  CCLOG(@" -------------          ------------ DOUBLE TOUCH DETECTED");
  
  if(paramSender.state == UIGestureRecognizerStateRecognized)
  {
    
    //CGPoint location = [self convertTouchToNodeSpace:touch];

    
    CGPoint point = [paramSender locationInView:paramSender.view];
    // x and y is interchanged while in lanscape view !!!!!!!!!!!!!!!!!!
    CCLOG(@" dobule tap location : %f, %f", point.x, point.y );
    
//    [self checkEqualsDoubleTap:point];
    [self checkEqualsDoubleTap:[self convertToWorldSpace:point]];
    
  }
  
  if (!levelEnded) {
    levelEnded = [levelManager checkResultForString:[self resultStringToCheck]];
  }
  
  [self endLevel];
}

/*
 -(void)handleLongPress:(UILongPressGestureRecognizer *)paramSender {
 if (!longPressedFlag) {
 //        [levelManager abortTouch];
 [[[GameManager instance] menuBar] menuBarAction];
 CCLOG(@"================ LONG PRESS DETECTED ! =============");
 }
 longPressedFlag = !longPressedFlag;
 }
 */

-(void)handleSwipes:(UISwipeGestureRecognizer *)paramSender {
  if (paramSender.direction & UISwipeGestureRecognizerDirectionLeft) {
    CCLOG(@" Swipe w Lewo");
    NSLog(@"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  }
  if (paramSender.direction & UISwipeGestureRecognizerDirectionRight) {
    CCLOG(@" ! Swipe w Prawo");
  }
  if (paramSender.direction & UISwipeGestureRecognizerDirectionUp) {
    //CCLOG(@" ! Swipe w Lewo");
    NSLog(@"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  }
  if (paramSender.direction & UISwipeGestureRecognizerDirectionDown) {
    CCLOG(@" ! Swipe w Lewo");
    NSLog(@"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  }
}


-(void) resetLevelTimer {
  levelTimeElapsed = 0.0f;
}


-(void) update:(ccTime) dt {
  
  
  
  if (!levelEnded) {
    timeElapsed += dt;
    //[scoreLabel setString:[NSString stringWithFormat:@"%.1f", timeElapsed]];
    
    levelTimeElapsed += dt;
    //[levelTimeLabel setString:[NSString stringWithFormat:@"%.1f", levelTimeElapsed]];
    
    [[[GameManager instance] menuBar] updateTimeElapsed:timeElapsed andLevelTime:levelTimeElapsed];
  }
  
  if (levelEnded) {
    // update chipmunk
    //cpSpaceStep(space, dt);
    
    [[PhysicsManager instance] updatePhysics:dt];
    
    //[levelManager update];
    [self updatePhysicsBodies];
  }
  
  
  
}


-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
  CGPoint location = [self convertTouchToNodeSpace:touch];
  
  //    [[SimpleAudioEngine sharedEngine] playEffect:@"pstryk.wav"];
  if (!levelEnded) {
    [self startTouch:location];
  } else {
    if (!levelCleared) {
      [self reset];
      [self removeChildByTag:100 cleanup:YES];
      
      levelEnded = NO;
      [self initLevel];
    }
    
    return NO;
  }
  return YES;
  //    return TRUE;
}


-(NSString*)resultStringToCheck {
  // please change name of current_level to something more descriptive and accurate !!!
  
  NSMutableString * current_level = [[[NSMutableString alloc] initWithString:@""] autorelease];
  
  //NSLog(@" in checkResult ... ");
  NSLog(@"%@ : %d", @"equElements.count", [equElements count]);
  
  for (EquationElement* equElem in equElements) {
    char equChar = [equElem getCharValue];
    [current_level appendString:[NSString stringWithFormat:@"%c",equChar]];
    //NSLog(@"equ element : %c", equChar);
  }
  
  NSLog(@"%@ : %@", @"current_level", current_level);
  return current_level;
}

-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
  // touch location
  CGPoint location = [self convertTouchToNodeSpace:touch];
  [self moveTouch:location];
}

-(void) endLevel {
  if (levelEnded) {
    // play TA DA !!!
    [self resetLevelTimer];
    [[SimpleAudioEngine sharedEngine] playEffect:@"tada.wav"];
    
    printf("We need to generate new level bitches !!!\n");
    [self addCongratsLabel];
  }
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
  
  CGPoint location = [self convertTouchToNodeSpace:touch];
  
  if (!levelEnded) {
    [self endTouch:location];
    //levelEnded = [levelManager checkResult];
    levelEnded = [levelManager checkResultForString:[self resultStringToCheck]];
  }
  
  [self endLevel];
  
}

-(void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
  //CGPoint location = [self convertTouchToNodeSpace:touch];
  
  [self abortTouch];
  //[levelManager endTouch:location];
  //levelEnded = [levelManager checkResult];
}

-(void)reset {
  levelCleared = YES;
  
  
  for (EquationElement* element in equElements) {
    [element removeFromParentAndCleanup:YES];
    
  }
  
  //[numbers removeAllObjects];
  [equElements removeAllObjects];
  
  // using random values from 1 to 4
  //    int demo = 1+ CCRANDOM_0_1() * 4;
  
  //nextPos = 0;
}


-(void)resetSelectedSprite {
  selectedSprite.digit = nil;
  selectedSprite.position = CGPointZero;
  selectedSprite.slot = 0;
  selectedSprite.sprite = nil;
  selectedSprite.updated = NO;
}


//-(int)getDigitCloseToTouch:(CGPoint)location toDigit:(Digit*)digit  {
//-(Digit*)getDigitCloseToTouch:(CGPoint)location  {
-(EquationElement*)getElementCloseToTouch:(CGPoint)location  {
  
  float minDistance = 1000;
  float dist;
  //Digit* digitToProcess = nil; // digit to add match to
  EquationElement * closestElement = nil;
  
  //    printf("czy ta funkcja w ogole jest wywolywana ???????????????\n");
  
  //for (Digit * digit in numbers) {
  for (EquationElement * elem in equElements) {
    //if ([elem isMemberOfClass:[Digit class]] == YES) {
    if ( (dist = ccpDistance(location,elem.position)) < minDistance) {
      minDistance = dist;
      closestElement = (Digit*) elem;
    }
    //} else if ([elem isMemberOfClass:[Operation class]] == YES) {
    //    printf("wlasnie dotykam operatorka ! ----------++++++++\n");
    //}
  }
  
  if (closestElement != nil) {
    CCLOG(@"[%@] - %@ : element to process value : %d",
          self, NSStringFromSelector(_cmd), [closestElement getCharValue]);
  } /*else {
     return 0;
     }
     
     CGPoint relLoc= ccpSub(location, digitToProcess.position);
     
     
     int slot = 0;
     slot = [digitToProcess getSlotIn:relLoc taken:NO];
     
     digit = digitToProcess;
     
     return slot;
     */
  return closestElement;
}

-(Digit*) checkDigitForMatchHighlight:(CGPoint)location {
  
  EquationElement * elem = [ self getElementCloseToTouch:location];
  if ([elem isMemberOfClass:[Digit class]] == YES) {
    Digit* digitToProcess = (Digit*) elem;
    return digitToProcess;
    
  } else {
    return nil;
  }
}

-(unsigned int) checkSlotForMatchHighlightForDigit:(Digit*)digit andRelLoc:(CGPoint)relLoc {
  
  int slot = 0;
  
  //if ((digitToProcess = [ self getDigitCloseToTouch:location]) != nil) {
  
  slot = [digit getSlotIn:relLoc taken:NO];
  //}
  
  return slot;
  
}

#pragma mark -
-(void)startTouch:(CGPoint)location {
  
  startTouchLocation = location;
  newLocation = location;
  
  [self selectSpriteForTouch:location];
}

-(void)moveTouch:(CGPoint)location {
  oldLocation = newLocation;
  newLocation = location;
  
  NSLog(@"current (x: %.1f  y: %.1f)",location.x,location.y);
  
  
  /// highlight process for Digits !
  Digit * digit = [self checkDigitForMatchHighlight:location];
  
  if (lastHighlightedDigit != digit) {
    [lastHighlightedDigit clearHighlight];
  }
  
  
  CGPoint relLoc= ccpSub(location, digit.position);
  int slot = [self checkSlotForMatchHighlightForDigit:digit andRelLoc:relLoc];
  
  if (slot !=0) {
    [digit highlightSlot:slot];
  } else {
    [digit clearHighlight];
  }
  
  lastHighlightedDigit = digit;
  
  CGPoint translation = ccpSub(newLocation, oldLocation);
  //////////////////////////////////////////////// highlight process for Digit !
  
  /// transition
  [self panForTranslation:translation ];
}

-(void)endTouch:(CGPoint)location {
  
  NSLog(@"ended (x: %.1f  y: %.1f)",location.x,location.y);
  
  
  if (selSprite == nil) {
    // here we can process UI things like butons, swipes and stuff ?
    NSLog(@"newLocation value : %.2f , %.2f", startTouchLocation.x, startTouchLocation.y);
    NSLog(@"location value : %.2f , %.2f", location.x, location.y);
    
    CGPoint offset = ccpSub(location, startTouchLocation);
    
    NSLog(@"offset : %.2f , %.2f", offset.x, offset.y);
    
    return;
  }
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  /// getting digit to add match, checking which slot should be filled with moved match
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  
  EquationElement* elemToAddMatchTo = nil;
  
  elemToAddMatchTo = [ self getElementCloseToTouch:location];
  
  if (elemToAddMatchTo == nil ) {
    [self abortTouch];
  }
  
  if ([elemToAddMatchTo isMemberOfClass:[Digit class]] == YES) {
    
    
    
    Digit * digitToAddMatchTo = (Digit*) elemToAddMatchTo;
    
    if (digitToAddMatchTo != nil) {
      printf(" ==== digit to process value : %d\n", [digitToAddMatchTo getValue]);
      
      [digitToAddMatchTo clearHighlight];
    } else {
      return;
    }
    
    CGPoint relLoc= ccpSub(location, digitToAddMatchTo.position);
    
    int slot = 0;
    //    slot = [self getDigitCloseToTouch:location toNumber:numberToProcess toDigit:digitToProcess];
    
    
    //printf("touchEnded : relLoc.x = %f.1 \t relLoc.y = %f.1",relLoc.x, relLoc.y);
    //NSLog(@"selSprite (x: %.1f  y: %.1f)", relLoc.x,relLoc.y);
    
    slot = [digitToAddMatchTo getSlotIn:relLoc taken:NO];
    
    printf(" slot for digitToProcess = %d \n", slot);
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /// process adding match : to same digit as taken or to different one
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    
    if ( slot != 0 ) {
      // TO DO : check if adding match to slot makes sense
      // ...
      if (selDigit == digitToAddMatchTo) {
        
        // this is the same match from touch begin
        
        if (![digitToAddMatchTo isNewMatchInSlotValid:slot without:selSlot]) {
          [self abortTouch];
          return;
        }
        
      } else {
        
        BOOL isValidInSlot = [digitToAddMatchTo isNewMatchInSlotValid:slot];
        BOOL isValidWithoutSlot = [selDigit isMatchValidWithoutSlot:selSlot];
        
        // from one match to another
        printf("digitToProcess isNewMatchInSlotValid:slot = %d \t slot: %d\n", isValidInSlot, slot);
        printf("selDigit isMatchValidWithoutSlot:selSlot = %d \t selSlot: %d\n", isValidWithoutSlot, selSlot);
        if (!isValidInSlot || !isValidWithoutSlot) {
          printf("Validation failed-> touch aborted\n");
          [self abortTouch];
          return;
        }
      }
      
      // process moving match ...
      Digit* selParent = (Digit*)[selSprite parent];
      [selParent removeMatch:selSprite withSlot:selSlot];
      selSprite = nil;
      [digitToAddMatchTo addMatchNumber:slot];
      
      printf(" changed match value to : %d\n", [digitToAddMatchTo getValue]);
    }
    else {
      // reset selected match and return
      //[selSprite setPosition:selPos];
      [self abortTouch];
    }
    // ready for checking end of the game !
    //[self checkResult];
  } else if ([elemToAddMatchTo isMemberOfClass:[Operation class]]) {
    Operation * operToAddMatchTo = (Operation*) elemToAddMatchTo;
    if ([operToAddMatchTo canRecieveMatch]) {
      [operToAddMatchTo addMatch];
    } else {
      [self abortTouch];
    }
    EquationElement* selParent = [selSprite parent];
    [selParent removeMatch:selSprite withSlot:selSlot];
    selSprite = nil;
  }
  else {
    // reset selected match and return
    //[selSprite setPosition:selPos];
    [self abortTouch];
  }
  
}

-(void)abortTouch {
  [selSprite setPosition:selPos];
  selSprite = nil;
}

// checks if DoubleTap gesture was made on equals or plus operator
// if so, we change them from '=' to '=' or vice versa !
-(void) checkEqualsDoubleTap:(CGPoint)tapLocation {
  
  selSprite = nil;
  
  // change x,y to y,x as we are horizontaly (not reflected in UIView): -- > now it's different
  //CGPoint touchLocation = CGPointMake(tapLocation.y, tapLocation.x);
  
  
  /// for GAME_AUTOROTATION == kGameAutorotationUIViewController
  CGPoint touchLocation = CGPointMake(tapLocation.x, size.height - tapLocation.y);
  CCLOG(@" dobule touch location - after change : %f, %f", touchLocation.x, touchLocation.y );
  
  //printf("numbers count : %d\n",numbers.count);
  
  //int slot = 0;
  
  //for (Digit* current in numbers) {
  for (EquationElement* current in equElements) {
    if ([current isMemberOfClass:[Operation class]] == YES) {
      char current_char_value = [current getCharValue];
      CGPoint relLoc= ccpSub(touchLocation,current.position);
      
      // check if plus or equals is in range of tap
      // ...
      if ([(Operation*)current operatorIntersectLocation:relLoc]) {
        // change equals to plus or other way around
        if (current_char_value == '+') {
          if (equalsCleared) {
            [(Operation*)current changeToEquals];
            equalsCleared = !equalsCleared;
          }
        } else if (current_char_value == '=') {
          if (!equalsCleared) {
            [(Operation*)current changeEqualsToPlus];
            equalsCleared = !equalsCleared;
          }
        }
      }
    }
  }
}

-(void)selectSpriteForTouch:(CGPoint)touchLocation {
  
  selSprite = nil;
  
  //printf("numbers count : %d\n",numbers.count);
  
  int slot = 0;
  
  //for (Digit* current in numbers) {
  for (EquationElement* current in equElements) {
    if ([current isMemberOfClass:[Digit class]] == YES) {
      if (selSprite == nil) {
        
        CGPoint relLoc= ccpSub(touchLocation,current.position);
        
        if ((slot = [current getMatchedSlot:relLoc]) != 0) {
          [[SimpleAudioEngine sharedEngine] playEffect:@"pstryk.wav"];
          
          selSprite = [current getMatchFromSlot:slot];
          selPos = selSprite.position;
          selDigit = current;
          selSlot = slot;
        }
        
      }
    }
    
    else if ([current isMemberOfClass:[Operation class]] == YES) {
      if (selSprite == nil) {
        CGPoint relLoc= ccpSub(touchLocation,current.position);
        //if ((slot = [current getMatchedSlot:relLoc]) != 0) {
        if ((slot = [current getMatchedSlot:touchLocation]) != 0) {
          [[SimpleAudioEngine sharedEngine] playEffect:@"pstryk.wav"];
          
          selSprite = [current getMatchFromSlot:slot];
          selPos = selSprite.position;
          selDigit = current;
          selSlot = slot;
        }
      }
    }
    
  }
  
  //    stop touch recognizers :
  if (selSprite ) {
    //lpgRecognizer state =
  }
  
#ifdef GAME_LAYER_DEBUG
  CCLOG(@"[%@] - ( %@ )sel sprite : %d", self, NSStringFromSelector(_cmd), selSprite.tag);
#endif
}

#pragma mark -
-(void)panForTranslation:(CGPoint)translation {
  CGPoint newPos = ccpAdd(selSprite.position, translation);
  if (selSprite != nil) {
    [selSprite setPosition:newPos];
  }
  
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
  [swipe_recognizer release];
  [longPressRecognizer release];
  
  
  [equElements release];
  
  [levelManager release];
  
	[super dealloc];
}

-(cpSpace *) getSpace {
  return space;
}


-(void)updatePhysicsBodies {
  for (EquationElement * elem in equElements) {
    [elem updatePhysicsBodies];
  }
  
}


@end
