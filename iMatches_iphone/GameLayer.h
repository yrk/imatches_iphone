//
//  HelloWorldLayer.h
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 11.10.2011.
//  Copyright yarkill@gmail.com 2011. All rights reserved.
//

#import "chipmunk.h"

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
//#import "My_constants.h"

//#import "Digit.h"
//#import "LevelManager.h"
#import "My_constants.h"

#import "SimpleAudioEngine.h"
#import "PhysicsManager.h"

#import "GameManager.h"
#import "DragNDrop.h"
//#import "Slot.h"

@class LevelManager;
@class Digit;
@class EquationElement;

// HelloWorldLayer
@interface GameLayer : CCLayer
{
    DragNDrop * dragNdrop;
    
    cpSpace *space; // chipmunk space - is this name ok ?
    
    Digit * lastHighlightedDigit;

    CCLabelTTF * congratsLabel;
    
    BOOL levelEnded;
    
    float timeElapsed;
    float levelTimeElapsed;    
    
    // test variable
    SimpleAudioEngine * soundEngine;
    
    //UILongPressGestureRecognizer * longPressRecognizer;

    
    
    // things taken form LevelManager :
    CCSprite * selSprite;
    CGPoint selPos;
    int selSlot;
    
    //int nextPos; // for placement
    
    Digit * selDigit;
    
    SelectedSprite selectedSprite;
    
    //MenuBar * menuBar;
    
    //CCArray* numbers;
    CCArray * equElements;
    
    //Equation level;
    
    CGPoint oldLocation;
    CGPoint newLocation;
    
    CGPoint startTouchLocation;
    
    CGSize size;
    BOOL levelCleared;
    
    BOOL isDebugMode;
    BOOL isBackgroundEnabled;
    
    
    unsigned int levelNumber;
    
    BOOL equalsCleared;
    
}

@property (nonatomic, strong) LevelManager * levelManager;

@property (nonatomic, strong) UISwipeGestureRecognizer * swipe_recognizer;
@property (nonatomic, strong) UILongPressGestureRecognizer * longPressRecognizer;

@property (nonatomic, strong) UITapGestureRecognizer * double_tap_recognizer;

@property (nonatomic, readwrite) BOOL physics;


-(cpSpace *) getSpace;

-(void) resetLevelTimer;

-(void)handleSwipes:(UISwipeGestureRecognizer *)paramSender;
//-(void)handleLongPress:(UILongPressGestureRecognizer *)paramSender;

-(void) handleDoubleTap:(UITapGestureRecognizer *) paramSender;

// to be removed
-(void)addNewDigit:(Digit*) digit;

-(void)addNewEquationElement:(EquationElement*) element;


-(id)initWithLevelManager:(LevelManager*) lvlMgr;

//-(Digit*)getDigitCloseToTouch:(CGPoint)location;
-(EquationElement*)getElementCloseToTouch:(CGPoint)location;

/*
-(void)addCongratsLabel;
-(void)addBackground;
-(void) onExitTransitionDidStart;
-(void)onEnterTransitionDidFinish;
-(void) resetLevelTimer;
-(void) update:(ccTime) dt;
-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event;
-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event;
-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event;
-(void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event;
*/

@end
