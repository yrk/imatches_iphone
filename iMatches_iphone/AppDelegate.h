//
//  AppDelegate.h
//  iMatches_iphone
//
//  Created by Admin on 25.01.2014.
//  Copyright __MyCompanyName__ 2014. All rights reserved.
//
/*
#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

@end
*/

//
//  AppDelegate.h
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 11.10.2011.
//  Copyright yarkill@gmail.com 2011. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

// scenes
#import "GameScene.h"
#import "MenuScene.h"

// layers
#import "MenuLayer.h"
#import "GameLayer.h"

#import "GameManager.h"

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
    
    //    CCScene* scene;
    GameScene* scene;
    MenuScene * menuScene;
    
    
    
    GameLayer * gameLayer;
    MenuLayer * menuLayer;
}



@property (nonatomic, retain) UIWindow *window;

@end
