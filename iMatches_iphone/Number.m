//
//  Number.m
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 01.03.2012.
//  Copyright (c) 2012 yarkill@gmail.com. All rights reserved.
//

#import "Number.h"
#import "My_constants.h"

@implementation Number

-(void)addDigit:(int)num {

    Digit* digit = [[Digit alloc] initWithNumber:num];
    [digit setPosition:CGPointMake((NUMBER_ELEMENT_WIDTH+ne_offset)*how_many_digits, 0)];
    [self addChild:digit z:20 tag:(how_many_digits+1)];
    [digits addObject:digit];
    
    printf("digits size = %d\n", digits.count);
    //[digit release];
    how_many_digits += 1;
    
    
}

-(void)makeNumber1:(int)num {
    [self addDigit:num];
}

-(void)makeNumber2:(int)num {
    int first, second;
    second = num % 10;
    first  = num / 10;
    
//    printf("number2 = %d : first = %d, second = %d\n",num,first,second);
    
    [self addDigit:first];
    [self addDigit:second];

    
}

-(void)makeNumber3:(int)num {
    int first, second, third;
    second = num %100 / 10;
    first  = num / 100;
    third = num % 10;
    
//    printf("number3 = %d : first = %d, second = %d, third = %d\n",num,first,second,third);
    
    [self addDigit:first];
    [self addDigit:second];
    [self addDigit:third];
}

-(void)makeNumber:(int)num
{
    if (num>99) {
        [self makeNumber3:num];
    }
    else if (num>9) {
        [self makeNumber2:num];
        
    } else {
        [self makeNumber1:num];
    }
}

-(SelectedSprite)getMatchedSlot:(CGPoint)loc {
    SelectedSprite selSprite;
    
    selSprite.updated = NO;
    

    //    int slot = 0;
    
    // touch info from digit
    //    CGPoint selPos;
    //    CCSprite* selSprite;
    //    int selSlot;
    //    Digit* selDigit;
    
    printf("co jest do kurwy nedzy ?!\n");
    printf("digits size = %d\n", digits.count);
    for (Digit* curr in digits) {
        
        printf(" digit position = %f, %f \n", curr.position.x, curr.position.y);
        CGPoint location = ccpSub(loc , curr.position);
        printf(" digit position po odjeciu = %f, %f \n", location.x, location.y);
        
        if ((selSprite.slot = [curr getMatchedSlot:location]) != 0) {
            selSprite.sprite = [curr getMatchFromSlot:selSprite.slot];
            selSprite.position = selSprite.sprite.position;
            selSprite.digit = curr;
            selSprite.updated = YES;
            return selSprite;
        }
    }
    return selSprite;
}

-(int)getValue {
    int value = 0;
    for (Digit* digit in digits) {
//        digit.tag
        value = value * 10 + [digit getValue];
    }
    return value;
}

-(Digit*)getDigitToProcessTouch:(CGPoint)loc {
    float minDistance = 1000;
    float dist;
    Digit* digitToProcess = nil;

    CGPoint location;
    
    for (Digit* digit in digits) {
        location = ccpSub(loc , ccpAdd(digit.position, self.position));
//        CCLOG(@"loc (x,y) : %.2f, %.2f", loc.x, loc.y);
//        CCLOG(@"location (x,y) : %.2f, %.2f", location.x, location.y);
//        CCLOG(@"digit.position [val= %d ] : %.2f, %.2f ", [digit getValue], digit.position.x, digit.position.y);
        //location = loc;
        if ( (dist = ccpDistance(location, digit.position)) < minDistance) {
            minDistance = dist;
            digitToProcess = digit;
//            CCLOG(@"min distance : %f   for digit :  %d",minDistance, [digit getValue]);            
        }

    }
    printf(" ==== digit value === (in number) : %d\n", [digitToProcess getValue]);
    return digitToProcess;
}


-(void)reset {
    how_many_digits = 0;
    digits = [[CCArray alloc] initWithCapacity:1];
}

-(id)init {
    self = [super init];
    
    if (self) {
        [self reset];
        
    }
    return self;
}

-(id)initWithNumber:(int)num {
    self = [super init];
    
    if (self) {
        [self reset];
        [self makeNumber:num];
    }
    return self;
}

-(id)initWithNumber:(int)num andType:(EquationElementType)etype
{
    self = [super init];
    if (self) {
        [self reset];
        [self setEquElementType:etype];
        [self makeNumber:num];
    }
    return self;
}

-(void)setEquElementType:(EquationElementType)etype {
    equElementType = etype;
}

-(EquationElementType)getEquElementType {
    return equElementType;
}

-(void)dealloc {
    
    [digits release];
    [super dealloc];
}
@end
