//
//  MenuScene.h
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 07.07.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

#import "GameLayer.h"

#import "GameScene.h"

#import "AboutScene.h"

#import "GameManager.h"

#import "LevelPickerScene.h"


@interface MenuScene : CCScene {
    AboutScene * aboutScene;
    LevelPickerScene * levelPickerScene;
    
}

-(void) playGame;

-(void) showLevels;
-(void) aboutInfo;

@end
