//
//  MenuScene.m
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 07.07.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import "MenuScene.h"


@implementation MenuScene

-(id) init {
  if (self = ([super init])) {
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    CCLayerColor * bgLayer = [[[CCLayerColor alloc] initWithColor:ccc4(0, 0, 0, 255)] autorelease];
    [self addChild:bgLayer z:0];
    
    
    /*
    CCLabelTTF * menuLabel = [CCLabelTTF labelWithString:@"iMatches Menu :" fontName:@"Marker Felt" fontSize:32];
    [menuLabel setPosition:ccp(size.width/2, size.height/2 + size.height/4)];
    [self addChild:menuLabel];
     */
     
    /*
     
     CCSprite * playButtonNormal = [CCSprite spriteWithFile:@"btn-play-normal.png"];
     CCSprite * playButtonSelected = [CCSprite spriteWithFile:@"btn-play-selected.png"];
     
     CCMenuItemSprite * playItem = [CCMenuItemSprite itemFromNormalSprite:playButtonNormal selectedSprite:playButtonSelected target:self selector:@selector(playGame)];
     
     
     CCSprite * highscoresButtonNormal = [CCSprite spriteWithFile:@"btn-highscores-normal.png"];
     CCSprite * highscoresButtonSelected = [CCSprite spriteWithFile:@"btn-highscores-selected.png"];
     
     CCMenuItemSprite * highscoresItem = [CCMenuItemSprite itemFromNormalSprite:highscoresButtonNormal selectedSprite:highscoresButtonSelected target:self selector:@selector(showHighscores)];
     */
    
    CCLabelTTF * playLabel = [CCLabelTTF labelWithString:@"[ play ]" fontName:@"Marker Felt" fontSize:20];
    
    CCMenuItemLabel * playItem = [CCMenuItemLabel itemWithLabel:playLabel target:self selector:@selector(playGame)];
    
    CCMenuItemImage * img = [CCMenuItemImage itemFromNormalImage:@"dice-icon.png" selectedImage:@"dice-icon.png"];
    [img setPosition:CGPointMake(100, 0)];
    [playItem addChild:img];
    
    CCLabelTTF * levelsLabel = [CCLabelTTF labelWithString:@"[ levels ]" fontName:@"Marker Felt" fontSize:20];
    
    CCMenuItemLabel * levelsItem = [CCMenuItemLabel itemWithLabel:levelsLabel target:self selector:@selector(showLevels)];
    
    //CCSprite * aboutButtonNormal = [CCSprite spriteWithFile:@"btn-about-normal.png"];
    //CCSprite * aboutButtonSelected = [CCSprite spriteWithFile:@"btn-about-selected.png"];
    
    CCLabelTTF * aboutLabel = [CCLabelTTF labelWithString:@"[ about ]" fontName:@"Marker Felt" fontSize:20];
    
    CCMenuItemLabel * aboutItem = [CCMenuItemLabel itemWithLabel:aboutLabel target:self selector:@selector(aboutInfo)];
    
    // CCMenuItemSprite * aboutItem = [CCMenuItemSprite itemFromNormalSprite:aboutButtonNormal selectedSprite:aboutButtonSelected target:self selector:@selector(aboutInfo)];
    
    
    CCMenu * menu = [CCMenu menuWithItems:playItem, levelsItem, aboutItem, nil];
    menu.position = CGPointMake(size.width/2, size.height/2 - size.height/8);
    
    [menu alignItemsVerticallyWithPadding:15];
    
    [self addChild:menu];
  }
  
  aboutScene = [[AboutScene alloc] init];
  aboutScene.menuScene = self;
  
  levelPickerScene = [[LevelPickerScene alloc] init];
  levelPickerScene.menuScene = self;
  //    levelPickerScene
  
  [self addTitle];
  
  return self;
}

// creates and adds title made from tiny matches
-(void) addTitle {
  CGSize size = [[CCDirector sharedDirector] winSize];
  
  CCSprite * title = [[CCSprite alloc] initWithFile:@"title_ver1.png"];
  
  [title setPosition:ccp(size.width/2, size.height*3/4)];
  [self addChild:title];
  
  /*
  
  CCSprite * tiny1 = [[CCSprite alloc] initWithFile:@"match1tiny.png"];
  CCSprite * tiny2 = [[CCSprite alloc] initWithFile:@"match2tiny.png"];
  CCSprite * tiny3 = [[CCSprite alloc] initWithFile:@"match3tiny.png"];
  CCSprite * tiny4 = [[CCSprite alloc] initWithFile:@"match4tiny.png"];
  
  //for (int i=1; i<10; i++) {
    [tiny1 setPosition:ccp(15+5, size.height/2)];
    [self addChild:tiny1];
  [tiny2 setPosition:ccp(35+5, size.height/2)];
  [self addChild:tiny2];
  [tiny3 setPosition:ccp(55+5, size.height/2)];
  [self addChild:tiny3];
  [tiny4 setPosition:ccp(75+5, size.height/2)];
  [self addChild:tiny4];
  
  //}
  
   */
}

-(void) playGame {
  [[SimpleAudioEngine sharedEngine] playEffect:@"pstryk.wav"];
  
  //CCTransitionMoveInR * moveInRight = [CCTransitionMoveInR transitionWithDuration:0.5f scene:[GameLayer scene]];
  
  //CCTransitionPageTurn * pageTurn = [CCTransitionPageTurn transitionWithDuration:1.0f scene:[GameLayer scene] backwards:YES];
  
  //CCTransitionPageTurn * pageTurn = [CCTransitionPageTurn transitionWithDuration:1.0f scene:[GameScene scene] backwards:YES];
  CCTransitionPageTurn * pageTurn = [CCTransitionPageTurn transitionWithDuration:1.0f
                                                                           scene:[[GameManager instance] gameScene] backwards:YES];
  
  //    [[CCDirector sharedDirector] replaceScene:[GameLayer scene]];
  [[CCDirector sharedDirector] replaceScene:pageTurn];
  //[[CCDirector sharedDirector] replaceScene:moveInRight];
}

-(void) showLevels {
  [[SimpleAudioEngine sharedEngine] playEffect:@"pstryk.wav"];
  
  levelPickerScene.panGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:levelPickerScene action:@selector(moveLayer:)] autorelease];
  [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:levelPickerScene.panGestureRecognizer];
  CCTransitionMoveInR * moveInR = [CCTransitionMoveInR transitionWithDuration:0.5f scene:levelPickerScene];
  [[CCDirector sharedDirector] replaceScene:moveInR];
  
}

-(void) aboutInfo {
  [[SimpleAudioEngine sharedEngine] playEffect:@"pstryk.wav"];
  
  aboutScene.panGestureRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:aboutScene action:@selector(moveLayer:)] autorelease];
  [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:aboutScene.panGestureRecognizer];
  CCTransitionMoveInT * moveInTop = [CCTransitionMoveInT transitionWithDuration:0.5f scene:aboutScene];
  [[CCDirector sharedDirector] replaceScene:moveInTop];
  
}



-(void) dealloc {
  [aboutScene release];
  
  [super dealloc];
}

@end
