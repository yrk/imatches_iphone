//
//  Equals.h
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 01.03.2012.
//  Copyright (c) 2012 yarkill@gmail.com. All rights reserved.
//

#import "cocos2d.h"
#import "EquationElement.h"

#import "MatchSprite.h"
#import "PhysicsWorks.h"

@interface Equals : EquationElement <PhysicsWorks> {
    CCArray * matches;
    
    
    //CCRotateTo * rotRight;
}


// from PhysicsWorks protocol :
//-(void)updatePhysicsBodies;

@end
