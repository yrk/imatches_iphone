//
//  My_constants.h
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 21.11.2011.
//  Copyright 2011 yarkill@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
//#import "Digit.h"

//extern const int MATCH_HEIGHT;
//extern const int MATCH_WIDTH;

//#define MATCH_HEIGHT 75
#define MATCH_HEIGHT 65
//#define MATCH_WIDTH 16
#define MATCH_WIDTH 12

// MATCH_HEIGHT + 2 * MATCH_WIDTH
//#define NUMBER_ELEMENT_WIDTH 99
#define NUMBER_ELEMENT_WIDTH (MATCH_HEIGHT + 2 * MATCH_WIDTH)
// 2 * MATCH_HEIGHT + 1
//#define NUMBER_ELEMENT_HEIGHT 151
#define NUMBER_ELEMENT_HEIGHT (2*MATCH_HEIGHT +1)

extern const float half_ne_width;
extern const float half_ne_height;

extern const float ne_offset;

extern const float half_match_width;

typedef enum {
    EGameSceneTag,
    EGameLayerTag,
    EMenuLayerTag,
    ELevelManagerTag
    
} AppObjectsTags;

typedef enum {
    EBackgroundTag = 100,
    ETouchBeganLabelTag,
    ETouchCurrentLabelTag,
    ETouchEndedLabalTag,
    ESelSpriteLocationTag,
    ESelNumberLocationTag,
    EEquStringTag
    
} GameObjectsTags;

typedef enum {
    background_zValue=0,
    digitPlaceholder_zValue,
    matchSprite_zValue,
    
    popupWindow_zValue
} LevelzOrderValues;

typedef enum {
    match_horizontal = 100,
    match_vertical,
    match_slash,
    match_backslash
} Match_type;

typedef enum {
    EPlusOperator,
    EMinusOperator,
    EMulOperator,
    EDivOperator,
    EEquOperator,
    
    OperatorTypeSize
} OperatorType;

typedef enum {
    oneDigitNumber,
    twoDigitNumber,
    threeDigitNumber
} NumberType;

typedef enum {
    EGenByAdd,
    EGenByRemove,
    EGenByMove,
    
    LevelGenTypeSize
} LevelGenType;

typedef enum {
    EOperandA,
    EOperandB,
    EOperator,
    EEquals,
    EResult
    
} EquationElementType;

struct Equation {
    int a,b,op,result;
};

typedef struct Equation Equation;

// Digit class forward declaration
@class Digit;


typedef struct SelectedSprite {
    CCSprite * sprite;
    Digit* digit;
    int slot;
    CGPoint position;
    BOOL updated;
} SelectedSprite;



