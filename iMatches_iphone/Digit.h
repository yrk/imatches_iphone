//
//  Digit.h
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 20.11.2011.
//  Copyright 2011 yarkill@gmail.com. All rights reserved.
//

#import "chipmunk.h"

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "My_constants.h"
#import "EquationElement.h"
#import "MatchSprite.h"

#import "PhysicsManager.h"

#import "MatchesCanChange.h"
#import "PhysicsWorks.h"

@interface Digit : EquationElement <MatchesCanChange, PhysicsWorks> {


    CCArray* matches;
    CGPoint points[8];
    CGRect slots[8];
    BOOL slotsTaken[8];
    
    CGPoint selectedPos;
    CCSprite* selectedSprite;
    int slotToUpdate;
    
    unsigned char numberCode;
    
    EquationElementType equElementType;
    
    cpSpace * space;
    
    // HIGLIGHT VARS and PARAMS
    int slotToHighlight;
    BOOL slotHighlighted;
    
    MatchSprite * highlightMatch;
    unsigned int highlightOpacity;
    BOOL highlightOpacityAscending;
}

//@property(nonatomic, )

-(void)makeNumber:(int)num;
-(void)reset;

-(void)addNumberBg;

//-(id)initWithSpace:(cpSpace*)newSpace;
-(id)initWithNumber:(int)num;
-(id)initWithNumber:(int)num andType:(EquationElementType)etype;

-(void)update:(ccTime)dt;

-(void)addMatchNumber:(int)slot;
-(void)initPoints;
-(void)initSlots;

-(void)removeMatch:(CCSprite*)match withSlot:(int)slot;
-(int)getValue;

// validation of digit value after applying match changes
-(BOOL)isNewMatchInSlotValid:(int)slot;
-(BOOL)isMatchValidWithoutSlot:(int)slot;
-(BOOL)isNewMatchInSlotValid:(int)slot without:(int)slotRemoved;

-(void)setEquElementType:(EquationElementType)etype;
-(EquationElementType)getEquElementType;

-(NSString*) getMatchStringFromSlot:(int)slot;
-(NSString*) getMatchStringFromSlot:(int)slot withBg:(BOOL)bg;

-(void)addBackgroundMatchNumber:(int)slot;

-(int)getSlotIn:(CGPoint)location taken:(BOOL)isTaken;

// from MatchesCanChange protocol:
//-(int)getMatchedSlot:(CGPoint)location;
//-(CCSprite*)getMatchFromSlot:(int)slot;


-(int)getNotMatchedSlot:(CGPoint)location;


-(void) clearHighlight;
-(void) highlightSlot:(int) slot;


//-(BOOL)fitsToSlot:(CGPoint)loc;


-(CGRect)makeRectFromPoints:(CGPoint)pos andSize:(CGPoint)size;

// chipmunk physics update
// from PhysicsWorks protocol :
//-(void)updatePhysicsBodies;

@end
