//
//  EquationElement.h
//  iMatches
//
//  Created by Jaroslaw Adamowicz on 20.07.2012.
//  Copyright 2012 yarkill@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#import "MatchesCanChange.h"
#import "PhysicsWorks.h"

@interface EquationElement : CCNode <MatchesCanChange, PhysicsWorks> {
    
}

-(char) getCharValue;

-(BOOL)canRecieveMatch;

-(BOOL)canGiveMatch;

@end
