//
//  Slot.m
//  cocosHelloWorld
//
//  Created by Jaroslaw Adamowicz on 20.11.2011.
//  Copyright 2011 yarkill@gmail.com. All rights reserved.
//

#import "Slot.h"

@implementation Slot

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@synthesize rect;
@synthesize x, y;
//@synthesize segment;


- (void)dealloc {
    [super dealloc];
}
@end
