//
//  LevelSolver.m
//  iMatches_iphone
//
//  Created by Admin on 12.06.2014.
//
//

#import "LevelSolver.h"

@implementation LevelSolver

-(id)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

-(BOOL) isEqualCleared:(NSString*)equation {
    NSArray * expressions = [equation componentsSeparatedByString:@"="];
    if ([expressions count] < 2) {
        return YES;
    }
    return NO;
}

-(BOOL) equationStringSolved:(NSString*)equation {
    BOOL result = NO;

    NSArray * expressions = [equation componentsSeparatedByString:@"="];
    if ([expressions count] < 2) {
        return NO;
    }

    
    NSString * leftAfterMult = [self computeMultiplications:(NSString*)[expressions objectAtIndex:0]];
    
    NSString * leftAfterDiv = [self computeDivisions:leftAfterMult];
    
    NSString * rightAfterMult = [self computeMultiplications:(NSString*)[expressions objectAtIndex:1]];
    
    NSString * rightAfterDiv = [self computeDivisions:rightAfterMult];
    
    int leftSide = [self computeStr:leftAfterDiv];
    int rightSide = [self computeStr:rightAfterDiv];
    
    NSLog(@"leftSide = %d", leftSide);
    NSLog(@"rightSide = %d", rightSide);
        
    result = (leftSide == rightSide) ? YES : NO;
    
    if (result) NSLog(@"equation solved !");
    else NSLog(@"equation not solved :(");
    
    return result;
}

-(int) computeStr:(NSString*)str {

    NSArray * dirtyNumbers = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"+-/*"]];

    NSArray * dirtyOperands = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]];

    NSMutableArray * numbers = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray * operands = [NSMutableArray arrayWithCapacity:1];
    
    for (NSString * intStr in dirtyNumbers) {
        if ([intStr length] != 0) {
            int val = [intStr intValue];
            NSValue * num = [NSValue value:&val withObjCType:@encode(int)];

            [numbers addObject:num];
        }
    }
    
    for (NSString * opStr in dirtyOperands) {
        if ([opStr length] != 0) {
            
            char op = [opStr characterAtIndex:0];
            NSValue * oper = [NSValue value:&op withObjCType:@encode(char)];
            [operands addObject:oper];
            
        }
    }

    // this section is trying to COMPUTE whole expersion and return result
    
    // computed output :
    int firstNumber = 0;
    
    // iterators :
    int operIt = 0;
    int numberIt = 0;
    if ([operands count] == [numbers count]) {
        // we have 1 operand befor first value 
        NSValue * firstSign = (NSValue*)[operands objectAtIndex:0];
        char sign;
        [firstSign getValue:&sign];
        
        NSValue * currNumber = (NSValue*)[numbers objectAtIndex:0];
        int currNumberInt;
        [currNumber getValue:(&currNumberInt)];
        
        if (sign == '-') 
            firstNumber -= currNumberInt;
        else {
            firstNumber += currNumberInt;
        }
        
        // pierwsza cyfra ujemna lub dodatnia uwzgledniona
        // teraz trzeba operowac na kolejnych
        operIt++;
        numberIt++;
    } else {
        NSValue * currNumber = (NSValue*)[numbers objectAtIndex:0];
        int currNumberInt;
        [currNumber getValue:(&currNumberInt)];
        firstNumber += currNumberInt;
        numberIt++;
    }
    
    while (operIt < [operands count] && numberIt < [numbers count]) {
        NSValue * operand = [operands objectAtIndex:operIt];
        char oper;
        [operand getValue:(&oper)];
        
        NSValue * nextNum = [numbers objectAtIndex:numberIt];
        int num;
        [nextNum getValue:(&num)];
        
        switch (oper) {
            case '-':
                firstNumber -= num;
                break;
                
            case '+':
                firstNumber += num;
                break;
                
            default:
                break;
        }
        operIt++;
        numberIt++;
        
    }
    
    // wynik jest poprawnie wyliczony tylko dla rownan bez * i /
    printf("wynik : %d \n", firstNumber);
    return firstNumber;
}

-(NSString*) computeMultiplications:(NSString*) str {

    NSLog(@"%@ : %@", @"computeMultiplications (str) ", str);
    
    NSError * error = NULL;
    
    NSRegularExpression * regexp = [NSRegularExpression regularExpressionWithPattern:@"[0-9]*\\*[0-9]*" options:NSRegularExpressionCaseInsensitive error:(&error)];
    
    NSTextCheckingResult * result = [regexp firstMatchInString:str options:0 range:NSMakeRange(0, [str length])];
    
    NSRange resultRange = [result range];
    NSLog(@"range len: %d", [result range].length);
    NSLog(@"range loc: %d", [result range].location);
    
    NSLog(@"result : %@", result);

    if (result != nil) {
    
        NSMutableString * strCopy = [str mutableCopy];
    
        NSString * match = [regexp replacementStringForResult:result inString:strCopy offset:0 template:@"$0"];
    
        NSLog(@"regexp match : %@", match);

        [strCopy replaceCharactersInRange:resultRange withString:[self resolveMultString:match]];
        
        return strCopy;
    }
    else {
        return str;
    }
}

-(NSString*) computeDivisions:(NSString*) str {

    NSLog(@"%@ : %@", @"computeDivisions (str) ", str);
    
    NSError * error = NULL;
    
    NSRegularExpression * regexp = [NSRegularExpression regularExpressionWithPattern:@"[0-9]*\\/[0-9]*" options:NSRegularExpressionCaseInsensitive error:(&error)];
    
    NSTextCheckingResult * result = [regexp firstMatchInString:str options:0 range:NSMakeRange(0, [str length])];
    
    NSRange resultRange = [result range];
    NSLog(@"range len: %d", [result range].length);
    NSLog(@"range loc: %d", [result range].location);
    
    NSLog(@"result : %@", result);
    
    if (result != nil) {
        
        NSMutableString * strCopy = [str mutableCopy];
        
        NSString * match = [regexp replacementStringForResult:result inString:strCopy offset:0 template:@"$0"];
        
        NSLog(@"regexp match : %@", match);
        
        [strCopy replaceCharactersInRange:resultRange withString:[self resolveDivString:match]];
        
        return strCopy;
    }
    else {
        return str;
    }
}

// solving only strings like : 2*4 or 15*3 (number op number)
-(NSString*) resolveMultString:(NSString*) str {
    
    int result = 0; // zero is assumed as usually wrong solution !!!
    
    NSArray * dirtyNumbers = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"*"]];

    if ([dirtyNumbers count] > 1 ) {
        int first = [[dirtyNumbers objectAtIndex:0] intValue];
        int second = [[dirtyNumbers objectAtIndex:1] intValue];
    
        result = first * second;
    }
    
    return [NSString stringWithFormat:@"%d", result];
}

-(NSString*) resolveDivString:(NSString*) str {
    
    int result = 0; // zero is assumed as usually wrong solution !!!
    
    NSArray * dirtyNumbers = [str componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/"]];
    
    if ([dirtyNumbers count] > 1 ) {
        int first = [[dirtyNumbers objectAtIndex:0] intValue];
        int second = [[dirtyNumbers objectAtIndex:1] intValue];
    
        result = first / second;
    }
        
    return [NSString stringWithFormat:@"%d", result];
}



@end
